<?php

namespace App\Services;

use App\Comment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommentService
{
    /**
     * Creates new comment
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Comment
     */
    public function create(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $comment = Comment::create([
                'body' => $request->body,
                'post_id' => $request->id,
                'user_id' => auth()->id()
            ]);

            auth()->user()->update(
                ['last_commented_at' => Carbon::now()]
            );

            return $comment;
        });
    }
}
