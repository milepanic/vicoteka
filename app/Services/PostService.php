<?php

namespace App\Services;

use App\Category;
use App\Jobs\MakeImageFromPost;
use App\Jobs\OptimizeImage;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostService
{
    /**
     * Creates new post
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Post
     */
    public function create(Request $request)
    {
        $category = Category::where('name', $request->category)
                        ->firstOrFail();

        return DB::transaction(function () use ($request, $category) {
            $post = Post::create([
                'body' => $request->body,
                'video' => $request->video,
                'user_id' => auth()->id(),
                'category_id' => $category->id,
                'nsfw' => $request->nsfw
            ]);

            // TODO: ovo prebaciti u post event created()
            auth()->user()->update(
                ['last_posted_at' => Carbon::now()]
            );

            auth()->user()
                ->postVote()
                ->attach($post->id, ['vote' => 1]);
            // ...

            if ($request->image)
                $this->uploadImage($post, $request->image);

            dispatch(new MakeImageFromPost($post));

            return $post;
        });
    }

    /**
     * Updates post
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        Post::findOrFail($id)
            ->update($request->all());
    }

    /**
     * Soft deletes post
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        Post::findOrFail($id)
            ->delete();
    }

    /**
     * Restores soft deleted post
     * @param  int $id
     * @return void
     */
    public function restore($id)
    {
        Post::withTrashed()
            ->find($id)
            ->restore();
    }

    /**
     * Uploads post image
     * @param  \App\Post $post
     * @param  \Illuminate\Http\File $image
     * @return void
     */
    private function uploadImage(Post $post, $image)
    {
        $ext = $image->getClientOriginalExtension() == 'gif' ? '.gif' : '.jpg';

        if ($ext == '.gif') {
            $path = $post->id . '-' . uniqid() . $ext;
            Storage::putFileAs('public/images/posts', $image, $path);
            $path = 'images/posts/' . $path;
        } else {
            $path = 'images/posts/' . $post->id . '-' . uniqid() . $ext;
            Storage::put("public/$path", (string) Image::make($image)->encode());
        }

        dispatch(new OptimizeImage(storage_path("app/public/$path")));

        $post->update(['image' => "storage/$path"]);
    }
}
