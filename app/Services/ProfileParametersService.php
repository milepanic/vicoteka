<?php

namespace App\Services;

use App\Category;
use App\Comment;
use App\Post;
use App\User;

class ProfileParametersService
{
    public static function getParameter($parameter, $user)
    {
        switch ($parameter) {

            case 'postovi':
                $posts = static::getLatestPosts($user);

                return view('profile', compact('user', 'posts'));

            case 'komentari':
                $comments = static::getComments($user);

                return view('profile', compact('user', 'comments'));

            case 'pratioci':
                $users = static::getFollowers($user, 'followed_id');

                return view('profile', compact('user', 'users'));

            case 'prati':
                $users = static::getFollows($user, 'follower_id');

                return view('profile', compact('user', 'users'));

            case 'pretplacene-kategorije':
                $categories = static::getCategories($user, 'subscribed');

                return view('profile', compact('user', 'categories'));

            case 'moderator-kategorija':
                $categories = static::getCategories($user, 'moderator');

                return view('profile', compact('user', 'categories'));

            case 'omiljeni-postovi':
                $posts = static::getFavoritePosts($user);

                return view('profile', compact('user', 'posts'));

            case 'upvoteovani-postovi':
                $posts = static::getVotedPosts($user, 1);

                return view('profile', compact('user', 'posts'));

            case 'downvoteovani-postovi':
                $posts = static::getVotedPosts($user, -1);

                return view('profile', compact('user', 'posts'));
        }
    }

    public static function getLatestPosts($user)
    {
        return Post::where('user_id', $user->id)
                ->eagerLoad()
                ->latest()
                ->simplePaginate(15);
    }

    public static function getVotedPosts($user, $vote)
    {
        $votedIds = $user->postVote()
                        ->where('vote', $vote)
                        ->get()
                        ->pluck('id')
                        ->toArray();

        return Post::whereIn('id', $votedIds)
                ->eagerLoad()
                ->latest()
                ->simplePaginate(15);
    }

    public static function getFavoritePosts($user)
    {
        $favorites = $user->favoritePosts()
                        ->get()
                        ->pluck('id')
                        ->toArray();

        return Post::whereIn('id', $favorites)
                ->eagerLoad()
                ->latest()
                ->simplePaginate(15);
    }

    public static function getCategories($user, $column)
    {
        $categoryIds = $user->categories()
                        ->where($column, 1)
                        ->get()
                        ->pluck('id')
                        ->toArray();
        
        return Category::whereIn('id', $categoryIds)
                ->withCount([
                    'users as subscribers' => function ($query) {
                        $query->where('subscribed', 1);
                    }
                ])->latest()
                ->simplePaginate(15);
    }

    public static function getFollowers($user, $column)
    {
        $userIds = $user->followers()
                        ->get()
                        ->pluck('id')
                        ->toArray();

        return User::whereIn('id', $userIds)
                ->withCount('followers as followers')
                ->latest()
                ->simplePaginate(15);
    }

    public static function getFollows($user, $column)
    {
        $userIds = $user->following()
                        ->get()
                        ->pluck('id')
                        ->toArray();

        return User::whereIn('id', $userIds)
                ->withCount('followers as followers')
                ->latest()
                ->simplePaginate(15);
    }

    public static function getComments($user)
    {
        return Comment::where('user_id', $user->id)
                ->with('user')
                ->withCount([
                    'votes as upvotes_count' => function ($query) {
                        $query->where('vote', 1);
                    },
                    'votes as downvotes_count' => function ($query) {
                        $query->where('vote', -1);
                    }
                ])
                ->simplePaginate(25);
    }

}
