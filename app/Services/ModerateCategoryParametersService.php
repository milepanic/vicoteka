<?php

namespace App\Services;

use App\Post;
use App\Report;

class ModerateCategoryParametersService
{
    
    public static function getByParameter($category)
    {
        switch (request()->prikaz) {
            case 'sve-objave':
                return static::getAllPosts($category);
            case 'prijave':
                return static::getReports($category);
            case 'edit':
                 
                break;
            case 'ban':
                 
                break;
            case 'dodavanje':
                
                break;
        }
    }

    public static function getAllPosts($category)
    {
        return Post::where('category_id', $category->id)
                    ->withTrashed()
                    ->eagerLoad()
                    ->get();
    }

    public static function getReports($category)
    {
        return Report::where('category_id', $category->id)
                    ->get();
    }

}
