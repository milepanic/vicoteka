<?php

namespace App\Services;

use App\Category;
use App\Jobs\OptimizeImage;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CategoryService
{
    /**
     * Creates new category
     *
     * @param  Request $request
     * @return \App\Category
     */
    public function create(Request $request)
    {
        // Slugify is called so the cyrillic and latin
        // names will have the same slug
        $slugify = new Slugify(['rulesets' => ['default', 'serbian']]);
        $slug = $slugify->slugify($request->name);

        $path = "images/categories/$slug-" . uniqid() . ".jpg";

        return DB::transaction(function () use ($request, $slug, $path) {
            $category = Category::create([
                'name' => $request->name,
                'slug' => $slug,
                'description' => $request->description,
                'image' => "storage/$path",
                'nsfw' => $request->nsfw,
                'pictures' => $request->pictures,
                'videos' => $request->videos,
                'mods_only' => $request->mods,
            ]);

            $this->uploadImage($category, $request->croppedImage, $path);

            auth()->user()->update(
                ['last_created_category_at' => Carbon::now()]
            );

            // gives moderator role to user for created category
            if (! $category->users->contains(auth()->id())) {
                $category->users()
                    ->attach(auth()->id(), ['moderator' => true]);
            } else {
                $category->users()
                    ->updateExistingPivot(auth()->id(), ['moderator' => true]);
            }

            return $category;
        });
    }

    /**
     * Updates category
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Category $category
     * @return void
     */
    public function update(Request $request, Category $category)
    {
        $path = "images/categories/$category->slug-" . uniqid() . ".jpg";

        $category->update([
            'description' => $request->description,
            'image' => $request->croppedImage ? "storage/$path" : $category->image,
            'nsfw' => $request->nsfw,
            'pictures' => $request->pictures,
            'videos' => $request->videos,
            'mods_only' => $request->mods,
        ]);

        if ($request->croppedImage)
            $this->uploadImage($category, $request->croppedImage, $path);
    }

    /**
     * Uploads category image
     *
     * @param  \App\Category $category
     * @param  \Illuminate\Http\File $image
     * @param  string $path
     * @return void
     */
    private function uploadImage(Category $category, $image, $path)
    {
        Storage::put(
            "public/$path",
            (string) Image::make($image)
                    ->resize(300, 300)
                    ->encode()
        );

        dispatch(new OptimizeImage(storage_path("app/public/$path")));
    }
}
