<?php

namespace App\Services;

use App\Comment;
use App\Notifications\NewComment;
use App\Notifications\NewFollower;
use App\Notifications\NewPost;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Auth;

class NotificationService
{
    /**
     * Notifies moderators about new post in their category
     * 
     * @param  App\Post $post
     * @return void
     */
    public static function postCreated(Post $post)
    {
        // FIX: send to moderators of category && make NotificationService
        $moderators = [];

        foreach($post->category->users as $user)
            if ($user->pivot->moderator == 1 && $user->id != Auth::id())
                $user->notify(new NewPost($post->user, $post, $post->category->name));
    }

    /**
     * Notifies posts owner about new new comment
     * 
     * @param  App\Comment $comment
     * @return void
     */
    public static function commentCreated(Comment $comment)
    {
        if (Auth::user()->slug != $comment->post->user->slug)
            $comment->post->user->notify(new NewComment($comment->user, $comment->post));
    }

    /**
     * Notifies user about new follower
     * 
     * @param  App\User $user
     * @return void
     */
    public static function userFollowed(User $user)
    {
        $user->notify(new NewFollower(Auth::user()));
    }
}
