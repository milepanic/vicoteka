<?php

namespace App\Services;

use App\Jobs\OptimizeImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UserService
{
    /**
     * Updates user info
     *
     * @param  Request $request
     * @return void
     */
    public function update(Request $request)
    {
        DB::transaction(function () use ($request) {
            Auth::user()->update([
                'username' => $request->username,
                'slug' => str_slug($request->username),
                'description' => $request->description,
                'facebook' => $request->facebook,
                'twitter' => $request->twitter,
                'instagram' => $request->instagram,
                'tumblr' => $request->tumblr,
                'nsfw' => $request->nsfw,
            ]);

            if ($request->croppedImage)
                $this->uploadImage($request->croppedImage);
        });
    }

    /**
     * Uploads small and large profile image
     *
     * @param  \Illuminate\Http\File $image
     * @return void
     */
    private function uploadImage($image)
    {
        $path_sm = 'images/users/' . Auth::user()->slug . '-' . time() . '-sm.jpg';
        $path_lg = 'images/users/' . Auth::user()->slug . '-' . time() . '-lg.jpg';

        Storage::put(
            "public/$path_sm",
            (string) Image::make($image)
                ->resize(100, 100)->encode()
        );

        Storage::put(
            "public/$path_lg",
            (string) Image::make($image)
                ->resize(300, 300)->encode()
        );

        dispatch(new OptimizeImage(public_path($path_sm)));
        dispatch(new OptimizeImage(public_path($path_lg)));

        Auth::user()->update([
            'image_sm' => "storage/$path_sm",
            'image_lg' => "storage/$path_lg",
        ]);
    }

}
