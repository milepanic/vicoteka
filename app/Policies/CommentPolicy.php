<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Admin can do everything
     * 
     * @param  \App\User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->admin)
            return true;
    }

    /**
     * Checks if user has commented less than 30 seconds ago
     * 
     * @param  \App\User $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->last_commented_at < Carbon::now()->subSeconds(30)->toDateTimeString();
    }
}
