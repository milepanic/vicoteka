<?php

namespace App\Policies;

use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Admin can do everything
     * 
     * @param  \App\User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->admin)
            return true;
    }

    /**
     * Checks if user has posted less than 10 minutes ago
     * 
     * @param  \App\User $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->last_posted_at < Carbon::now()->subMinutes(10)->toDateTimeString();
    }

    /**
     * TODO: Add moderator of posts category
     * Checks if user is owner of post or moderator
     * 
     * @param  App\User $user
     * @return void
     */
    public function update(User $user)
    {
        return Auth::id() == $post->user->id;
    }
}
