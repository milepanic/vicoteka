<?php

namespace App\Policies;

use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Admin can do everything
     * 
     * @param  \App\User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->admin)
            return true;
    }

    /**
     * Checks if user has created a category less than day ago
     * 
     * @param  \App\User $user
     * @return bool
     */
    public function store(User $user)
    {
        return $user->last_created_category_at < Carbon::now()->subDays(1)->toDateTimeString();
    }

    /**
     * TODO: check if it is moderator of the category
     * Checks if user is moderator of the category
     * 
     * @param  App\User $user
     * @return void
     */
    public function update(User $user)
    {
        return true;
    }
}
