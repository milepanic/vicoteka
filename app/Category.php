<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Laravel\Scout\Searchable;

class Category extends Model
{
    use Searchable;
    use SoftDeletes;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['id', 'name', 'description']);
    }
    
	protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }
	
    public function posts()
    {
    	return $this->hasMany('App\Post');
    }

    public function users()
    {
    	return $this->belongsToMany('App\User')
	    	->withPivot('subscribed', 'blocked', 'moderator');
    }

    public function scopeFilter($query, $filter)
    {
        switch ($filter) {
            case 'popularne':
                return $query->orderBy('subscribers', 'desc');

            case 'nove':
                return $query->latest();

            case 'moje':
                if (Auth::guest()) return;

                $moderator = Auth::user()->categories()
                                ->where('moderator', 1)
                                ->get()
                                ->pluck('id')
                                ->toArray();

                return $query->whereIn('id', $moderator);

            case 'nepracene':
                if (Auth::guest()) return;
            
                $following = Auth::user()->categories()
                                ->where('subscribed', 1)
                                ->get()
                                ->pluck('id')
                                ->toArray();

                return $query->whereNotIn('id', $following);
            
            // case: 'popularne'
            default:
                return $query->orderBy('subscribers', 'desc');
        }
    }

}
