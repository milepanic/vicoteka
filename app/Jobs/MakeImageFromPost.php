<?php

namespace App\Jobs;

use App\Jobs\OptimizeImage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MakeImageFromPost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $post;
    public $text;
    public $path;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($post)
    {
        $this->post = $post;
        $this->text = Str::limit($post->body, 450);
        $this->path = "public/images/share-posts-fb/$post->id.jpg";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch (true) {
            case $this->post->image:
                Storage::put(
                    $this->path,
                    (string) Image::make(public_path($this->post->image))
                                ->encode()
                );

                break;

            case $this->post->video:
                Storage::put(
                    $this->path,
                    (string) Image::make('https://img.youtube.com/vi/' . substr($this->post->video, 30) . '/mqdefault.jpg')
                                ->encode()
                );

                break;

            default:
                $width = 1200;
                $height = 700;
                $center_x = 60;
                $center_y = 250;
                $max_len = 68;
                $font_size = 30;
                $font_height = 23;

                $lines = explode("\n", wordwrap($this->text, $max_len));
                $y     = $center_y - ((count($lines) - 1) * $font_height);
                $img   = Image::canvas($width, $height, '#f8fafc');

                foreach ($lines as $line)
                {
                    $img->text($line, $center_x, $y, function ($font) use ($font_size) {
                        $font->file(public_path('fonts/OpenSans-Regular.ttf'));
                        $font->size($font_size);
                        $font->color('#334854');
                        $font->align('left');
                        $font->valign('center');
                    });

                    $y += $font_height * 2;
                }

                $img->insert(public_path('images/social-footer.png'), 'bottom');

                Storage::put(
                    $this->path,
                    (string) $img->encode()
                );

                break;
        }
        
    }
}
