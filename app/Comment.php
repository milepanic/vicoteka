<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    
	protected $fillable = [
		'body', 'post_id', 'user_id',
	];

    protected $appends = ['diffCreatedAt'];

    public function getDiffCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    public function post()
    {
    	return $this->belongsTo('App\Post');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function votes()
    {
        return $this->belongsToMany('App\User', 'comments_votes')->withPivot('vote');
    }

    public function votedBy(User $user)
    {
        return $this->votes()->where('user_id', $user->id)->pluck('vote');
    }
}
