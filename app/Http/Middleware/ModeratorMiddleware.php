<?php

namespace App\Http\Middleware;

use App\Category;
use Closure;
use Illuminate\Support\Facades\Auth;

class ModeratorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $urlParts = explode('/', $request->path());

        $category = Category::where('slug', $urlParts[1])
                ->firstOrFail();

        if (Auth::guest() || Auth::user()->admin === 0 
            && Auth::user()->categories->where('id', $category->id)->first()->pivot->moderator !== 1)
            abort(403);

        return $next($request);
    }
}
