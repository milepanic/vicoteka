<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Http\Responses;
use App\Services\CommentService;
use App\Services\NotificationService;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Creates new instance
     *
     * @param \App\Services\CommentService $commentService
     */
    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return new Responses\CommentIndexResponse($id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CommentRequest $request
     * @return \App\User
     * @return \App\Comment
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $comment = $this->commentService->create($request);

        NotificationService::commentCreated($comment);

        return response()->json([
            'user' => auth()->user(),
            'comment' => $comment
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, Comment $comment)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        Comment::destroy($id);

        return response()->json('Comment has been deleted');
    }

    public function vote(Request $request)
    {
        $id       = $request->id;
        $type     = $request->type;
        $comment  = Comment::find($id);

        if ($type === 'upvote')
            $vote = 1;
        elseif ($type === 'downvote')
            $vote = -1;

        // ako korisnik nije glasao - upisi $vote u tabelu
        if (!$comment->votedBy(auth()->user())->count() > 0) {
            auth()->user()
                ->commentVote()
                ->attach($id, ['vote' => $vote]);

            return response(201);
        }

        // ako korisnik zeli da ponisti svoj vote (vote === $vote) - obrisi red iz tabele
        if ($comment->votedBy(auth()->user())->first() === $vote)
            auth()->user()
                ->commentVote()
                ->detach($id);

        // ako korisnik promijeni vote (vote !== $vote) - update-uj vote
        elseif ($comment->votedBy(auth()->user())->first() !== $vote)
            auth()->user()
                ->postVote()
                ->updateExistingPivot($id, ['vote' => $vote]);

        return response(200);
    }
}
