<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class FeedbackController extends Controller
{
    public function store(Request $request)
    {
        $report = Feedback::create([
            'body' => $request->body,
            'user_id' => Auth::id()
        ]);

        if($request->image) {
            Image::make($request->image)
                ->encode('png')
                ->save(public_path('images/feedback/' . $report->id . '.png'));

            $report->update(['image' => 'images/feedback/' . $report->id . '.png']);
        }

        return back();
    }
}
