<?php

namespace App\Http\Controllers;

use App\Post;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    public function store(Request $request)
    {
        if ($request->reportable_type === 'comment') {
            $post = Post::where('id', $request->post_id)
                    ->with('category')
                    ->first();

            $request->category_id = $post->category->id;
        }

        Report::create([
            'body' => $request->body,
            'reportable_type' => $request->reportable_type,
            'reportable_id' => $request->reportable_id,
            'user_id' => Auth::id(),
            'category_id' => $request->category_id,
        ]);

        return 201;
    }
    
}
