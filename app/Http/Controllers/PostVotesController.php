<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostVotesController extends Controller
{
    /**
     * Votes on post
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $post = Post::withTrashed()
                    ->find($id);

        if ($type === 'upvote')
            $vote = 1;
        elseif ($type === 'downvote')
            $vote = -1;

        // if the user did not vote - store new vote in pivot table
        if (! $post->votedBy(auth()->user())->count() > 0) {
            auth()->user()
                ->postVote()
                ->attach($id, ['vote' => $vote]);

            return response(201);
        }

        // check if the user cancelled or changed his vote
        if ($post->votedBy(auth()->user())->first() === $vote) {
            auth()->user()
                ->postVote()
                ->detach($id);
        } else if ($post->votedBy(auth()->user())->first() !== $vote) {
            auth()->user()
                ->postVote()
                ->updateExistingPivot($id, ['vote' => $vote]);
        }

        return response(204);
    }
}
