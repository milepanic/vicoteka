<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use App\Http\Responses;
use App\Services\CategoryService;
use App\Services\ModerateCategoryParametersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CategoryController extends Controller
{
    /**
     * Creates new instance
     *
     * @param \App\Services\CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Shows all categories
     *
     * @param string $filter
     * @return \App\Http\Responses\CategoryIndexResponse
     */
    public function index($filter = null)
    {
        return new Responses\CategoryIndexResponse($filter);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Creates a new category
     *
     * @param  \App\Http\Requests\CategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category = $this->categoryService->create($request);

        return response($category->slug, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @param  string $filter
     * @return \App\Http\Responses\CategoryShowResponse
     */
    public function show($slug, $filter = null)
    {
        return new Responses\CategoryShowResponse($slug, $filter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $category = Category::where('slug', $slug)->first();

        $data = ModerateCategoryParametersService::getByParameter($category);

        return view('category-settings', compact('category', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $this->categoryService->update($request, $category);

        return 201;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

    /**
     * User blocks category
     *
     *
     */
    public function block($id)
    {
        $category = Category::find($id);

        if(!$category->users->contains(auth()->id())) {
            $category->users()
                ->attach(auth()->id(), ['blocked' => 1]);

            return 201;
        }

        $category->users()
            ->updateExistingPivot(auth()->id(), ['blocked' => 1]);

        return 201;
    }

    public function unblock($id)
    {
        $category = Category::find($id);

        $category->users()
            ->updateExistingPivot(auth()->id(), ['blocked' => 0]);

        return 204;
    }

    public function subscribe(Request $request)
    {
        $category = Category::findorFail($request->id);

        if(!$category->users->contains(auth()->id())) {
            $category->users()
                ->attach(auth()->id(), [$request->column => $request->action]);

            return 201;
        }

        $category->users()
            ->updateExistingPivot(auth()->id(), [$request->column => $request->action]);

        return 201;
    }

    public function toggleRecommended(Request $request)
    {
        $category = Category::findOrFail($request->id);

        $category->update(['recommended' => $request->recommended]);

        Cache::forget('bestCategories');

        $bestCategories = Category::where('recommended', true)
                            ->withCount([
                                'users as followers' => function($query) {
                                    $query->where('subscribed', 1);
                                }
                            ])
                            ->orderBy('followers', 'desc')
                            ->get();

        Cache::forever('bestCategories', $bestCategories);

        return 200;
    }

    public function blockedCategories()
    {
        $blocked  = auth()->user()->categories()
                        ->where('blocked', 1)
                        ->get();

        return view('blocked-categories', compact('blocked'));
    }

    public function ownedCategories()
    {
        $owned = auth()->user()
            ->categories()
            ->where('moderator', 1)
            ->get();

        return view('owned-categories', compact('owned'));
    }

}
