<?php

namespace App\Http\Controllers;

class PostFavoritesController extends Controller
{
    /**
     * Toggles 'favorite' on post
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        auth()->user()->favoritePosts()
            ->withTrashed()
            ->toggle($id);

        return response(201);
    }
}
