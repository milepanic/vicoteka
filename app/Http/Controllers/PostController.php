<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\PostRequest;
use App\Http\Responses;
use App\Post;
use App\Services\NotificationService;
use App\Services\PostService;

class PostController extends Controller
{
    /**
     * @var \App\Services\PostService
     */
    protected $postService;

    /**
     * Creates new instance
     *
     * @param \App\Services\PostService $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Shows index page
     * @param string $filter - filter to sort by posts
     * @return \App\Http\Responses\PostIndexResponse
     */
    public function index($filter = null)
    {
        return new Responses\PostIndexResponse($filter);
    }

    public function create()
    {
        //
    }

    /**
     * Creates new post
     *
     * @param  \App\Http\Requests\PostRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $post = $this->postService->create($request);

        // TODO: dodati u event post created
        NotificationService::postCreated($post);

        return response(201);
    }

    /**
     * Shows single post page
     * @param int $id - post's ID
     * @return \App\Http\Responses\PostShowResponse
     */
    public function show($id)
    {
        return new Responses\PostShowResponse($id);
    }

    public function edit(Post $post)
    {
        //
    }

    /**
     * Updates post
     * @param  \App\Http\Requests\PostRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $this->postService->update($request, $id);

        return 200;
    }

    /**
     * Soft deletes post
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->postService->destroy($id);

        return 200;
    }

    /**
     * Restores soft deleted post
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->postService->restore($id);

        return 200;
    }

    // returns post of the day/week/month
    public function bestOf($param)
    {
        $post = Post::getBestOf($param)
                    ->withTrashed()
                    ->eagerLoad()
                    ->first();

        return response()->json($post);
    }

    public function showBestOf($param)
    {
        $string = 'day';
        switch ($param) {
            case 'sedmice':
                $string = 'week';
                break;
            case 'meseca':
                $string = 'month';
                break;
        }

        $post = Post::getBestOf($string)
                    ->withTrashed()
                    ->eagerLoad()
                    ->first();

        $category = Category::where('id', $post->category->id)
                        ->withCount([
                            'users as subscribers' => function ($query) {
                                $query->where('subscribed', 1);
                            }
                        ])->first();

        $moderators = $category->users()
                        ->where('moderator', true)
                        ->get();

        return view('single', compact('post', 'category', 'moderators'));
    }

}
