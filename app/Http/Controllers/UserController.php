<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Responses;
use App\Services\NotificationService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * Creates new instance
     *
     * @param \App\Services\UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display all users that auth user is not following
     *
     * @return \App\Http\Responses\UserIndexResponse
     */
    public function index()
    {
        return new Responses\UserIndexResponse();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return new Responses\UserShowResponse($slug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @return string
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request)
    {
        $this->userService->update($request);

        return response()->json(
            url('profil/' . auth()->user()->slug),
            201
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getFollow()
    {
        $following =
            auth()->user()->following()
                ->get()
                ->pluck('id')
                ->toArray();

        array_push($following, auth()->id());

        $newFollow =
            User::inRandomOrder()
                ->whereNotIn('id', $following)
                ->take(1)
                ->get();

        return response()->json($newFollow);
    }

    public function follow($id)
    {
        if (auth()->id() == $id)
            abort(422);

        $status = auth()->user()->following()
                    ->toggle($id);

        if (! empty(array_get($status, 'attached')))
            NotificationService::userFollowed(User::find($id));

        return 201;
    }

}
