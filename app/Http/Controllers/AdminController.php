<?php

namespace App\Http\Controllers;

use App\Category;
use App\Jobs\OptimizeImage;
use App\Post;
use App\Report;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function users()
    {
        return view('admin.users')->withUsers(User::all());
    }

    public function posts()
    {
        return view('admin.posts')->withPosts(Post::eagerLoad()->get());
    }

    public function stackPosts()
    {
        return view('admin.stack-posts')->with([
            'categories' => Category::all(),
            'users' => User::all(),
        ]);
    }

    public function submitStackPosts(Request $request)
    {
        $postId = DB::table('stack_posts')->insertGetId([
            'body' => $request->body,
            'video' => $request->video,
            'user_id' => $request->user,
            'category_id' => $request->category,
            'nsfw' => $request->nsfw ? true : false, 
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        if ($request->image) {
            $extension = $request->image->getClientOriginalExtension();
            $ext = $extension == 'gif' ? '.gif' : '.jpg';
            $path = 'images/posts/' . uniqid() . $ext;

            if ($ext == '.gif')
                copy($request->image, $path);
            else
                Image::make($request->image)
                    ->save(public_path($path));
            
            dispatch(new OptimizeImage(public_path($path)));

            DB::table('stack_posts')
                ->where('id', $postId)
                ->update(['image' => $path]);
        }

        return back();
    }

    public function categories()
    {
        $categories = Category::all();
        $recommended = Category::where('recommended', true)->get();

        return view('admin.categories', compact('categories', 'recommended'));
    }

    public function medals()
    {
        return view('admin.medals');
    }

    public function reports()
    {
        return view('admin.reports')->withReports(Report::all());
    }
}
