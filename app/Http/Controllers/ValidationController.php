<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ValidationController extends Controller
{
    // TODO: napraviti konstruktor i tu ubaciti Slugify
    public function username(Request $request)
    {
        Log::debug('Validira se korisnicko ime: ' . $request->username);

         $slugify = new Slugify(['rulesets' => ['default', 'serbian']]);

        // Checks if username or slug exists
        // Slug is validated because cyrillic and latin chars 
        // have the same slug, but different name
        $username = User::where('username', $request->username)->exists();
        $userSlug = User::where('slug', $slugify->slugify($request->username))->exists();

        if ($username || $userSlug)
            return "false";

        return "true";
    }

    public function email(Request $request)
    {
        Log::debug('Validira se email adresa: ' . $request->name);

        $email = User::where('email', $request->email)->exists();

        if ($email)
            return "false";

        return "true";
    }

    public function categoryName(Request $request)
    {
    	Log::debug('Validira se ime kategorije: ' . $request->name);

        $slugify = new Slugify(['rulesets' => ['default', 'serbian']]);

    	$category = Category::where('name', $request->name)->exists();
        $categorySlug = Category::where('slug', $slugify->slugify($request->name))->exists();

    	if ($category || $categorySlug)
    		return "false";

		return "true";
    }

    public function image(Request $request)
    {
    	Log::debug('Validira se tip slike');

    	$val = $request->validate([$request->image => 'required|image']);

    	return $val;
    }

    public function editUsername(Request $request)
    {
        Log::debug('Validira se editovanje korisnickog imena: ' . $request->username);

        $slugify = new Slugify(['rulesets' => ['default', 'serbian']]);

        $user = User::where('username', $request->username)->first();
        $userSlug = User::where('slug', $slugify->slugify($request->username))->exists();

        if ($user && Auth::user()->username === $user->username)
            return "true";
        else if ($userSlug || $user)
            return "false";
    }
}
