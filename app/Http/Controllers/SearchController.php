<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SearchController extends Controller
{
    public function ajax(Request $request)
    {
        Log::debug('User is searching for: ' . $request->data);
        
    	$posts = Post::search($request->data)
    		    	->take(4)
    		    	->get();

    	$categories = Category::search($request->data)
    		    	->take(4)
    		    	->get();
				    	
    	$users = User::search($request->data)
    			    ->take(4)
                    ->get();

    	return response()->json([
    		'posts' => $posts, 'categories' => $categories, 'users' => $users
    	]);
    }

    public function categories(Request $request)
    {
        $categories = Category::search($request->data)
                        ->take(6)
                        ->get();

        return response()->json(['categories' => $categories]);
    }
}
