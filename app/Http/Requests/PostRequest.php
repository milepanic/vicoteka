<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //TODO: provjeriti da li je banovan
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        switch ($request->getMethod()) {
            case 'POST':
                if ($request->action === 'image')
                    $rules['image'] = 'required|image';

                if ($request->action === 'video')
                    $rules['video'] = 'required|url';

                $rules['body'] = 'nullable|string';
                $rules['nsfw'] = 'nullable|boolean';

                break;

            case 'PUT':
            case 'PATCH':
                $rules['body'] = 'required|string';
                
                break;
            
            default:
                break;
        }

        return $rules;
    }
}
