<?php

namespace App\Http\Requests;

use App\Category;
use Cocur\Slugify\Slugify;
use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slugify = new Slugify(['rulesets' => ['default', 'serbian']]);

        return [
            'name' => 'required|unique:categories|min:3|max:30',
            'description' => 'nullable|string',
            'croppedImage' => 'required|image',
            $slugify->slugify('name') => 'unique:categories,slug'
        ];
    }
}
