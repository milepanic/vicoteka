<?php

namespace App\Http\Responses;

use App\Post;
use App\User;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Cache;

/**
 * 
 */
class PostIndexResponse implements Responsable
{
    /**
     * Filter to sort by posts
     * 
     * @var string
     */
    protected $filter;

    public function __construct($filter)
    {
        $this->filter = $filter;
    }

    /**
     * Returns data to index page
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        if ($request->ajax()) {
            return view('components.posts')
                ->with('posts', $this->getPosts());
        }

        return view('index', [
            'posts' => $this->getPosts(),
            'bestCategories' => $this->getBestCategories(),
            'bestCategory' => $this->getBestCategory(),
            'recommendedUsers' => $this->getRecommendedUsers()
        ]);
    }

    /**
     * Gets paginated filtered posts
     * 
     * @return App\Post
     */
    protected function getPosts()
    {
        return Post::notBlocked()
                ->withTrashed()
                ->filter($this->filter)
                ->eagerLoad()
                ->simplePaginate(15);
    }

    /**
     * Gets categories cached in admin panel
     * 
     * @return App\Category
     */
    protected function getBestCategories()
    {
        return Cache::get('bestCategories');
    }

    /**
     * Gets one category cached in admin panel
     * 
     * @return App\Category
     */
    protected function getBestCategory()
    {
        return Cache::get('bestCategories')->random();
    }

    /**
     * Gets random recommended users to follow
     * 
     * @return App\User
     */
    protected function getRecommendedUsers()
    {
        if (auth()->guest()) {
            return;
        }

        $following = auth()->user()
                        ->following()
                        ->get()
                        ->pluck('id')
                        ->toArray();

        array_push($following, auth()->id());

        return User::inRandomOrder()
                ->whereNotIn('id', $following)
                ->take(4)
                ->get();
    }
}
