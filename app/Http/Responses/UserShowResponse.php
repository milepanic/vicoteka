<?php

namespace App\Http\Responses;

use App\Services\ProfileParametersService;
use App\Post;
use App\User;
use Illuminate\Contracts\Support\Responsable;

/**
 * Show users profile
 */
class UserShowResponse implements Responsable
{
    /**
     * @var App\User
     */
    protected $user;

    function __construct($slug)
    {
        $this->user = $this->getUser($slug);
    }

    /**
     * Returns user's all posts or if there is parameter 'prikaz'
     * then return choosen parameter 
     * 
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        if ($request->has('prikaz')) {
            return ProfileParametersService::getParameter($request->prikaz, $this->user);
        }

        return view('profile', [
            'user' => $this->user, 
            'posts' => $this->getPosts()
        ]);
    }

    /**
     * @param string $slug
     * @return App\User
     */
    protected function getUser($slug)
    {
        return User::where('slug', $slug)
            ->withCount([
                'posts',
                'comments',
                'followers as followers',
                'following as following',
                'categories as subscribed' => function ($query) {
                    $query->whereRaw('category_user.user_id = users.id')->where('subscribed', 1);
                },
                'categories as moderator' => function ($query) {
                    $query->whereRaw('category_user.user_id = users.id')->where('moderator', 1);
                },
                'favoritePosts as favorites' => function ($query) {
                    $query->whereRaw('favorites.user_id = users.id');
                },
                'postVote as upvoted' => function ($query) {
                    $query->whereRaw('votes.user_id = users.id')->where('vote', 1);
                },
                'postVote as downvoted' => function ($query) {
                    $query->whereRaw('votes.user_id = users.id')->where('vote', -1);
                }
            ])->firstOrFail();
    }

    /**
     * Gets all user's posts
     * 
     * @return App\Post
     */
    protected function getPosts()
    {
        return Post::where('user_id', $this->user->id)
                ->withTrashed()
                ->eagerLoad()
                ->latest()
                ->simplePaginate(15);
    }
}
