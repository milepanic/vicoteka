<?php

namespace App\Http\Responses;

use App\Category;
use App\Post;
use Illuminate\Contracts\Support\Responsable;

/**
 * 
 */
class PostShowResponse implements Responsable
{
    /**
     * User ID
     * 
     * @var int
     */
    protected $id;

    /**
     * @var App\Post
     */
    protected $post;

    function __construct($id)
    {
        $this->id = $id;
        $this->post = $this->getPost();
    }

    /**
     * Returns data to single post page
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        return view('single', [
            'post' => $this->post, 
            'category' => $this->getCategory()
        ]);
    }

    /**
     * Gets post by id
     * 
     * @return App\Post
     */
    public function getPost()
    {
        return Post::eagerLoad()
                ->withTrashed()
                ->findOrFail($this->id);
    }

    /**
     * Gets category of post with moderators and number of subscribers
     * 
     * @return App\Category
     */
    public function getCategory()
    {
        return Category::where('id', $this->post->category->id)
                ->with(['users' => function ($query) {
                    $query->where('moderator', true);
                }])->withCount(['users as subscribers' => function ($query) {
                    $query->where('subscribed', 1);
                }])->first();
    }
}
