<?php

namespace App\Http\Responses;

use App\Comment;
use Illuminate\Contracts\Support\Responsable;

/**
 * 
 */
class CommentIndexResponse implements Responsable
{
    /**
     * @var int
     */
    protected $id;

    function __construct($id)
    {
        $this->id = $id;
    }

    public function toResponse($request)
    {
        return response()->json($this->getComments());
    }

    protected function getComments()
    {
        return Comment::where('post_id', $this->id)
            ->with('user')
            ->withCount(['votes as upvotes_count' => function ($query) {
                $query->where('vote', 1);
            }, 'votes as downvotes_count' => function ($query) {
                $query->where('vote', -1);
            }])
            ->latest()
            ->get();
    }
}
