<?php

namespace App\Http\Responses;

use App\Category;
use App\Post;
use Illuminate\Contracts\Support\Responsable;

/**
 * Shows single category page
 */
class CategoryShowResponse implements Responsable
{
    /**
     * @var App\Category
     */
    protected $category;

    /**
     * @var string
     */
    protected $filter;

    function __construct($slug, $filter)
    {
        $this->category = $this->getCategory($slug);
        $this->filter = $filter;
    }

    /**
     * Returns category and all it's posts and moderators
     * 
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        if ($request->ajax()) {
            return view('components.posts')
                ->with('posts', $this->getPosts());
        }

        return view('category', [
            'category' => $this->category, 
            'posts' => $this->getPosts()
        ]);
    }

    /**
     * @param string $slug
     * @return App\Category
     */
    protected function getCategory($slug)
    {
        return Category::where('slug', $slug)
                ->with(['users' => function ($query) {
                    $query->where('moderator', true);
                }])->withCount(['users as subscribers' => function ($query) {
                    $query->where('subscribed', 1);
                }])->firstOrFail();
    }

    /**
     * Gets all category posts
     * 
     * @return App\Post
     */
    protected function getPosts()
    {
        return Post::where('category_id', $this->category->id)
                ->filter($this->filter)
                ->withTrashed()
                ->eagerLoad()
                ->simplePaginate(15);
    }
}
