<?php

namespace App\Http\Responses;

use App\Category;
use App\User;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Cache;

/**
 * 
 */
class CategoryIndexResponse implements Responsable
{
    /**
     * @var string
     */
    protected $filter;

    public function __construct($filter)
    {
        $this->filter = $filter;
    }

    /**
     * Gets all categories
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        if ($request->ajax()) {
            return view('components.categories')
                ->with('categories', $this->getCategories());
        }

        $bestCategories = Cache::get('bestCategories');
        $bestCategory = Cache::get('bestCategories')->random();

        if (auth()->check()) {
            $following = auth()->user()
                    ->following()
                    ->get()
                    ->pluck('id')
                    ->toArray();

            array_push($following, auth()->id());

            $recommendedUsers = User::inRandomOrder()
                            ->whereNotIn('id', $following)
                            ->take(4)
                            ->get();
        }

        return view('discover-categories', [
            'categories' => $this->getCategories(), 
            'bestCategories' => $bestCategories, 
            'bestCategory' => $bestCategory, 
            'recommendedUsers' => $recommendedUsers
        ]);
    }

    /**
     * Gets all categories
     * 
     * @return App\Category
     */
    protected function getCategories()
    {
        return Category::withCount([
                    'users as subscribers' => function ($query) {
                        $query->where('subscribed', 1);
                    }
                ])
                ->filter($this->filter)
                ->simplePaginate(15);
    }
}
