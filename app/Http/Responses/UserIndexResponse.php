<?php

namespace App\Http\Responses;

use App\User;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Cache;

/**
 * 
 */
class UserIndexResponse implements Responsable
{
    public function toResponse($request)
    {
        $bestCategories = Cache::get('bestCategories');
        $bestCategory = Cache::get('bestCategories')->random();

        if (auth()->check()) {
            $following = auth()->user()->following()
                                ->get()
                                ->pluck('id')
                                ->toArray();

            array_push($following, auth()->id());

            $recommendedUsers = User::inRandomOrder()
                                    ->whereNotIn('id', $following)
                                    ->take(4)
                                    ->get();
        }

        return view('discover-users', [
            'users' => $this->getUsers(), 
            'bestCategories' => $bestCategories, // TODO: napraviti servis za ovo
            'bestCategory' => $bestCategory, 
            'recommendedUsers' => $recommendedUsers
        ]);
    }

    /**
     * Gets users to follow
     * 
     * @return App\User
     */
    protected function getUsers()
    {
        $following = auth()->user()
                        ->following()
                        ->get()
                        ->pluck('id')
                        ->toArray();

        return User::inRandomOrder()
                ->whereNotIn('id', $following)
                ->withCount('followers as followers')
                ->simplePaginate(15);
    }
}
