<?php

namespace App\Console;

use App\Post;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('bestof:day')->daily();
        $schedule->command('bestof:week')->weekly();
        $schedule->command('bestof:month')->monthly();

        $schedule->command('backup:clean --disable-notifications')->weekly();
        $schedule->command('backup:run --disable-notifications')->weekly();

        $schedule->command('stacked:publish')->cron('0 */3 * * *'); // every 3 hours
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
