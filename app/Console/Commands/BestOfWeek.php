<?php

namespace App\Console\Commands;

use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BestOfWeek extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bestof:week';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the best post of the week based on upvotes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::notice('Weekly task is running');
        Post::where('best_of', 'week')->update(['best_of' => null]);
        Log::info('Removed old best_of_week');

        $bestOfWeek = Post::bestOf('week')
                        ->eagerLoad()
                        ->orderBy('upvotes_count', 'desc')
                        ->first();

        $bestOfWeek->best_of = 'week';
        $bestOfWeek->save();

        DB::table('best_of')
            ->insert([
                'post_id' => $bestOfWeek->id,
                'type' => 'week',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

        Log::info('New best_of_week is post by id: ' . $bestOfWeek->id);
    }
}
