<?php

namespace App\Console\Commands;

use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BestOfMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bestof:month';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the best post of the month based on upvotes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::notice('Monthly task is running');
        Post::where('best_of', 'month')->update(['best_of' => null]);
        Log::info('Removed old best_of_month');

        $bestOfMonth = Post::bestOf('month')
                        ->eagerLoad()
                        ->orderBy('upvotes_count', 'desc')
                        ->first();

        $bestOfMonth->best_of = 'month';
        $bestOfMonth->save();

        DB::table('best_of')
            ->insert([
                'post_id' => $bestOfMonth->id,
                'type' => 'month',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

        Log::info('New best_of_month is post by id: ' . $bestOfMonth->id);
    }
}
