<?php

namespace App\Console\Commands;

use App\Post;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PublishStackedPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stacked:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moves post from stacked_posts table to posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $post = DB::table('stack_posts')
                        ->first();

            $newPost = Post::create([
                'body' => $post->body,
                'video' => $post->video,
                'image' => $post->image,
                'user_id' => $post->user_id,
                'category_id' => $post->category_id,
                'nsfw' => $post->nsfw,
            ]);

            User::where('id', $post->user_id)
                ->first()
                ->postVote()
                ->attach($newPost->id, ['vote' => 1]);

            DB::table('stack_posts')
                ->where('id', $post->id)
                ->delete();
        } catch (\Exception $e) {
            Log::alert('No more stacked posts');
        }
    }
}
