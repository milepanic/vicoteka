<?php

namespace App\Console\Commands;

use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BestOfDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bestof:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the best post of the day based on upvotes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::notice('Daily task is running');
        Post::where('best_of', 'day')->update(['best_of' => null]);
        Log::info('Removed old best_of_day');

        $bestOfToday = Post::bestOf('day')
                        ->eagerLoad()
                        ->orderBy('upvotes_count', 'desc')
                        ->first();

        $bestOfToday->best_of = 'day';
        $bestOfToday->save();

        DB::table('best_of')
            ->insert([
                'post_id' => $bestOfToday->id,
                'type' => 'day',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

        Log::info('New best_of_day is post by id: ' . $bestOfToday->id);
    }
}
