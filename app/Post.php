<?php

namespace App;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;

class Post extends Model
{
    use Searchable;
    use SoftDeletes;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['id', 'body']);
    }

    protected $fillable = [
    	'body', 'image', 'video', 'nsfw', 'best_of', 'category_id', 'user_id', 'created_at'
    ];

    protected $appends = ['diffCreatedAt'];

    protected $dates = ['deleted_at'];

    public function getDiffCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
    
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function votes()
    {
        return $this->belongsToMany('App\User', 'votes')
            ->withPivot('vote')
            ->withTimestamps();
    }

    public function votedBy(User $user)
    {
        return $this->votes()->where('user_id', $user->id)->pluck('vote');
    }

    public function favorites()
    {
        return $this->belongsToMany('App\User', 'favorites');
    }

    public function scopeEagerLoad($query)
    {
        return $query
            ->with(['user', 'category', 'votes', 'favorites'])
            ->withCount([
                'favorites',
                'comments',
                'votes as upvotes_count' => function ($query) {
                    $query->where('vote', 1);
                },
                'votes as downvotes_count' => function ($query) {
                    $query->where('vote', -1);
                }
            ]);
    }

    public function scopeNotBlocked($query)
    {
        if(Auth::guest()) return;
        
        $blocked = 
            Auth::user()
                ->categories()
                ->where('blocked', 1)
                ->get()
                ->pluck(['id'])
                ->toArray();

        return $query->whereNotIn('category_id', $blocked);                    
    }

    public function scopeFilter($query, $filter)
    {
        switch ($filter) {
            case 'novo':
                return $query->latest();
                break;
            case 'top':
                return $query->orderBy('upvotes_count', 'desc');
                break;
            case 'kategorije':
                if(auth()->guest()) return; 
                
                $subs = auth()->user()->categories()
                            ->where('subscribed', 1)
                            ->get()
                            ->pluck('id')
                            ->toArray();

                return $query->whereIn('category_id', $subs)
                            ->latest();

                break;
            case 'korisnici':
                if(auth()->guest()) return; 
                
                $subs = auth()->user()->following()
                            ->get()
                            ->pluck('id')
                            ->toArray();

                return $query->whereIn('user_id', $subs)
                            ->latest();
                break;
            // case: popularno
            default:
                /*
                 * SEE: https://medium.com/hacking-and-gonzo/how-hacker-news-ranking-algorithm-works-1d9b0cf2c08d
                 * 0.1 is when upvotes and downvotes are the same count,
                 * so it wont be 0/POWER.... which would be 0
                 */ 
                return $query->orderByRaw(
                    '((upvotes_count - downvotes_count + 0.1) / POWER(TIMEDIFF(NOW(), created_at) + 2, 1.8)) desc'
                );
        }
    }

    // obrisati?
    public function scopeBestOf($query, $param)
    {
        switch ($param) {
            case 'day':
                return $query->whereBetween(
                    'created_at', [Carbon::yesterday(), Carbon::today()]
                );
                break;
            case 'week':
                return $query->whereBetween(
                    'created_at', [Carbon::now()->startOfWeek()->subWeek(), Carbon::now()->endOfWeek()->subWeek()]
                );
                break;
            case 'month':
                return $query->whereBetween(
                    'created_at', [Carbon::now()->startOfMonth()->subMonth(), Carbon::now()->endOfMonth()->subMonth()]
                );
                break;
        }
    }

    public function scopeGetBestOf($query, $param)
    {
        switch ($param) {
            case 'day':
                return $query->where('best_of', 'day');
                break;
            case 'week':
                return $query->where('best_of', 'week');
                break;
            case 'month':
                return $query->where('best_of', 'month');
                break;
        }
    }
}
