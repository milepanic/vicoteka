<?php

namespace App;

use App\Notifications\MailResetPasswordToken;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;

class User extends Authenticatable
{
    use Searchable;
    use Notifiable;
    use CanResetPassword;
    use SoftDeletes;

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['id', 'username', 'slug']);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'slug', 'email', 'password', 'provider', 'provider_id', 'description', 'has_image', 'image_sm', 'image_lg', 'facebook', 'twitter', 'instagram', 'tumblr', 'nsfw', 'last_commented_at', 'last_posted_at', 'last_created_category_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'email', 'admin', 'password', 'remember_token',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function postVote()
    {
        return $this->belongsToMany('App\Post', 'votes')
            ->withPivot('vote')
            ->withTimestamps();
    }

    public function commentVote()
    {
        return $this->belongsToMany('App\Comment', 'comments_votes')
            ->withPivot('vote');
    }

    
    public function favoritePosts()
    {
        return $this->belongsToMany('App\Post', 'favorites');
    }

    public function following()
    {
        return $this->belongsToMany('App\User', 'follow', 'follower_id', 'followed_id');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'follow', 'followed_id', 'follower_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category')
            ->withPivot('subscribed', 'blocked', 'moderator');
    }

}
