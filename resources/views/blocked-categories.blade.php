@extends('layouts/master')
@section('info')
    <title>Blokirane kategorije | Distopija</title>
    <meta name="robots" content="noindex">
@endsection
@section('content')
@include('components/header')

<div class="mx-auto w-160 bg-white rounded shadow">
@forelse($blocked as $category)
	<div class="blocked-category-box py-4 px-8 border-b flex">
		<div class="w-1/3">
			<a href="{{ url('kategorija/' . $category->slug) }}">
            	<img class="w-48 h-48" src="{{ asset($category->image) }}">
        	</a>
		</div>
    	<div class="w-2/3 pl-4 relative">
        	<a class="text-lg font-bold" href="{{ url('kategorija/' . $category->slug) }}">{{ $category->name }}</a>
        	<p class="text-grey-darkest text-sm mb-8">{{ Illuminate\Support\Str::limit($category->description, 420) }}</p>
        	<div class="alignt-bottom absolute pin-b pin-r">
                <button class="unblock-btn bg-blue text-white mr-4 text-sm py-2 px-4 font-bold rounded shadow" data-id="{{ $category->id }}">Odblokiraj</button>
        	</div>
    	</div>
    </div>
@empty
	<div class="flex items-center p-8">
		<img src="{{ asset('images/warning.png') }}" alt="Upozorenje">
		<p class="ml-8 text-lg text-grey-darkest">
			Nemate ni jednu blokiranu kategoriju.
		</p>
	</div>
@endforelse
</div>

@endsection
