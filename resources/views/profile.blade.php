@extends('layouts/master')
@section('info')
    <title>{{ $user->username }} | Distopija</title>

    <meta name="description" content="{{ Illuminate\Support\Str::limit($user->description, 200) }}">
    <link rel="canonical" href="{{ url('profil/' . $user->slug) }}">

    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:title" content="{{ $user->username }} | Distopija" />
    <meta property="og:description" content="{{ Illuminate\Support\Str::limit($user->description, 200) }}" />
    <meta property="og:image" content="{{ asset($user->image_lg) }}" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="800">
    <meta property="og:image:type" content="image/jpg" />
    <meta property="fb:app_id" content="1900990443537383">
    <meta property="og:locale" content="sr" />

    <meta name="twitter:description" content="{{ Illuminate\Support\Str::limit($user->description, 200) }}" />
    <meta name="twitter:url" content="{{ Request::url() }}" />
    <meta name="twitter:image" content="{{ asset($user->image_lg) }}" />
@endsection
@section('content')
@include('components/responsive/profile')
@include('components/responsive/sidebar-profile')
@include('components/header')

@if(auth()->user() && auth()->id() === $user->id)
<div class="modal absolute z-50" id="modal-edit-profile" hidden>
    @include('components/modal-edit-profile')
</div>
@endif

<div class="container mx-auto mb-8 md:flex" data-id="{{ $user->id }}">

    <div class="hidden xl:block xl:w-1/4">
        <img class="rounded-full border-4 border-white shadow" src="{{ asset($user->image_lg) }}" alt="{{ $user->username }}"> 

        <div class="py-4 mb-2">
            <h2 class="mb-3 text-center">{{ $user->username }}</h2>
            <p class="text-grey-darkest text-sm">{{ $user->description }}</p>
        </div>

        @auth
            @if($user->id !== auth()->id())
                @if(auth()->user()->following->contains($user->id))
                    {{-- follow btn jquery selector --}}
                    <button class="follow-btn following py-3 px-8 w-full uppercase border-red rounded shadow border font-medium tracking-wide">Pratite</button>
                @else
                    <button class="follow-btn follow py-3 px-8 w-full uppercase border-red rounded shadow border font-medium tracking-wide">Zaprati</button>
                @endif
            @else
                <button class="open-edit-profile-modal py-3 px-8 w-full text-grey-darker border-grey-darker uppercase rounded shadow border font-medium tracking-wide">Izmeni profil</button>
            @endif
        @endauth
        
        <div class="mt-6 flex justify-around">
            @if($user->facebook)
            <a href="{{ $user->facebook }}" target="_blank">
                <i class="fab fa-facebook-f fa-2x"></i>
            </a>
            @endif
            @if($user->twitter)
            <a href="{{ $user->twitter }}">
                <i class="fab fa-twitter fa-2x"></i>
            </a>
            @endif
            @if($user->instagram)
            <a href="{{ $user->instagram }}">
                <i class="fab fa-instagram fa-2x"></i>
            </a>
            @endif
            @if($user->tumblr)
            <a href="{{ $user->tumblr }}">
                <i class="fab fa-tumblr fa-2x"></i>
            </a>
            @endif
        </div>
        @if(auth()->user() && $user->id === auth()->id())
        <div class="mt-2 text-center">
            <a class="text-xs text-blue" href="{{ url('blokirane-kategorije') }}">Blokirane kategorije</a>
        </div>
        @endif
    </div>
    

    <div class="mx-auto xl:mx-0 xl:ml-4">
        <div class="md:w-160 bg-white rounded shadow">

            @include('components/navigation/profile')

            @isset($posts)
                @include('components/posts')
            @endisset

            @isset($categories)
                @include('components/categories')
            @endisset

            @isset($comments)
                @include('components/comments')
            @endisset

            @isset($users)
                @include('components/users')
            @endisset

        </div>
    </div>
    
    <div class="ml-4 hidden xl:block xl:w-1/4">
        @include('components/sidebar-profile')
    </div>
</div>

@endsection

