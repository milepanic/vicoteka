@extends('layouts/master')
@section('info')
    <title>Otkrijte korisnike | Distopija</title>
    
    <meta name="robots" content="noindex">
@endsection
@section('content')
@include('components/header')

<div class="container mx-auto mb-8">
    <div class="flex xl:justify-center">
        <div class="mx-auto xl:mx-0">
            <div class="mx-auto xl:mx-0 lg:w-160 bg-white rounded shadow">
                @include('components/navigation/users')
                @include('components/users')
            </div>
        </div>

        <div class="ml-8 w-full hidden xl:block xl:w-1/4">
            @include('components/sidebar-main')
        </div>

    </div>
</div>

@endsection
