@extends('layouts/master')
@section('info')

<title>Moderator | {{ $category->name }} | Distopija</title>
<meta name="robots" content="noindex" />

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.jqueryui.min.css">
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.jqueryui.min.js"></script>

@endsection
@section('content')
@include('components/header')

<div class="container mx-auto flex justify-center">
    <div class="w-80 mr-8">
        <a class="block text-center bg-orange-dark hover:bg-orange text-grey-lighter text-sm font-bold uppercase tracking-wide py-3 rounded shadow mb-4" href="{{ url('kategorija/' . $category->slug) }}">Nazad</a>
        <div class="bg-white rounded shadow px-4 py-2">
            <ul class="list-reset">
                <a href="{{ url('kategorija/' . $category->slug . '/moderator?prikaz=sve-objave') }}">
                    <li class="py-2 px-2 text-grey-darkest hover:bg-grey-lighter cursor-pointer">Sve objave</li>
                </a>
                <a href="{{ url('kategorija/' . $category->slug . '/moderator?prikaz=prijave') }}">
                    <li class="py-2 px-2 text-grey-darkest hover:bg-grey-lighter cursor-pointer">Prijave</li>
                </a>
                <a href="{{ url('kategorija/' . $category->slug . '/moderator?prikaz=edit') }}">
                    <li class="py-2 px-2 text-grey-darkest hover:bg-grey-lighter cursor-pointer">Edit kategorije</li>
                </a>
                <a href="{{ url('kategorija/' . $category->slug . '/moderator?prikaz=ban') }}">
                    <li class="py-2 px-2 text-grey-darkest hover:bg-grey-lighter cursor-pointer">Banovanje korisnika</li>
                </a>
                <a href="{{ url('kategorija/' . $category->slug . '/moderator?prikaz=dodavanje') }}">
                    <li class="py-2 px-2 text-grey-darkest hover:bg-grey-lighter cursor-pointer">Dodaj moderatora</li>
                </a>
            </ul>
        </div>
    </div>
    <div class="w-full">
        <div class="bg-white rounded shadow py-4 px-8">
            @includeWhen(request()->input('prikaz') === 'sve-objave', 'components/category-settings/all-posts')
            @includeWhen(request()->input('prikaz') === 'prijave', 'components/category-settings/reports')
            @includeWhen(request()->input('prikaz') === 'edit', 'components/category-settings/edit')
            @includeWhen(request()->input('prikaz') === 'ban', 'components/category-settings/ban')
            @includeWhen(request()->input('prikaz') === 'dodavanje', 'components/category-settings/add')
        </div>
    </div>
</div>

@endsection

@section('external-js')

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<!-- DataTables JavaScript -->
<script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('#dataTable').DataTable();
    });
</script>
<script src="{{ asset('js/category-settings.js') }}"></script>

@endsection