@extends('layouts/master')
@section('info')
    <title>{{ Illuminate\Support\Str::limit($post->body, 35) }} - {{ $post->category->name }} | Distopija</title>
    <link rel="canonical" href="{{ url('objava/' . $post->id) }}">

    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:title" content="{{ Illuminate\Support\Str::limit($post->body, 50) }}" /> {{--Najbolji vic <tip> <datum> | Distopija--}}
    <meta property="og:description" content="{{ $post->diffCreatedAt }} - objavio {{ $post->user->username }}, u kategoriji {{ $post->category->name }}" />
    <meta property="og:image" content="{{ asset('images/fb-image.jpg') }}" /> {{-- mozda slika pocetne stranice --}}
    <meta property="og:image:width" content="1200"> {{-- 1200x630, 1.91:1 ratio - preporucljivo --}}
    <meta property="og:image:height" content="630">
    <meta property="og:image:type" content="image/jpg" />
    <meta name="author" content="{{ $post->user->username }}" />
    <meta property="fb:app_id" content="">
    <meta property="og:locale" content="sr" />

    <meta name="twitter:description" content="Your 200-character description here" />
    <meta name="twitter:url" content="http://www.yourdomain.com" />
    <meta name="twitter:image" content="http://www.yourdomain.com /image-name.jpg" />
@endsection
@section('content')
@include('components/header')

<div class="container mx-auto font-sans mb-8">
    <div class="flex xl:justify-center">
        <div class="mx-auto xl:mx-0">
            <div class="mx-auto xl:mx-0 xl:w-160 bg-white rounded shadow">

                @include('components/post')
                
            </div>
        </div>
        <div class="ml-8 w-full hidden xl:block xl:w-1/4">
            @include('components/sidebar-category')
        </div>

    </div>
</div>

@endsection