@extends('layouts/master')
@section('info')
    <title>Otkrijte kategorije | Distopija</title>
    
    <meta name="description" content="Prikaz svih kategorija koje su korisnici pravili na distopji">
    <link rel="canonical" href="{{ url('otkrijte-kategorije') }}">
@endsection
@section('content')
@include('components/header')

<div class="container mx-auto mb-8">
    <div class="flex lg:justify-center">
        <div class="mx-auto xl:mx-0">
            <div class="mx-auto lg:mx-0 lg:w-160 bg-white rounded shadow">
                @include('components/navigation/categories')
                @include('components/categories')               
            </div>
        </div>

        <div class="ml-8 w-full hidden lg:block lg:w-1/4">
            @include('components/sidebar-main')
        </div>

    </div>
</div>

@endsection
