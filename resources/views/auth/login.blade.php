@extends('layouts.master')
@section('info')
    <title>Prijava | Distopija</title>

    <meta name="description" content="Prijavite se na naš sajt i moći ćete objavljivati novi sadržaj, glasati na objave i pratiti kategorije i korisnike">
    <link rel="canonical" href="{{ route('login') }}">
@endsection
@section('content')
@include('components/header-clean')

<form class="max-w-sm mx-auto bg-white rounded shadow p-4 font-sans border-t-4 border-blue" method="POST" action="{{ route('login') }}">
    @csrf
    <h3 class="text-grey-darkest uppercase mb-6 tracking-wide">Prijavite se</h3>
    <div class="mb-4">
        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Email</label>
        <input id="email" class="w-full text-sm bg-grey-lightest px-4 py-3 border border-grey-light" type="email" name="email" placeholder="Unesite vašu email adresu" required autofocus>
    </div>
    <div class="relative mb-4">
        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Lozinka</label>
        <a class="absolute pin-r pin-t text-xs text-blue " href="{{ route('password.request') }}">
            Zaboravili ste lozinku?
        </a>
        <input id="password" class="w-full text-sm bg-grey-lightest px-4 py-3 border border-grey-light" type="password" name="password" placeholder="Unesite lozinku" required>
        @if($errors->has('email') || $errors->has('password'))
            <span class="text-xs text-red">
                <strong>Neispravan email ili lozinka</strong>
            </span>
        @endif
    </div>
    <div class="mb-8">
        <div class="pretty p-curve p-default p-pulse">
            <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}/>
            <div class="state p-primary-o">
                <label class="text-grey-darker">Zapamti me</label>
            </div>
        </div>
    </div>
    <button class="w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-white font-semibold text-sm tracking-wide" type="submit">Prijavite se</button>
    <div class="border-t mt-8 pt-8">
        <p class="text-grey-darkest text-sm mb-4 text-center">Prijavite se preko socijalnih mreža</p>
        <div class="flex justify-between">
            <a href="{{ url('/auth/facebook') }}" class="border rounded hover:border-blue-dark px-2 sm:px-4 py-2 w-1/3 mr-2">
                <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-blue-dark fab fa-facebook-f"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Facebook</span>
            </a>
            <a href="{{ url('/auth/google') }}" class="border rounded hover:border-red px-2 sm:px-4 py-2 w-1/3 mr-2">
                <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-red fab fa-google"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Google</span>
            </a>
            <a href="{{ url('/auth/twitter') }}" class="border rounded hover:border-blue px-2 sm:px-4 py-2 w-1/3">
                <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-blue fab fa-twitter"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Twitter</span>
            </a>
        </div>
        <p class="text-grey-darker text-xs mt-4">*Vaši privatni podaci su sigurni: www.distopija.com ih neće zloupotrebljavati niti slati trećim partijama</p>
    </div>
</form>

@endsection
