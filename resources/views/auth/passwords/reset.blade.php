@extends('layouts.master')
@section('info')
    <title>Resetovanje lozinke | Distopija</title>
    
    <meta name="robots" content="noindex">
@endsection
@section('content')
@include('components/header-clean')

<form class="max-w-sm mx-auto bg-white rounded shadow p-4 font-sans border-t-4 border-blue" method="POST" action="{{ route('password.request') }}">
    @csrf
    <input type="hidden" name="token" value="{{ $token }}">
    <div class="mb-4">
        <label for="email" class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Email</label>
        <input id="email" type="email" class="w-full bg-grey-lightest px-4 py-3 border border-grey-light{{ $errors->has('email') ? ' text-red' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>

        @if ($errors->has('email'))
            <span class="text-xs text-red">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="mb-4">
        <label for="password" class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Nova lozinka</label>
        <input id="password" type="password" class="w-full bg-grey-lightest px-4 py-3 border border-grey-light{{ $errors->has('password') ? ' text-red' : '' }}" name="password" required>

        @if ($errors->has('password'))
            <span class="text-xs text-red">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="mb-8">
        <label for="password-confirm" class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Potvrdite lozinku</label>
        <input id="password-confirm" type="password" class="w-full bg-grey-lightest px-4 py-3 border border-grey-light{{ $errors->has('password_confirmation') ? ' border-red' : '' }}" name="password_confirmation" required>

        @if ($errors->has('password_confirmation'))
            <span class="text-xs text-red">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
    </div>

    <button type="submit" class="w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-white font-semibold text-sm tracking-wide">Resetujte lozinku</button>
</form>
@endsection
