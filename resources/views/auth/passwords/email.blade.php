@extends('layouts.master')
@section('info')
    <title>Zaboravljena lozinka | Distopija</title>
@endsection
@section('content')
@include('components/header-clean')

<form class="max-w-sm mx-auto bg-white rounded shadow p-4 font-sans border-t-4 border-blue" method="POST" action="{{ route('password.email') }}">
    @csrf
    <h3 class="text-grey-darkest uppercase mb-6 tracking-wide">Resetujte lozinku</h3>
    <div class="mb-8">
        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Email</label>
        <input id="email" class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" type="email" name="email" value="{{ old('email') }}" placeholder="Unesite email" required autofocus>
    </div>
    <button class="w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-white font-semibold text-sm tracking-wide" type="submit">Pošalji</button>
</form>

@endsection
