@extends('layouts/master')
@section('info')
    <title>{{ Request::segment(1) ? ucwords(Request::path()) . ' | Distopija' : 'Distopija: prozor u humor'}}</title>
    @if (Request::is(['popularno', '/']))
    <meta name="description" content="Čuo si dobar vic? Video zanimljiv grafit, dobar mim ili ultra smešan video? Podeli ga sa nama baš ovde, na Distopiji!">
    <link rel="canonical" href="{{ url('/') }}">
    @elseif (Request::is('novo'))
    <meta name="description" content="Najnovije objave koje se nalaze na sajtu">
    <link rel="canonical" href="{{ url('novo') }}">
    @elseif (Request::is('top'))
    <meta name="description" content="Najbolje objave ocenjene od strane korisnika">
    <link rel="canonical" href="{{ url('top') }}">
    @endif

    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:title" content="{{ config('app.name') }}" />
    <meta property="og:description" content="Čuo si dobar vic? Video zanimljiv grafit, dobar mim ili ultra smešan video? Podeli ga sa nama baš ovde, na Distopiji!" />
    <meta property="og:image" content="{{ asset('images/fb-image.jpg') }}" /> {{-- mozda slika pocetne stranice --}}
    <meta property="og:image:width" content="1200"> {{-- 1200x630, 1.91:1 ratio - preporucljivo --}}
    <meta property="og:image:height" content="630">
    <meta property="og:image:type" content="image/jpg" />
    <meta property="fb:app_id" content="1900990443537383">
    <meta property="og:locale" content="sr" />

    <meta name="twitter:description" content="Čuo si dobar vic? Video zanimljiv grafit, dobar mim ili ultra smešan video? Podeli ga sa nama baš ovde, na Distopiji!" />
    <meta name="twitter:url" content="{{ Request::url() }}" />
    <meta name="twitter:image" content="{{ asset('images/fb-image.jpg') }}" />
@endsection
@section('content')
@include('components/sidebar-main-responsive')
@include('components/header')

<div class="modal absolute z-50" id="modal-bestOf-post" hidden>
    @include('components/modal-bestOf-post')
</div>

<div class="container mx-auto mb-8">
    <div class="flex xl:justify-center">
        <div class="mx-auto xl:mx-0">

            <div class="md:w-160 bg-white rounded shadow">
                @include('components/navigation/index')
                @include('components/posts')
            </div>
        </div>

        <div class="ml-8 w-full hidden xl:block xl:w-1/4">
            @include('components/sidebar-main')
        </div>

    </div>
</div>

@endsection
