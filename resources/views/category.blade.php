@extends('layouts/master')
@section('info')
    <title>{{ $category->name }} | Distopija</title>

    <meta name="description" content="{{ Illuminate\Support\Str::limit($category->description, 200) }}">
    <link rel="canonical" href="{{ url('kategorija/' . $category->slug) }}">

    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:title" content="{{ $category->name }}" />
    <meta property="og:description" content="{{ Illuminate\Support\Str::limit($category->description, 200) }}" />
    <meta property="og:image" content="{{ asset($category->image) }}" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="800">
    <meta property="og:image:type" content="image/jpg" />
    <meta property="fb:app_id" content="1900990443537383">
    <meta property="og:locale" content="sr" />

    <meta name="twitter:description" content="{{ Illuminate\Support\Str::limit($category->description, 200) }}" />
    <meta name="twitter:url" content="{{ Request::url() }}" />
    <meta name="twitter:image" content="{{ asset($category->image) }}" />
@endsection
@section('content')
@include('components/sidebar-category-responsive')
@include('components/header')
<div class="modal absolute z-50" id="modal-submit" hidden>
    @include('components/modal-submit')
</div>
<div class="container mx-auto mb-8">
    <div class="flex xl:justify-center">
        <div class="mx-auto xl:mx-0">
            
            <div class="w-full md:w-160 bg-white rounded shadow">
                @include('components/navigation/category')
                @include('components/posts')
            </div>
        </div>

        <div class="ml-8 w-full hidden xl:block xl:w-1/4">
            @include('components/sidebar-category')
        </div>

    </div>
</div>

@endsection
