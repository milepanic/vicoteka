{{-- Create category modal --}}
<div class="fixed pin overflow-auto bg-smoke-darker flex py-4">
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-md m-auto flex-col flex border-t-4 border-blue">

        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <form class="px-1 md:px-4 py-4" id="form-create">
            <h3 class="text-grey-darkest uppercase mb-6 tracking-wide">Napravite novu kategoriju</h3>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Ime</label>
                <input id="cat-name" class="validate-input w-full bg-grey-lightest px-4 py-3 border border-grey-light" type="text" name="name" placeholder="Unesite ime kategorije" autofocus>
            </div>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Opis</label>
                <textarea id="cat-description" class="w-full bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" name="description" rows="7" placeholder="Kratak opis kategorije i njena pravila"></textarea>
            </div>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Slika</label>
                <input class="w-full" type="file" name="image" id="upload-cat-img" accept="image/*">
                <img class="mt-4" id="display-cat-img" src="">
            </div>
            <div class="mb-8">
                <div class="flex flex-col mb-4">
                    <div class="pretty p-curve p-default p-pulse mb-3">
                        <input id="cat-nsfw" type="checkbox"/>
                        <div class="state p-primary-o">
                            <label>18+</label>
                        </div>
                    </div>
                    <div class="pretty p-curve p-default p-pulse mb-3">
                        <input id="cat-pictures" type="checkbox"/>
                        <div class="state p-primary-o">
                            <label>Slike</label>
                        </div>
                    </div>
                    <div class="pretty p-curve p-default p-pulse mb-3">
                        <input id="cat-videos" type="checkbox"/>
                        <div class="state p-primary-o">
                            <label>Video klipovi</label>
                        </div>
                    </div>
                    <div class="pretty p-curve p-default p-pulse mb-3">
                        <input id="cat-mods" type="checkbox"/>
                        <div class="state p-primary-o">
                            <label>Samo moderatori</label>
                        </div>
                    </div>
                </div>
            </div>
            <button id="btn-create-category" class="w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit" data-disabled="false">Napravi kategoriju</button>
        </form>
    </div>
</div>
