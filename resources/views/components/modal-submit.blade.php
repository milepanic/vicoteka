{{-- Submit modal --}}
<div class="fixed pin overflow-auto bg-smoke-darker flex">
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-md m-auto flex-col flex border-t-4 border-blue">
        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <div class="px-1 md:px-4 py-4" id="modal-submit-content">
            <h3 class="text-grey-darkest uppercase mb-6 tracking-wide">Nova objava</h3>
            <div class="flex mb-8 border rounded">
                <a class="submit-tab submit-tab-active w-full py-2 text-sm font-semibold text-center uppercase tracking-wide" href="" data-type="text"><i class="far fa-file-alt"></i> Tekst</a>
                <a class="submit-tab submit-tab-inactive w-full py-2 text-sm font-semibold text-center font-medium uppercase tracking-wide" href="" data-type="image"><i class="fas fa-image"></i> Slika</a>
                <a class="submit-tab submit-tab-inactive w-full py-2 text-sm font-semibold text-center font-medium uppercase tracking-wide" href="" data-type="video"><i class="fas fa-video"></i> Video</a>
            </div>

            <input id="submit-hidden" type="hidden" name="action" value="text">

            <form class="submit-tab-div" id="form-submit-text">
                <div class="mb-4">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Tekst</label>
                    <textarea id="submit-body" class="w-full bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" name="body" rows="12" autofocus></textarea>
                </div>
                <div class="mb-4">
                    <div class="flex flex-col mb-4">
                        <div class="pretty p-curve p-default p-pulse mb-3">
                            <input id="submit-text-nsfw" type="checkbox"/>
                            <div class="state p-primary-o">
                                <label>18+</label>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="submit-btn w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit" data-name="{{ $category->name }}" data-disabled="false">Pošalji</button>
            </form>

            <form class="submit-tab-div" id="form-submit-image" hidden>
                <div class="mb-4">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Slika</label>
                    <input id="submit-image" type="file" name="image">
                </div>
                <div class="mb-4">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Tekst</label>
                    <textarea id="submit-body-image" class="w-full bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" name="body" rows="8"></textarea>
                </div>
                <div class="mb-4">
                    <div class="flex flex-col mb-4">
                        <div class="pretty p-curve p-default p-pulse mb-3">
                            <input id="submit-image-nsfw" type="checkbox"/>
                            <div class="state p-primary-o">
                                <label>18+</label>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="submit-btn w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit" data-name="{{ $category->name }}" data-disabled="false">Pošalji</button>
            </form>

            <form class="submit-tab-div" id="form-submit-video" hidden>
                <div class="mb-4">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Video URL</label>
                    <input id="submit-video-url" class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" type="text" name="video" placeholder="Primer: https://www.youtube.com/watch?v=2b4lLUe9L58">
                </div>
                <div class="mb-4">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Tekst</label>
                    <textarea id="submit-body-video" class="w-full bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" name="body" rows="7"></textarea>
                </div>
                <div class="mb-4">
                    <div class="flex flex-col mb-4">
                        <div class="pretty p-curve p-default p-pulse mb-3">
                            <input id="submit-video-nsfw" type="checkbox"/>
                            <div class="state p-primary-o">
                                <label>18+</label>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="submit-btn w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit" data-name="{{ $category->name }}" data-disabled="false">Pošalji</button>
            </form>
            
        </div>
    </div>
</div>
