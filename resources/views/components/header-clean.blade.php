<header class="bg-white mb-4 border-b border-grey-light">
    <div class="container mx-auto">
        <div class="flex items-center py-4">
            <div class="w-1/3 flex">
                <a href="{{ url('/') }}">
                    <h1 class="text-blue font-bold font-cursive">Distopija</h1>
                </a>
            </div>
        </div>
    </div>
</header>