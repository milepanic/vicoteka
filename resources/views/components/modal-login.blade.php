{{-- Submit modal --}}
<div class="fixed pin overflow-auto bg-smoke-darker flex" hidden>
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-sm m-auto flex-col flex border-t-4 border-blue">

        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <div class="px-1 md:px-4 py-4">
            <h3 class="text-grey-darkest uppercase mb-6 tracking-wide">Prijavite se</h3>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Email</label>
                <input id="login-email" class="w-full text-sm bg-grey-lightest px-4 py-3 border border-grey-light" type="email" name="email" placeholder="Unesite vašu email adresu" required>
            </div>
            <div class="relative mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Lozinka</label>
                <a class="absolute pin-r pin-t text-xs text-blue " href="{{ route('password.request') }}">
                    Zaboravili ste lozinku?
                </a>
                <input id="login-password" class="w-full text-sm bg-grey-lightest px-4 py-3 border border-grey-light" type="password" name="pasword" placeholder="Unesite lozinku" required>
                <div id="login-has-errors">
                    
                </div>
            </div>
            <div class="mb-8">
                <div class="pretty p-curve p-default p-pulse">
                    <input id="login-remember" type="checkbox"/>
                    <div class="state p-primary-o">
                        <label class="text-grey-darkest">Zapamti me</label>
                    </div>
                </div>
            </div>
            <button class="login-btn w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-white font-semibold text-sm tracking-wide" data-disabled="false">Prijavite se</button>
            <div class="border-t mt-8 pt-8">
                <p class="text-grey-darkest text-sm mb-4 text-center">Prijavite se preko socijalnih mreža</p>
                <div class="flex justify-between">
                    <a href="{{ url('/auth/facebook') }}" class="border rounded hover:border-blue-dark px-2 sm:px-4 py-2 w-1/3 mr-2">
                        <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-blue-dark fab fa-facebook-f"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Facebook</span>
                    </a>
                    <a href="{{ url('/auth/google') }}" class="border rounded hover:border-red px-2 sm:px-4 py-2 w-1/3 mr-2">
                        <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-red fab fa-google"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Google</span>
                    </a>
                    <a href="{{ url('/auth/twitter') }}" class="border rounded hover:border-blue px-2 sm:px-4 py-2 w-1/3">
                        <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-blue fab fa-twitter"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Twitter</span>
                    </a>
                </div>
                <p class="text-grey-darker text-xs mt-4">*Vaši privatni podaci su sigurni: www.distopija.com ih neće zloupotrebljavati niti slati trećim partijama</p>
            </div>
        </div>
    </div>
</div>
