{{-- Post modal --}}
    <div class="fixed pin z-50 overflow-auto bg-smoke-darker flex py-4">
        <div class="relative bg-white rounded shadow-lg w-full max-w-md m-auto flex-col flex">
    
            {{-- Close button --}}
            <span class="absolute pin-t pin-b pin-r p-4 px-4">
                <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                    <i class="fas fa-times"></i>
                </a>
                {{-- <span class="previous-url" hidden></span> --}}
            </span>

            <div class="post-box">
                {{-- Content --}}
                <div class="py-4 px-4 md:px-8">
                    <div class="modal-post-user-info"></div>
                    <div id="modal-post-content"></div>
                    <div id="modal-post-buttons"></div>
                </div>
                {{-- Comments --}}
                <div class="comment-box bg-grey-lightest px-8 border-t" hidden>
                    @auth
                    <div class="flex pt-4 pb-2">
                        <img class="rounded-full w-10 h-10 mr-3" src="{{ asset(auth()->user()->image_sm) }}">
                        {{-- write-comment jquery selector --}}
                        <input class="write-comment block w-full rounded-full border border-grey-light px-4" type="text" name="comment" placeholder="Komentariši...">
                    </div>
                    @endauth
                    <div class="comments-single py-4"></div>
                </div>
            </div>
        </div>
    </div>
