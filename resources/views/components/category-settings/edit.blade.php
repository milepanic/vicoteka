<form class="p-4" id="form-edit-category">
    <div class="mb-4 w-full text-center">
        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Slika kategorije</label>
        <img class="shadow rounded" src="{{ asset($category->image) }}">
    </div>
    <div class="mb-4">
        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Izmeni sliku</label>
        <input class="w-full" type="file" name="image" id="upload-edit-cat-img" accept="image/*">
        <img class="mt-4" id="display-edit-cat-img" src="">
    </div>
    <div class="mb-4">
        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Opis</label>
        <textarea id="edit-cat-description" class="w-full bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" name="description" rows="7" placeholder="Kratak opis kategorije i njena pravila">{{ $category->description }}</textarea>
    </div>
    <div class="mb-8">
        <div class="flex flex-col mb-4">
            <div class="pretty p-curve p-default p-pulse mb-3">
                <input id="edit-cat-nsfw" type="checkbox" @if($category->nsfw) checked @endif />
                <div class="state p-primary-o">
                    <label>18+</label>
                </div>
            </div>
            <div class="pretty p-curve p-default p-pulse mb-3">
                <input id="edit-cat-pictures" type="checkbox" @if($category->pictures) checked @endif/>
                <div class="state p-primary-o">
                    <label>Slike</label>
                </div>
            </div>
            <div class="pretty p-curve p-default p-pulse mb-3">
                <input id="edit-cat-videos" type="checkbox" @if($category->videos) checked @endif/>
                <div class="state p-primary-o">
                    <label>Video klipovi</label>
                </div>
            </div>
            <div class="pretty p-curve p-default p-pulse mb-3">
                <input id="edit-cat-mods" type="checkbox" @if($category->mods_only) checked @endif/>
                <div class="state p-primary-o">
                    <label>Samo moderatori</label>
                </div>
            </div>
        </div>
    </div>
    <button id="btn-edit-category" class="w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit" data-id="{{ $category->id }}" data-disabled="false">Izmeni osobine</button>
</form>

@section('external-js')
<script>
    
$(document).ready(function () {

    // Cropper
    var display_img = $("#display-edit-cat-img");
    var upload_img = $("#upload-edit-cat-img");

    imageUploaded = false;

    // Display image before upload
    $("input:file").change(function () {
        if ($(this).val() !== '') 
        {
            imageUploaded = true;

            display_img.cropper('destroy');

            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (e) {

                display_img.width("100%");
                display_img.attr('src', this.result);

                // Cropper options
                var options = {
                    aspectRatio: 1 / 1,
                    minContainerWidth: 350,
                    minContainerHeight: 350,
                    minCropBoxWidth: 100,
                    minCropBoxHeight: 100,
                    minCanvasWidth: 300,
                    minCanvasHeight: 300,
                    cropBoxResizable: true,
                    viewMode: 2,
                    responsive: true,
                    strict: false,
                    preview: '.ac-preview'
                };

                display_img.cropper(options);
            }
        }
    });
    
    $('#btn-edit-category').on('click', function (e) {
        e.preventDefault();

        id = $(this).data('id');
        parent = $(this).parent();
        description = parent.find('#edit-cat-description').val();

        nsfw = 0;
        cover = 0;
        pictures = 0;
        videos = 0;
        mods = 0;

        if (parent.find('#edit-cat-nsfw').is(":checked")) nsfw = 1;
        if (parent.find('#edit-cat-cover').is(":checked")) cover = 1;
        if (parent.find('#edit-cat-pictures').is(":checked")) pictures = 1;
        if (parent.find('#edit-cat-videos').is(":checked")) videos = 1;
        if (parent.find('#edit-cat-mods').is(":checked")) mods = 1;

        if (imageUploaded) {
            display_img.cropper('getCroppedCanvas').toBlob(function (blob) {
                var formData = new FormData();

                formData.append('description', description);
                formData.append('croppedImage', blob);
                formData.append('nsfw', nsfw);
                formData.append('cover', cover);
                formData.append('pictures', pictures);
                formData.append('videos', videos);
                formData.append('mods', mods);

                $.ajax({
                    type: 'POST',
                    url: '/kategorija/' + id,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function () {
                        window.location.reload();
                    },
                    error: function () {
                        alert('Dogodila se greška')
                    }
                });
            });

        } else {
            var data = {
                description: description,
                nsfw: nsfw,
                cover: cover,
                pictures: pictures,
                videos: videos,
                mods: mods
            };

            $.ajax({
                type: 'POST',
                url: '/kategorija/' + id,
                data: data,
                success: function () {
                    window.location.reload();
                },
                error: function () {
                    alert('Dogodila se greška')
                }
            });
        }
        
        
    });

});

</script>
@endsection
