<div class="row">
    <div class="col-lg-12">
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTable">
                    <thead>
                            <tr>
                                <th>Tekst</th>
                                <th>Prijavljen</th>
                                <th>ID Modela</th>
                                <th>Korisnik</th>
                                <th>Poslato</th>
                                <th>Akcija</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $report)
                            <tr>
                                <td>{{ $report->body }}</td>
                                <td>{{ $report->reportable_type }}</td>
                                <td>{{ $report->reportable_id }}</td>
                                <td>{{ $report->user_id }}</td>
                                <td>{{ $report->created_at }}</td>
                                <td class="flex justify-end">
                                    @if($report->reportable_type === 'user')
                                        <a class="ml-2 text-red-dark" href="#">
                                            <i class="fas fa-ban"></i>
                                        </a>
                                    @elseif($report->reportable_type === 'post')
                                        <a class="ml-2 text-orange" href="#">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a class="ml-2 text-red-dark" href="#">
                                            <i class="fas fa-minus-square"></i>
                                        </a>
                                    @else
                                        <a class="ml-2 text-red-dark" href="#">
                                            <i class="fas fa-minus-square"></i>
                                        </a>
                                    @endif
                                    <a class="ml-2 text-green-dark" href="#">
                                        <i class="fas fa-check-circle"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>