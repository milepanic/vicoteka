<div class="row">
    <div class="col-lg-12">
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th>Tekst</th>
                            <th>Korisnik</th>
                            <th>Upvote</th>
                            <th>Downvote</th>
                            <th>Omiljeno</th>
                            <th>Napravljeno</th>
                            <th>Obrisano</th>
                            <th>Akcija</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($data as $post)

                        <tr class="post-row" data-id="{{ $post->id }}">
                            <td class="col-md-3">
                                <a class="post-body @if($post->trashed()) text-red @else text-blue @endif" href="{{ url('objava/'. $post->id) }}" target="_blank">
                                    {{ Illuminate\Support\Str::limit($post->body, 100) }}
                                </a>
                            </td>
                            <td>
                                <a class="text-blue" href="{{ url('profil/' . $post->user->slug) }}" target="_blank">
                                    {{ $post->user->username }}
                                </a>
                            </td>
                            <td>{{ $post->upvotes_count }}</td>
                            <td>{{ $post->downvotes_count }}</td>
                            <td>{{ $post->favorites_count }}</td>
                            <td>
                                {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                            </td>
                            <td>
                                @if($post->trashed())
                                    Da
                                @else
                                    Ne
                                @endif
                            </td>
                            @if($post->trashed())
                                <td>
                                    <a class="restore-post-btn text-blue" href="#">
                                        <i class="fas fa-undo"></i>
                                    </a>
                                </td>
                            @else
                                <td>
                                    {{-- <a class="edit-post-btn text-orange" href="#">
                                        <i class="fas fa-edit"></i>
                                    </a> --}}
                                    <a class="delete-post-btn text-red-dark" href="#">
                                        <i class="fas fa-minus-square"></i>
                                    </a>
                                </td>
                            @endif
                            
                        </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>