<footer class="lg:rounded lg:shadow lg:bg-white">
    <div class="p-2">
        <div class="px-2">
            {{-- <p>Pratite Vicoteku na socijalnim mrezama</p> --}}
            <div class="my-4 flex justify-around">
                <a href="https://www.facebook.com/distopijaonline" target="_blank">
                    <img src="{{ asset('images/social-icons/facebook.png') }}" alt="Facebook official page">
                </a>
                <a href="#">
                    <img src="{{ asset('images/social-icons/twitter.png') }}" alt="Twitter official page">
                </a>
                <a href="https://www.instagram.com/distopija_com">
                    <img src="{{ asset('images/social-icons/instagram.png') }}" alt="Instagram official page">
                </a>
            </div>
        </div>
        
        <ul class="list-reset px-2 flex justify-center">
            <li class="text-xs">
                <a class="open-contact-modal text-grey-darker hover:text-grey-darkest hover:underline" href="#">Kontakt</a> &#9900;&nbsp;
            </li>
            <li class="text-xs">
                <a class="text-grey-darker hover:text-grey-darkest hover:underline" href="#">Legalne informacije</a> &#9900;&nbsp;
            </li>
            <li class="text-xs">
                <a class="text-grey-darker hover:text-grey-darkest hover:underline" href="#">O nama</a>
            </li>
        </ul>
        <ul class="list-reset px-2 flex justify-center">
            <li class="text-xs">
                <a class="open-autors-modal text-grey-darker hover:text-grey-darkest hover:underline" href="#">Autori slika</a> &#9900;&nbsp;
            </li>
            <li class="text-xs">
                <a class="text-grey-darker hover:text-grey-darkest hover:underline" href="{{ url('polisa-privatnosti') }}" target="_blank">Polisa privatnosti</a>
                {{-- <a class="text-grey-darker hover:text-grey-darkest hover:underline" href="#">Newsletter</a> &#9900;&nbsp; --}}
            </li>
            {{-- <li class="text-xs">
                <a class="text-grey-darker hover:text-grey-darkest hover:underline" href="#">Doniraj</a>
            </li> --}}
        </ul>
    </div>
    <div class="border-t border-grey-light">
        <p class="text-center text-xs text-grey-darkest p-2">&copy; 2018 Distopija - Sva prava zadržana</p>
    </div>
</footer>
