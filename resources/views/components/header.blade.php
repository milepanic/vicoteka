<header class="fixed w-full z-30 bg-white mb-8 border-b border-grey-light">
    <div class="container mx-auto">
        <div class="flex justify-between xl:items-center py-2 xl:py-4">
            <div class="p-3 mr-4 xl:hidden">
                @if(Request::is('profil/*'))
                <button class="-mr-4 text-grey-darker p-2 -m-2" id="responsive-caret-left">
                    <i class="fas fa-bars"></i>
                </button>
                @endif
            </div>
            <div class="xl:w-1/5">
                <a href="{{ url('/') }}">
                    <h1 class="inline text-blue font-bold font-cursive">Distopija</h1>
                </a>
            </div>
            <div class="hidden xl:block xl:w-2/5">
                <div id="search-box" class="relative">
                    <input id="search" class="py-2 border border-1 border-grey-light pl-4 pr-8 rounded w-full text-black text-sm" 
                        type="text" placeholder="Pretraga...">
                    <div class="absolute pin-r pin-y flex items-center px-3 text-grey cursor-pointer text-sm">
                        <i class="fas fa-search"></i>
                    </div>
                    <div hidden id="search-results" class="absolute border shadow w-full bg-white z-20">
                        <div class="w-full bg-grey-lightest border-b">
                            <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Objave</span>
                        </div>
                        <div id="search-results-posts"></div>
                        <div class="w-full bg-grey-lightest border-b">
                            <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Kategorije</span>
                        </div>
                        <div id="search-results-categories"></div>
                        <div class="w-full bg-grey-lightest border-b">
                            <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Korisnici</span>
                        </div>
                        <div id="search-results-users"></div>
                    </div>
                </div>
            </div>
            <div class="hidden xl:block xl:w-2/5">
                @include('components/user-navigation')
            </div>
            <div class="p-3 xl:hidden">
                <button class="text-grey-darker p-2 -m-2" id="responsive-caret">
                    <i class="fas fa-bars"></i>
                </button>
            </div>
        </div>
    </div>
</header>
<div class="mb-16 xl:mb-20 w-full h-4"></div>