{{-- Submit modal --}}
<div class="fixed pin overflow-auto bg-smoke-darker flex py-4">
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-md m-auto flex-col flex border-t-4 border-blue">

        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <form class="p-6" data-id="{{ auth()->id() }}" id="form-edit-profile">
            <h3 class="text-grey-darkest uppercase mb-6 tracking-wide">Izmenite profil</h3>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Profilna slika</label>
                <input class="mb-4" type="file" name="image" id="edit-profile-upload-img" accept="image/*">
                <img id="edit-profile-display-img" src="">
            </div>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Korisničko ime</label>
                <input class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" id="edit-username" type="text" name="username" value="{{ auth()->user()->username }}" placeholder="Promenite korisnicko ime">
                {{-- <p class="text-red text-xs italic">Please fill out this field.</p> --}}
            </div>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Opis</label>
                <textarea class="w-full bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" id="edit-description" name="description" rows="5" placeholder="Dodajte kratak opis">{{ auth()->user()->description }}</textarea>
                {{-- <p class="text-red text-xs italic">Please fill out this field.</p> --}}
            </div>
            <div class="flex justify-between mb-4">
                <div class="w-1/2 mr-1">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">facebook</label>
                    <input class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" id="edit-facebook" type="text" name="facebook" value="{{ auth()->user()->facebook }}" placeholder="Facebook profil">
                    {{-- <p class="text-red text-xs italic">Please fill out this field.</p> --}}
                </div>
                <div class="w-1/2 ml-1">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">twitter</label>
                    <input class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" id="edit-twitter" type="text" name="twitter" value="{{ auth()->user()->twitter }}" placeholder="Twitter profil">
                    {{-- <p class="text-red text-xs italic">Please fill out this field.</p> --}}
                </div>
            </div>
            <div class="flex justify-between mb-6">
                <div class="w-1/2 mr-1">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">instagram</label>
                    <input class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" id="edit-instagram" type="text" name="instagram" value="{{ auth()->user()->instagram }}" placeholder="Instagram profil">
                    {{-- <p class="text-red text-xs italic">Please fill out this field.</p> --}}
                </div>
                <div class="w-1/2 ml-1">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">tumblr</label>
                    <input class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" id="edit-tumblr" type="text" name="tumblr" value="{{ auth()->user()->tumblr }}" placeholder="Tumblr profil">
                    {{-- <p class="text-red text-xs italic">Please fill out this field.</p> --}}
                </div>
            </div>
            <div class="mb-8">
                <div class="pretty p-curve p-default p-pulse">
                    <input id="user-adult" type="checkbox" @if(auth()->user()->nsfw) checked @endif />
                    <div class="state p-primary-o">
                        <label class="text-grey-darkest">Prikaži 18+ sadržaj</label>
                    </div>
                </div>
            </div>
            <button id="btn-edit-profile" class="w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit" data-disabled="false">Izmeni</button>
        </form>
    </div>
</div>

@section('external-js')
{{-- <script src="{{ asset('js/profile.js') }}"></script> --}}
<link  href="{{ asset('assets/cropper/cropper.css') }}" rel="stylesheet">
<script src="{{ asset('assets/cropper/cropper.js') }}"></script>

<script>
    
    // Cropper
    var display_img = $("#edit-profile-display-img");
    var upload_img = $("#edit-profile-upload-img");

    // Display image before upload
    $("#edit-profile-upload-img").change(function () {
        if ($(this).val() !== '') 
        {
            display_img.cropper('destroy');

            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (e) {

                display_img.width("100%");
                display_img.attr('src', this.result);

                // Cropper options
                var options = {
                    aspectRatio: 1 / 1,
                    minContainerWidth: 350,
                    minContainerHeight: 350,
                    minCropBoxWidth: 100,
                    minCropBoxHeight: 100,
                    minCanvasWidth: 300,
                    minCanvasHeight: 300,
                    cropBoxResizable: true,
                    viewMode: 2,
                    responsive: true,
                    strict: false,
                    preview: '.ac-preview'
                };

                display_img.cropper(options);
            }
        }
    });

    // edit profile
    $('#btn-edit-profile').on('click', function (e) {
        e.preventDefault();

        if(!$('#form-edit-profile').valid()) {
            $(this).removeClass('bg-teal hover:bg-teal-dark').addClass('bg-red hover:bg-red-dark');

            return;
        } else {
            if($(this).data('disabled') === true) return;

            $(this).addClass('opacity-50 cursor-not-allowed');
            $(this).data('disabled', true);
        }

        nsfw = 0;
        if ($('#user-adult').is(":checked")) nsfw = 1;

        parent          = $(this).parent();
        id              = parent.data('id');
        username        = $('#edit-username').val();
        description     = $('#edit-description').val();
        facebook        = $('#edit-facebook').val();
        twitter         = $('#edit-twitter').val();
        instagram       = $('#edit-instagram').val();
        tumblr          = $('#edit-tumblr').val();

        if ($('#edit-profile-upload-img').val() !== '') {
            display_img.cropper('getCroppedCanvas').toBlob(function (blob) {
                var formData = new FormData();

                formData.append('username', username);
                formData.append('description', description);
                formData.append('croppedImage', blob);
                formData.append('facebook', facebook);
                formData.append('twitter', twitter);
                formData.append('instagram', instagram);
                formData.append('tumblr', tumblr);
                formData.append('nsfw', nsfw);

                $.ajax('/profile/edit', {
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        window.location.replace(data);
                    },
                    error: function () {
                        alert('Dogodila se greška...');
                    }
                });
            });
        } else {
            var data = {
                username: username,
                description: description,
                facebook: facebook,
                twitter: twitter,
                instagram: instagram,
                tumblr: tumblr,
                nsfw: nsfw,
            };

            var elem = $(this);

            $.ajax({
                type: 'POST',
                url: '/profile/edit',
                data: data,
                success: function (data) {
                    window.location.replace(data);
                },
                error: function () {
                    alert('Dogodila se greška');
                }
            });
        }
    });
</script>
@endsection
