<div id="posts" class="infinite-scroll">
	
	@forelse($posts as $post)
    {{-- post-box jquery selector --}}
	<div class="post-box break-words relative" data-id="{{ $post->id }}" data-category-id="{{ $post->category->id }}">
	    <div class="p-4 border-b">
	        <div class="post-user-info flex mb-2 md:mb-3">
	            <a class="post-user-image" href="{{ url('profil/' . $post->user->slug) }}">
	                <img class="lazyload rounded-full w-10 h-10" 
	                    data-src="{{ asset($post->user->image_sm) }}"
	                    alt="{{ $post->user->username }}">
	            </a>
	            <div class="ml-2 md:ml-4">
	                <a class="text-grey-black" href="{{ url('profil/' . $post->user->slug) }}">
	                    <p class="text-sm md:text-base font-bold -mb-1">{{ $post->user->username }}</p>
	                </a>
	                <span class="text-xs text-grey-dark">
	                    <a class="open-post-modal text-grey-dark hover:text-grey-darker hover:underline" href="{{ url('objava/' . $post->id) }}" data-id="{{ $post->id }}">
	                        {{ $post->diffCreatedAt }}
	                    </a> &#8226; 
	                    <a class="text-grey-dark hover:text-grey-darker hover:underline" href="{{ url('kategorija/' . $post->category->slug) }}">
	                        {{ $post->category->name }}</a>
	                    @if($post->nsfw)
							&#8226; <span class="text-red">18+</span>
	                    @endif
	                </span>
	            </div>
	        </div>
	        @if(auth()->check() && auth()->id() === $post->user->id && $post->created_at > now()->subMinutes(30) 
	        	|| auth()->check() && auth()->user()->admin == true && $post->created_at > now()->subHours(64))
		        <div class="absolute pin-r pin-t mt-3 mr-3">
		        	<button class="post-edit p-1 pointer text-grey hover:text-grey-dark">
			        	<i class="far fa-edit"></i>
		        	</button>
		        	<button class="post-delete p-1 pointer text-grey hover:text-grey-dark">
			        	<i class="fas fa-times"></i>
		        	</button>
		        </div>
	        @endif
	        {{-- post-content for appending to modal --}}
	        <div class="post-content">
	        @if(!$post->nsfw || auth()->user() && auth()->user()->nsfw)
		        <p class="font-sans text-grey-black text-xs md:text-sm mb-4">
		        	@if($post->trashed())
		        		[obrisano]
		        	@else
		        		{!! nl2br(e($post->body)) !!}
		        	@endif
		        </p>
		        @if($post->image && ! $post->trashed())
			        <div class="text-center">
			        	<img class="lazyload magnific-popup cursor-pointer mb-4 w-full h-auto" 
			        		data-src="{{ asset($post->image) }}">
			        </div>
		        @elseif($post->video && ! $post->trashed())
		        	<div class="video-player mb-4 relative overflow-hidden cursor-pointer" data-id="{{ substr($post->video, 30) }}" style="padding-bottom: 56.23%;">
			        	<img class="lazyload video-thumbnail absolute pin-t pin-l max-w-full h-full" data-src="https://img.youtube.com/vi/{{ substr($post->video, 30) }}/mqdefault.jpg">
		        		<img class="video-play-btn absolute block w-12 md:w-16 h-12 md:h-16 pin m-auto" src="{{ asset('images/play-button.png') }}">
						{{-- <iframe class="absolute pin-t pin-l w-full h-full" width="560" height="316" src="{{ $post->video }}" frameborder="0" allowfullscreen></iframe>  --}}
		        	</div>
		        @endif
	        @else
		        <div class="flex items-center pb-8 px-12">
					<img src="{{ asset('images/nsfw.png') }}" alt="Sadrzaj za odrasle">
					@guest
						<p class="ml-8 text-md text-grey-darkest">
							Sadržaj za odrasle mogu pogledati samo prijavljeni korisnici. <br>
							<a class="open-login-modal text-blue hover:underline" href="#">Prijavite se</a> ili se <a class="open-register-modal text-blue hover:underline" href="#">registrujte</a>.
						</p>
					@endguest

					@auth
						<p class="ml-8 text-md text-grey-darkest">
							Da biste omogućili gledanje 18+ sadržaja izmenite postavke profila.
						</p>
					@endauth
				</div>
	        @endif
	        </div>
	        <div class="post-buttons flex justify-between">
	            <div class="flex items-center mr-4">
	                {{-- vote-btn jquery selector --}}
	                <a class="vote-btn mr-4 text-sm md:text-base
	                    @if(auth()->user() && $post->votes->where('id', auth()->user()->id)->first()
	                            && $post->votes->where('id', auth()->user()->id)->first()->pivot->vote === 1)
	                        text-blue
	                    @else
	                        text-grey
	                    @endif" data-type="upvote" href="">
	                    <i class="fas fa-arrow-up"></i>
	                    <span>{{ $post->upvotes_count }}</span>
	                </a>
	                {{-- vote-btn jquery selector --}}
	                <a class="vote-btn mr-4 text-sm md:text-base
	                    @if(auth()->user() && $post->votes->where('id', auth()->user()->id)->first()
	                            && $post->votes->where('id', auth()->user()->id)->first()->pivot->vote === -1)
	                        text-purple
	                    @else
	                        text-grey
	                    @endif" data-type="downvote" href="">
	                    <i class="fas fa-arrow-down"></i>
	                    <span>{{ $post->downvotes_count }}</span>
	                </a>
	                {{-- favorite-btn jquery selector --}}
	                <a class="favorite-btn mr-4 text-sm md:text-base 
	                    @if(auth()->user() && $post->favorites->contains(auth()->user())) 
	                        text-orange
	                    @else 
	                        text-grey
	                    @endif" href="">
	                    <i class="far fa-star"></i>
	                    <span>{{ $post->favorites_count }}</span>
	                </a>
                	<div class="-ml-1 mt-1 addthis_inline_share_toolbox" 
		                	data-url="{{ url('objava/' . $post->id) }}" 
		                	@if($post->body)
			                	data-title="{{ Illuminate\Support\Str::limit($post->body, 100) }}" 
			                @else
			                	data-title="Nova objava korisnika {{ $post->user->username }}" 
			                @endif
		                	data-description="Objavio {{ $post->user->username }}, u kategoriji {{ $post->category->name }} - {{ $post->created_at->format('d/m/Y H:i:s') }}" 
		                	data-media="{{ asset('images/share-posts-fb/' . $post->id . '.jpg') }}"
			                >
	                </div>
	            </div>
	            <div class="flex items-center">

	                <a class="btn-comments mr-4 text-grey text-sm md:text-base" href="">
	                    <i class="fas fa-comments"></i>
	                    <span>{{ $post->comments_count }}</span>
	                </a>

	                {{-- dropdown menu btn --}}
	                <a class="display-dropdown-post text-grey text-sm md:text-base" href="#">
	                    <i class="fas fa-ellipsis-v"></i>
	                </a>

					{{-- dropdown menu box --}}
					<ul class="dropdown-post list-reset border border-grey-light bg-white py-1 rounded absolute pin-b pin-r w-48 mb-12 hidden">
						<li class="hover:bg-grey-lightest">
							<a class="open-report-modal block py-2 px-4 text-sm text-grey-darkest" data-id="{{ $post->id }}" data-type="post" data-category="{{ $post->category->id }}" href="#"><i class="text-grey-dark fas fa-flag"></i> Prijavi objavu</a>
						</li>
						<li class="hover:bg-grey-lightest">
							<a class="open-report-modal block py-2 px-4 text-sm text-grey-darkest" data-id="{{ $post->user->id }}" data-type="user" data-category="{{ $post->category->id }}" href="#"><i class="text-grey-dark fas fa-user-times"></i> Prijavi korisnika</a>
						</li>
						<li class="hover:bg-grey-lightest">
							<a class="block-category-btn block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-times"></i> Blokiraj kategoriju</a>
						</li>
					</ul>
	            </div>
	        </div>
	    </div>

	    {{-- comment-box jquery selector --}}
	    <div class="comment-box bg-grey-lightest border-b px-6" hidden>
	        @auth
	        <div class="flex pt-4 pb-2">
	            <img class="rounded-full w-10 h-10 mr-3" 
	                src="{{ asset(auth()->user()->image_sm) }}" alt="{{ auth()->user()->username }}">
	            {{-- write-comment jquery selector --}}
	            <textarea class="write-comment text-xs md:text-sm w-full rounded-lg border border-grey-light px-4 py-3" name="comment" placeholder="Komentariši..."></textarea>
	        </div>
	        @endauth

	        <div class="comments-single py-4"></div>
	    </div>
	</div>
	@empty
		@if(Request::is('korisnici'))
		<div class="flex items-center p-8">
			<img src="{{ asset('images/warning.png') }}" alt="Upozorenje">
			<p class="ml-8 text-lg text-grey-darkest">
				Trenutno ne pratite ni jednog korisnika ili još nema objava kod korisnika koje pratite. <br>Pronađite još korisnika <a class="text-blue hover:underline" href="{{ url('otkrijte-korisnike') }}">ovde</a>.
			</p>
		</div>
		@endif
		@if(Request::is('kategorije'))
		<div class="flex items-center p-8">
			<img src="{{ asset('images/warning.png') }}" alt="Upozorenje">
			<p class="ml-8 text-lg text-grey-darkest">
				Trenutno ne pratite ni jednu kategoriju ili još nema objava u kategorijama koje pratite. <br>Pronađite još kategorija <a class="text-blue hover:underline" href="{{ url('otkrijte-kategorije') }}">ovde</a>.
			</p>
		</div>
		@endif
		@if(Request::is('profil/*'))
		<div class="flex items-center p-8">
			<img src="{{ asset('images/warning.png') }}" alt="Upozorenje">
			<p class="ml-8 text-lg text-grey-darkest">
				Trenutno nema objava.
			</p>
		</div>
		@endif
		@if(Request::is('kategorija/*'))
		<div class="flex items-center p-8">
			<img src="{{ asset('images/warning.png') }}" alt="Upozorenje">
			<p class="ml-8 text-lg text-grey-darkest">
				<strong>{{ $category->name }}</strong> trenutno nema objava. <br>Pronadjite jos kategorija <a class="text-blue hover:underline" href="{{ url('otkrijte-kategorije') }}">ovde</a>.
			</p>
		</div>
		@endif
	@endforelse

	<div hidden>
		@if(request()->is('profil/*') && request()->input('prikaz') === 'postovi')
			{{ $posts->appends(['prikaz' => 'postovi'])->links() }}

		@elseif(request()->is('profil/*') && request()->input('prikaz') === 'omiljeni-postovi')
			{{ $posts->appends(['prikaz' => 'omiljeni-postovi'])->links() }}

		@elseif(request()->is('profil/*') && request()->input('prikaz') === 'upvoteovani-postovi')
			{{ $posts->appends(['prikaz' => 'upvoteovani-postovi'])->links() }}

		@elseif(request()->is('profil/*') && request()->input('prikaz') === 'downvoteovani-postovi')
			{{ $posts->appends(['prikaz' => 'downvoteovani-postovi'])->links() }}

		@else
			{{ $posts->links() }}

		@endif
	</div>

</div>

