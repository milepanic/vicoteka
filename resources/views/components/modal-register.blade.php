{{-- Submit modal --}}
<div class="fixed pin z-50 overflow-auto bg-smoke-darker flex ">
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-md m-auto flex-col flex border-t-4 border-teal">

        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <form class="px-1 md:px-4 py-4" id="form-register">
            <h3 class="text-grey-darkest uppercase mb-6 tracking-wide">Registrujte se</h3>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Korisničko ime</label>
                <input id="register-username" class="w-full text-sm bg-grey-lightest px-4 py-3 border border-grey-light" type="text" name="username" placeholder="Unesite novo korisničko ime" required>
            </div>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Email</label>
                <input id="register-email" class="w-full text-sm bg-grey-lightest px-4 py-3 border border-grey-light" type="email" name="email" placeholder="Unesite vašu email adresu" required>
            </div>
            <div class="flex justify-between mb-6">
                <div class="w-1/2 mr-1">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Lozinka</label>
                    <input id="register-password" class="w-full text-sm bg-grey-lightest px-4 py-3 border border-grey-light" type="password" name="password" placeholder="Unesite lozinku" required>
                </div>
                <div class="w-1/2 ml-1">
                    <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Potvrdite lozinku</label>
                    <input id="register-password_confirmation" class="w-full text-sm bg-grey-lightest px-4 py-3 border border-grey-light" type="password" name="password_confirmation" placeholder="Ponovite lozinku" required>
                </div>
            </div>
            
            <div class="mb-6">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Opis (opcionalno)</label>
                <textarea id="register-description" class="w-full text-sm bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" name="description" rows="5" placeholder="Unesite kratak opis"></textarea>
            </div>
            <button class="register-btn w-full bg-teal hover:bg-teal-dark rounded py-3 uppercase text-white font-semibold text-sm tracking-wide" data-disabled="false">Registrujte se</button>
            <div class="border-t mt-8 pt-8">
                <p class="text-grey-darkest text-sm mb-4 text-center">Prijavite se preko socijalnih mreža</p>
                <div class="flex justify-between">
                    <a href="{{ url('/auth/facebook') }}" class="border rounded hover:border-blue-dark px-2 sm:px-4 py-2 w-1/3 mr-2">
                        <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-blue-dark fab fa-facebook-f"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Facebook</span>
                    </a>
                    <a href="{{ url('/auth/google') }}" class="border rounded hover:border-red px-2 sm:px-4 py-2 w-1/3 mr-2">
                        <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-red fab fa-google"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Google</span>
                    </a>
                    <a href="{{ url('/auth/twitter') }}" class="border rounded hover:border-blue px-2 sm:px-4 py-2 w-1/3">
                        <i class="block sm:inline mx-auto sm:mx-0 my-1 sm:my-0 text-blue fab fa-twitter"></i> <span class="block sm:inline text-center sm:text-left text-xs sm:text-sm sm:ml-2">Twitter</span>
                    </a>
                </div>
                <p class="text-grey-darker text-xs mt-4">*Vaši privatni podaci su sigurni: www.distopija.com ih neće zloupotrebljavati niti slati trećim partijama</p>
            </div>
        </form>
    </div>
</div>
