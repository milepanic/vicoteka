<div class="fixed pin z-50 overflow-auto bg-smoke-darker flex" hidden>
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-sm m-auto flex-col flex">

        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <div class="p-4 font-sans">
            <h3>Autori slika koje se nalaze na sajtu</h3>
            <div class="mt-4">
                <div class="text-grey-darkest mb-2">Icons made by <a class="font-bold text-blue" href="https://www.flaticon.com/authors/eucalyp" title="Eucalyp">Eucalyp</a> from <a class="text-blue" href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a class="text-blue" href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
                <div class="text-grey-darkest mb-2">Icons made by <a class="font-bold text-blue" href="https://www.flaticon.com/authors/business-dubai" title="Business Dubai">Business Dubai</a> from <a class="text-blue" href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a class="text-blue" href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
                <div class="text-grey-darkest">Icons made by <a class="font-bold text-blue" href="http://www.freepik.com" title="Freepik">Freepik</a> from <a class="text-blue" href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a class="text-blue" href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
            </div>
        </div>
    </div>
</div>