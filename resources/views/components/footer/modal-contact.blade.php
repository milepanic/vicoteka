<div class="fixed pin z-50 overflow-auto bg-smoke-darker flex" hidden>
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-sm m-auto flex-col flex border-t-4 border-blue">

        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <form class="px-1 md:px-4 py-4">
            <h3 class="text-grey-darkest text-center uppercase mb-6 tracking-wide">Kontaktirajte nas</h3>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Vaše ime</label>
                <input class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" type="text" name="name" required>
            </div>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Vaša email adresa</label>
                <input class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" type="email" name="email" required>
            </div>
            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Poruka</label>
                <textarea rows="7" class="w-full bg-grey-lightest px-4 py-3 border border-grey-light" name="message" required></textarea>
            </div>
            <button class="w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit">Pošaljite poruku</button>
        </form>
    </div>
</div>