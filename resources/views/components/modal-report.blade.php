{{-- Report post/user/category modal --}}
<div class="fixed pin overflow-auto bg-smoke-darker flex py-4">
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-md m-auto flex-col flex border-t-4 border-blue">

        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <form class="px-1 md:px-4 py-4 text-grey-darkest" id="form-report">
            <h3 class="uppercase mb-6 tracking-wide">Izaberite razlog</h3>
            <div class="mb-4">
                <div class="flex flex-col mb-4">
                    <div class="pretty p-round p-default p-pulse mb-3">
                        <input name="report-radio" type="radio" value="1"/>
                        <div class="state p-primary-o">
                            <label id="report-1"></label>
                        </div>
                    </div>
                    <div class="pretty p-round p-default p-pulse mb-3">
                        <input name="report-radio" type="radio" value="2"/>
                        <div class="state p-primary-o">
                            <label id="report-2"></label>
                        </div>
                    </div>
                    <div class="pretty p-round p-default p-pulse mb-3">
                        <input name="report-radio" type="radio" value="3"/>
                        <div class="state p-primary-o">
                            <label id="report-3"></label>
                        </div>
                    </div>
                    <div class="pretty p-round p-default p-pulse mb-3">
                        <input name="report-radio" type="radio" value="4" id="report-other"/>
                        <div class="state p-primary-o">
                            <label>Ostalo</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-8" id="report-text" hidden>
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Upišite razlog</label>
                <textarea id="report-body" class="w-full bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" name="body" rows="4"></textarea>
            </div>
            <button id="btn-report" class="close-modal w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit" data-disabled="false">Prijavi</button>
        </form>
    </div>
</div>
