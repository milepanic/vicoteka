<div class="mb-4 py-2 rounded shadow bg-white">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
        Objave</h4>
    <ul class="list-reset px-4">
        <li class="py-2 border-b border-grey-lighter flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=postovi') }}">Postovi</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->posts_count }}</span>
        </li>
        <li class="py-2 flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=komentari') }}">Komentari</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->comments_count }}</span>
        </li>
    </ul>
</div>

<div class="mb-4 py-2 rounded shadow bg-white">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
        Korisnici</h4>
    <ul class="list-reset px-4">
        <li class="py-2 border-b border-grey-lighter flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=pratioci') }}">Pratioci</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->followers }}</span>
        </li>
        <li class="py-2 flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=prati') }}">Prati</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->following }}</span>
        </li>
    </ul>
</div>
<div class="mb-4 py-2 rounded shadow bg-white">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
        Kategorije</h4>
    <ul class="list-reset px-4">
        <li class="py-2 border-b border-grey-lighter flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=pretplacene-kategorije') }}">Pretplacene kategorije</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->subscribed }}</span>
        </li>
        <li class="py-2 flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=moderator-kategorija') }}">Vlasnik kategorija</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->moderator }}</span>
        </li>
    </ul>
</div>
<div class="mb-4 py-2 rounded shadow bg-white">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
        Aktivnost</h4>
    <ul class="list-reset px-4">
        <li class="py-2 border-b border-grey-lighter flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=omiljeni-postovi') }}">Omiljeni postovi</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->favorites }}</span>
        </li>
        <li class="py-2 border-b border-grey-lighter flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=upvoteovani-postovi') }}">Upvotovani postovi</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->upvoted }}</span>
        </li>
        <li class="py-2 flex justify-between">
            <a class="text-sm text-blue-dark hover:underline pr-2" 
                href="{{ url('profil/' . $user->slug . '?prikaz=downvoteovani-postovi') }}">Downvoteovani postovi</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->downvoted }}</span>
        </li>
    </ul>
</div>
<div class="mb-4">
    @include('components/footer')
</div>