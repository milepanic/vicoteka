<div id="sidebar-responsive" class="h-screen overflow-hidden overflow-y-scroll fixed shadow z-40 w-80 bg-white border-l border-b px-4 pin-r" hidden>
    <div class="pl-4 py-4 my-1 flex justify-end">
        <button class="text-grey-darker p-2 -m-2" id="responsive-caret-close">
            <i class="fas fa-bars"></i>
        </button>
    </div>
    @guest
    <ul class="list-reset py-1 border-b mb-6 pb-6">
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-login-modal block py-2 px-4 text-sm text-grey-darkest" href="{{ url('login') }}"><i class="text-grey-dark fas fa-sign-in-alt"></i> Prijava</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-register-modal block py-2 px-4 text-sm text-grey-darkest" href="{{ url('register') }}"><i class="text-grey-dark fas fa-user-circle"></i> Registracija</a>
        </li>
    </ul>
    @endguest
    @auth
    <ul class="list-reset py-1 border-b mb-6 pb-6">
        <button class="bg-blue text-grey-lighter hover:bg-blue-dark text-sm font-bold tracking-wide w-full mb-4 py-2 px-4 rounded shadow" id="header-open-submit-modal"><i class="fas fa-sm fa-plus"></i> Postavi</button>
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('profil/' . auth()->user()->slug) }}"><i class="text-grey-dark fas fa-user"></i> Profil</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-create-modal block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-plus-circle"></i> Napravi kategoriju</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('moje-kategorije') }}"><i class="text-grey-dark fas fa-th-large"></i> Moje kategorije</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-ban"></i> Blokirane kategorije</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-report-modal block py-2 px-4 text-sm text-grey-darkest border-b border-grey-lighter" href="#"><i class="text-grey-dark fas fa-bug"></i> Prijavi problem</a>
        </li>
        @if(auth()->user()->admin)
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest border-b border-grey-lighter" href="{{ url('admin') }}"><i class="text-grey-dark fas fa-lock-open"></i> Admin Panel</a>
        </li>
        @endif
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('/logout') }}"><i class="text-grey-dark fas fa-sign-out-alt"></i> Odjavite se</a>
        </li>
    </ul>
    @endauth
    
    <div class="border-b mb-6 pb-6">
        @if(auth()->user() && $category->users->where('id', auth()->id())->first() && 
            $category->users->where('id', auth()->id())->first()->pivot->moderator === 1) 
        <a class="mb-4 w-full block text-center bg-orange-dark text-grey-lighter text-sm font-bold uppercase tracking-wide py-3 rounded shadow" href="{{ url('kategorija/' . $category->slug . '/moderator?prikaz=sve-objave') }}">Moderator</a>
        @endif
        <img class="rounded shadow" src="{{ asset($category->image) }}" alt="naslovna slika kategorije {{ $category->name }}">
        <div class="px-4 py-2">
            <div class="flex justify-between">
                <div>
                    <a class="hover:underline" href="{{ url('kategorija/' . $category->slug) }}">
                        <h3 class="text-grey-black">{{ $category->name }}</h3>
                    </a>
                    <span class="text-sm text-grey-darker">Pratioci: 
                        <span id="subscribers-count">{{ $category->subscribers }}</span>
                    </span>
                </div>
                {{-- subscribe-btn jquery selector --}}
                @if(auth()->user() && $category->users->where('id', auth()->id())->first()
                    && $category->users->where('id', auth()->id())->first()->pivot->subscribed === 1)
                <button class="subscribe-btn subscribed text-sm my-2 py-2 px-4 font-bold rounded shadow" data-id="{{ $category->id }}" data-action="0">Pratiš</button>
                @else
                <button class="subscribe-btn subscribe text-sm my-2 py-2 px-4 font-bold rounded shadow" data-id="{{ $category->id }}" data-action="1">Zaprati</button>
                @endif
            </div>
        </div>
    </div>

    <div class="border-b mb-6 pb-6">
        <button class="w-full bg-blue hover:bg-blue-dark text-grey-lighter text-sm font-bold uppercase tracking-wide py-3 rounded shadow" id="responsive-open-submit-modal">Postavi</button>
    </div>

    <div class="border-b mb-6 pb-6">
        <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">Opis</h4>
        <p class="text-grey-darker">{{ $category->description }}</p>
    </div>
    
    <div class="border-b mb-6 pb-6">
        <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-6 px-4">Moderatori</h4>
        <ul class="list-reset px-4">

            @foreach($category->users as $user)

            <li class="mb-3 @if(!$loop->last) border-b border-grey-lighter pb-2 @endif">
                <div class="flex items-center relative">
                    <a href="{{ url('profil/' . $user->slug) }}">
                        <img class="w-12 h-12 rounded-full" src="{{ asset($user->image_sm) }}" alt="{{ $user->username }}">
                    </a>
                    <div class="ml-3 flex flex-col">
                        <a class="text-grey-darkest font-medium -mt-3 mb-1" 
                            href="{{ url('profil/' . $user->slug) }}">{{ $user->username }}</a>
                        <div>
                            <button class="follow-btn-sidebar py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Zaprati</button>
                        </div>
                    </div>
                </div>
            </li>

            @endforeach

        </ul>
    </div>

    <div class="pb-6">
        @include('components/footer')
    </div>

</div>
