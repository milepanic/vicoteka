<div id="post-single" class="post-box relative" data-id="{{ $post->id }}">
    <div class="py-4 px-4 md:px-8 border-b">
        <div class="post-user-info flex mb-2 md:mb-3">
            <a class="post-user-image" href="{{ url('profil/' . $post->user->slug) }}">
                <img class="rounded-full w-10 h-10" 
                    src="{{ asset($post->user->image_sm) }}"
                    alt="profile picture of {{ $post->user->username }}">
            </a>
            <div class="ml-4">
                <a class="text-grey-black" href="{{ url('profil/' . $post->user->slug) }}">
                    <h4 class="text-sm md:text-base font-bold -mb-1">{{ $post->user->username }}</h4>
                </a>
                <span class="text-xs text-grey-dark">
                    <a class="open-post-modal text-grey-dark hover:text-grey-darker hover:underline" href="#" data-id="{{ $post->id }}">
                        {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                    </a> &#8226; 
                    <a class="text-grey-dark hover:text-grey-darker hover:underline" href="{{ url('kategorija/' . $post->category->slug) }}">
                        {{ $post->category->name }}</a>
                    @if($post->nsfw)
                        &#8226; <span class="text-red">18+</span>
                    @endif
                </span>
            </div>
        </div>
        {{-- post-content for appending to modal --}}
        <div class="post-content">
        @if(!$post->nsfw || auth()->user() && auth()->user()->nsfw)
                <p class="font-sans text-grey-black text-xs md:text-sm mb-4">
                    @if($post->trashed())
                        [obrisano]
                    @else
                        {!! nl2br(e($post->body)) !!}
                    @endif
                </p>
                @if($post->image && ! $post->trashed())
                    <img class="lazyload magnific-popup mb-4 w-full h-auto" data-src="{{ asset($post->image) }}">
                @elseif($post->video && ! $post->trashed())
                     <div class="video-player mb-4 relative overflow-hidden cursor-pointer" data-id="{{ substr($post->video, 30) }}" style="padding-bottom: 56.23%;">
                        <img class="video-thumbnail absolute pin-t pin-l max-w-full h-full" src="https://img.youtube.com/vi/{{ substr($post->video, 30) }}/mqdefault.jpg">
                        <img class="video-play-btn absolute block w-12 md:w-16 h-12 md:h-16 pin m-auto" src="{{ asset('images/play-button.png') }}">
                        {{-- <iframe class="absolute pin-t pin-l w-full h-full" width="560" height="316" src="{{ $post->video }}" frameborder="0" allowfullscreen></iframe>  --}}
                    </div>
                @endif
            @else
                <div class="flex items-center pb-8 px-12">
                    <img src="{{ asset('images/nsfw.png') }}" alt="Sadrzaj za odrasle">
                    @guest
                    <p class="ml-8 text-md text-grey-darkest">
                        Sadržaj za odrasle mogu pogledati samo prijavljeni korisnici. <br>
                        <a class="open-login-modal text-blue hover:underline" href="#">Prijavite se</a> ili se <a class="open-register-modal text-blue hover:underline" href="#">registrujte</a>.
                    </p>
                    @endguest

                    @auth
                    <p class="ml-8 text-md text-grey-darkest">
                        Da biste omogućili gledanje 18+ sadržaja izmenite postavke profila.
                    </p>
                    @endauth
                </div>
            @endif
        </div>
        <div class="post-buttons flex justify-between">
            <div class="flex items-center mr-4">
                {{-- vote-btn jquery selector --}}
                <a class="vote-btn mr-4 text-sm md:text-base 
                    @if(auth()->user() && $post->votes->where('id', auth()->user()->id)->first()
                            && $post->votes->where('id', auth()->user()->id)->first()->pivot->vote === 1)
                        text-blue
                    @else
                        text-grey
                    @endif" data-type="upvote" href="">
                    <i class="fas fa-arrow-up"></i>
                    <span>{{ $post->upvotes_count }}</span>
                </a>
                {{-- vote-btn jquery selector --}}
                <a class="vote-btn mr-4 text-sm md:text-base 
                    @if(auth()->user() && $post->votes->where('id', auth()->user()->id)->first()
                            && $post->votes->where('id', auth()->user()->id)->first()->pivot->vote === -1)
                        text-purple
                    @else
                        text-grey
                    @endif" data-type="downvote" href="">
                    <i class="fas fa-arrow-down"></i>
                    <span>{{ $post->downvotes_count }}</span>
                </a>
                {{-- favorite-btn jquery selector --}}
                <a class="favorite-btn mr-4 text-sm md:text-base 
                    @if(auth()->user() && $post->favorites->contains(auth()->user())) 
                        text-orange
                    @else 
                        text-grey
                    @endif" href="">
                    <i class="far fa-star"></i>
                    <span>{{ $post->favorites_count }}</span>
                </a>
                <div class="-ml-1 mt-1 addthis_inline_share_toolbox" 
                    data-url="{{ url('objava/' . $post->id) }}" 
                    @if($post->body)
                        data-title="{{ Illuminate\Support\Str::limit($post->body, 100) }}" 
                    @else
                        data-title="Nova objava korisnika {{ $post->user->username }}" 
                    @endif
                    data-description="Objavio {{ $post->user->username }}, u kategoriji {{ $post->category->name }} - {{ $post->created_at->format('d/m/Y H:i:s') }}" 
                    data-media="{{ asset('images/share-posts-fb/' . $post->id . '.jpg') }}"
                    >
                </div>
            </div>
            <div class="flex items-center">

                <a class="btn-comments mr-4 text-grey text-sm md:text-base" href="">
                    <i class="fas fa-comments"></i>
                    <span>{{ $post->comments_count }}</span>
                </a>

                {{-- dropdown menu btn --}}
                <a class="display-dropdown-post text-grey text-sm md:text-base" href="#">
                    <i class="fas fa-ellipsis-v"></i>
                </a>

                {{-- dropdown menu box --}}
                <ul class="dropdown-post list-reset border border-grey-light bg-white py-1 rounded absolute pin-b pin-r w-48 mb-12 hidden">
                    <li class="hover:bg-grey-lightest">
                        <a class="block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-flag"></i> Prijavi vic</a>
                    </li>
                    <li class="hover:bg-grey-lightest">
                        <a class="block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-user-times"></i> Prijavi korisnika</a>
                    </li>
                    <li class="hover:bg-grey-lightest">
                        <a class="block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-times"></i> Blokiraj kategoriju</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    {{-- comment-box jquery selector --}}
    <div class="comment-box bg-grey-lightest border-b px-6" hidden>
        @if(auth()->user())
        <div class="flex pt-4 pb-2">
            <img class="rounded-full w-10 h-10 mr-3" 
                src="{{ asset(auth()->user()->image_sm) }}">
            {{-- write-comment jquery selector --}}
            <input class="write-comment block w-full rounded-full border border-grey-light px-4" type="text" name="comment" placeholder="Komentariši...">
        </div>
        @endif

        <div class="comments-single py-4"></div>
    </div>
</div>
