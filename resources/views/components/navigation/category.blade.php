<div class="py-4 border-b border-grey-lighter flex justify-between">
    <div class="relative">
        <span class="pl-4 md:pl-8 text-grey-dark uppercase text-xs font-bold">Sortiraj: &nbsp;
            <a class="display-sort-nav text-blue font-bold tracking-wide" href="#">
                @if(Request::is('kategorija/*/novo'))
                <span id="sort-current-icon"><i class="fas fa-clock"></i></span> <span id="sort-current-page">Novo</span>
                @elseif(Request::is('kategorija/*/top'))
                <span id="sort-current-icon"><i class="fas fa-sort-amount-down"></i></span> <span id="sort-current-page">Top</span>
                @elseif(Request::is('kategorija/*'))
                <span id="sort-current-icon"><i class="fas fa-fire"></i></span> <span id="sort-current-page">Popularno</span> 
                @endif
                <i class="fas fa-caret-down fa-xs"></i>
            </a>
        </span>
        <ul id="sort-nav" class="category-sort-nav list-reset border border-grey-lighter shadow-md bg-white py-1 rounded w-48 absolute mt-2 ml-6 z-20 hidden">
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is(['kategorija/*/popularno', 'kategorija/*']) && ! Request::is(['kategorija/*/novo', 'kategorija/*/top'])) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="#">
                    <i class="sort-icon fas fa-fire @if(Request::is(['kategorija/*/popularno', 'kategorija/*']) && ! Request::is(['kategorija/*/novo', 'kategorija/*/top'])) text-blue @endif"></i> &nbsp; Popularno
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is('kategorija/*/novo')) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="#">
                    <i class="sort-icon fas fa-clock @if(Request::is('kategorija/*/novo')) text-blue @endif"></i> &nbsp; Novo
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is('kategorija/*/top')) text-blue font-bold @else text-grey-darker @endif" href="#">
                    <i class="sort-icon fas fa-sort-amount-down @if(Request::is('kategorija/*/top')) text-blue @endif"></i> &nbsp; Top
                </a>
            </li>
        </ul>
    </div>
    <div class="text-grey-dark leading-loose uppercase text-xs font-bold tracking-wide pr-4 md:pr-8">
        <span class="text-grey-black">{{ $category->name }}</span>
    </div>
</div>