<div class="py-4 border-b border-grey-lighter flex justify-between">
    <div class="relative">
        <span class="pl-4 md:pl-8 text-grey-dark uppercase text-xs font-bold">Sortiraj: &nbsp;
            <a class="display-sort-nav text-blue font-bold tracking-wide" href="#">
                @if(Request::is(['popularno', '/']))
                <span id="sort-current-icon"><i class="fas fa-fire"></i></span> <span id="sort-current-page">Popularno</span> 
                @elseif(Request::is('novo'))
                <span id="sort-current-icon"><i class="fas fa-clock"></i></span> <span id="sort-current-page">Novo</span>
                @elseif(Request::is('top'))
                <span id="sort-current-icon"><i class="fas fa-sort-amount-down"></i></span> <span id="sort-current-page">Top</span>
                @elseif(Request::is('kategorije'))
                <span id="sort-current-icon"><i class="fas fa-th-large"></i></span> <span id="sort-current-page">Kategorije</span>
                @elseif(Request::is('korisnici'))
                <span id="sort-current-icon"><i class="fas fa-users"></i></span> <span id="sort-current-page">Korisnici</span>
                @endif
                <i class="fas fa-caret-down fa-xs"></i>
            </a>
        </span>
        <ul id="sort-nav" class="homepage-sort-nav list-reset border border-grey-lighter shadow-md bg-white py-1 rounded w-48 absolute mt-2 ml-6 z-20 hidden">
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is(['popularno', '/'])) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="#">
                    <i class="sort-icon fas fa-fire @if(Request::is(['popularno', '/'])) text-blue @endif"></i> &nbsp; Popularno
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is('novo')) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="#">
                    <i class="sort-icon fas fa-clock @if(Request::is('novo')) text-blue @endif"></i> &nbsp; Novo
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is('top')) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="#">
                    <i class="sort-icon fas fa-sort-amount-down @if(Request::is('top')) text-blue @endif"></i> &nbsp; Top
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is('korisnici')) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="{{ url('moje-kategorije') }}">
                    <i class="sort-icon fas fa-users @if(Request::is('korisnici')) text-blue @endif"></i> &nbsp; Korisnici
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is('kategorije')) text-blue font-bold @else text-grey-darker @endif" href="{{ url('moje-kategorije') }}">
                    <i class="sort-icon fas fa-th-large @if(Request::is('kategorije')) text-blue @endif"></i> &nbsp; Kategorije
                </a>
            </li>
        </ul>
    </div>
    <div class="pr-4 md:pr-8 leading-loose text-grey-dark uppercase text-xs font-bold tracking-wide">
        Sve objave
    </div>
</div>
