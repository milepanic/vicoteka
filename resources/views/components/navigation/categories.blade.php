<div class="py-4 border-b border-grey-lighter flex justify-between">
    <div class="relative">
        <span class="pl-4 md:pl-8 text-grey-dark uppercase text-xs font-bold">Sortiraj: &nbsp;
            <span class="display-sort-nav text-blue font-bold tracking-wide cursor-pointer">
                @if(Request::is(['otkrijte-kategorije', 'otkrijte-kategorije/popularne']))
                <span id="sort-current-icon"><i class="fas fa-fire"></i></span> <span id="sort-current-page">Popularne kategorije</span>
                @elseif(Request::is('otkrijte-kategorije/nove'))
                <span id="sort-current-icon"><i class="fas fa-clock"></i></span> <span id="sort-current-page">Nove kategorije</span>
                @elseif(Request::is('otkrijte-kategorije/moje'))
                <span id="sort-current-icon"><i class="fas fa-sort-amount-down"></i></span> <span id="sort-current-page">Moje kategorije</span>
                @elseif(Request::is('otkrijte-kategorije/nepracene'))
                <span id="sort-current-icon"><i class="fas fa-th-large"></i></span> <span id="sort-current-page">Nepraćene kategorije</span>
                @endif
                <i class="fas fa-caret-down fa-xs"></i>
            </span>
        </span>
        <ul id="sort-nav" class="discover-categories-sort-nav list-reset border border-grey-lighter shadow-md bg-white py-1 rounded w-48 absolute mt-2 ml-6 z-20 hidden">
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is(['otkrijte-kategorije', 'otkrijte-kategorije/popularne'])) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="#">
                    <i class="sort-icon fas fa-fire @if(Request::is(['otkrijte-kategorije', 'otkrijte-kategorije/popularne'])) text-blue @endif"></i> &nbsp; Popularne kategorije
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                <a class="block py-2 px-4 text-sm @if(Request::is('otkrijte-kategorije/nove')) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="#">
                    <i class="sort-icon fas fa-clock @if(Request::is('otkrijte-kategorije/nove')) text-blue @endif"></i> &nbsp; Nove kategorije
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                @if(auth()->guest())
                    <a class="open-login-modal block py-2 px-4 text-sm text-grey-darker border-b border-grey-lighter" href="#">
                @else
                    <a class="block py-2 px-4 text-sm @if(Request::is('otkrijte-kategorije/moje')) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="#">
                @endif
                    <i class="sort-icon fas fa-user @if(Request::is('otkrijte-kategorije/moje')) text-blue @endif"></i> &nbsp; Moje kategorije
                </a>
            </li>
            <li class="hover:bg-grey-lightest">
                @if(auth()->guest())
                    <a class="open-login-modal block py-2 px-4 text-sm text-grey-darker border-b border-grey-lighter" href="#">
                @else
                    <a class="block py-2 px-4 text-sm @if(Request::is('otkrijte-kategorije/nepracene')) text-blue font-bold @else text-grey-darker @endif border-b border-grey-lighter" href="{{ url('moje-kategorije') }}">
                @endif
                    <i class="sort-icon fas fa-users @if(Request::is('otkrijte-kategorije/nepracene')) text-blue @endif"></i> &nbsp; Nepraćene kategorije
                </a>
            </li>
        </ul>
    </div>
    <div class="pr-4 md:pr-8 leading-loose text-grey-dark uppercase text-xs font-bold tracking-wide">
        Kategorije
    </div>
</div>
