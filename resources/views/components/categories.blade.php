<div class="infinite-scroll" id="all-categories">

    @forelse($categories as $category)
    <div class="discover-category-box py-4 px-8 border-b flex flex-wrap">
        <div class="w-full sm:w-1/3">
            <a href="{{ url('kategorija/' . $category->slug) }}">
                <img class="lazyload min-w-full h-auto" data-src="{{ asset($category->image) }}">
            </a>
        </div>
        <div class="w-full sm:w-2/3 sm:pl-4 relative">
            <a class="text-lg font-bold" href="{{ url('kategorija/' . $category->slug) }}">{{ $category->name }}</a>
            <p class="text-grey-darkest text-sm mb-16">{{ Illuminate\Support\Str::limit($category->description, 420) }}</p>
            <div class="alignt-bottom absolute pin-b">
                @if(auth()->user() && $category->users->where('id', auth()->id())->first()
                && $category->users->where('id', auth()->id())->first()->pivot->subscribed === 1)
                    <button class="subscribe-btn subscribed mr-4 text-sm py-2 px-4 font-bold rounded shadow" data-id="{{ $category->id }}" data-action="0">Pratiš</button>
                @else
                    <button class="subscribe-btn subscribe mr-4 text-sm py-2 px-4 font-bold rounded shadow" data-id="{{ $category->id }}" data-action="1">Zaprati</button>
                @endif
                <span class="text-xs italic text-grey-darkest">Pratioci: {{ $category->subscribers }}</span>
                @if($category->nsfw)
                    <span class="text-grey-darker text-xs">&nbsp;&#8226; </span><span class="text-xs text-red">18+</span>
                @endif
            </div>
        </div>
    </div>
    @empty
    <div class="flex items-center p-8">
        <img src="{{ asset('images/warning.png') }}" alt="Upozorenje">
        <p class="ml-8 text-lg text-grey-darkest">
            Trenutno nema ni jedne kategorije.
        </p>
    </div>
    @endforelse

    <div hidden>
        @if(request()->input('prikaz') === 'pretplacene-kategorije')
            {{ $categories->appends(['prikaz' => 'pretplacene-kategorije'])->links() }}

        @elseif(request()->input('prikaz') === 'moderator-kategorija')
            {{ $categories->appends(['prikaz' => 'moderator-kategorija'])->links() }}

        @else
            {{ $categories->links() }}

        @endif
    </div>

</div>
