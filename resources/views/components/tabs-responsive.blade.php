<div id="tabs-responsive" class="h-screen overflow-hidden overflow-y-scroll fixed shadow h-screen z-40 w-80 bg-white border-r border-b px-4 pin-l" hidden>
    <div class="pr-4 py-4 my-1">
        <a class="text-grey-darker" id="responsive-caret-left-close" href="#">
            <i class="fas fa-bars"></i>
        </a>
    </div>
    <a class="block border-b mb-2 pb-2 text-md {{ Request::is('popularno') || Request::is('/') ? 'font-bold' : '' }}" 
        href="{{ url('/popularno') }}">Popularno</a>
    <a class="block border-b mb-2 pb-2 text-md {{ Request::is('novo') ? 'font-bold' : '' }}" 
        href="{{ url('/novo') }}">Novo</a>
    <a class="block border-b mb-2 pb-2 text-md {{ Request::is('top') ? 'font-bold' : '' }}" 
        href="{{ url('/top') }}">Top</a>
    @if(auth()->check() && ! Request::is('kategorija/*'))
    <a class="block border-b mb-2 pb-2 text-md {{ Request::is('kategorije') ? 'font-bold' : '' }}" 
        href="{{ url('/kategorije') }}">Kategorije</a>
    <a class="block mb-2 pb-2 text-md {{ Request::is('korisnici') ? 'font-bold' : '' }}" 
        href="{{ url('/korisnici') }}">Korisnici</a>
    @endif
</div>