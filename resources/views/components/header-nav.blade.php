<header class="fixed w-full z-30 bg-white border-b border-grey-light">
    <div class="container mx-auto">
        <div class="flex justify-between xl:items-center py-2 xl:py-4 xl:border-b xl:border-grey-light">
            <div class="p-3 xl:hidden">
                <a class="text-grey-darker" id="responsive-caret-left" href="#">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
            <div class="xl:w-1/5">
                <a href="{{ url('/') }}">
                    <h1 class="inline text-blue font-bold font-cursive">Distopija</h1>
                </a>
            </div>
            <div class="hidden xl:block xl:w-2/5">
                <div id="search-box" class="relative">
                    <input id="search" class="py-2 border border-1 border-grey-light pl-4 pr-8 rounded w-full text-black text-sm" 
                        type="text" placeholder="Pretraga...">
                    <div class="absolute pin-r pin-y flex items-center px-3 text-grey cursor-pointer text-sm">
                        <i class="fas fa-search"></i>
                    </div>
                    <div hidden id="search-results" class="absolute border shadow w-full bg-white z-20">
                        <div class="w-full bg-grey-lightest border-b">
                            <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Objave</span>
                        </div>
                        <div id="search-results-posts"></div>
                        <div class="w-full bg-grey-lightest border-b">
                            <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Kategorije</span>
                        </div>
                        <div id="search-results-categories"></div>
                        <div class="w-full bg-grey-lightest border-b">
                            <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Korisnici</span>
                        </div>
                        <div id="search-results-users"></div>
                    </div>
                </div>
            </div>
            <div class="hidden xl:block xl:w-2/5">
                @include('components/user-navigation')
            </div>
            <div class="p-3 xl:hidden">
                <a class="text-grey-darker" id="responsive-caret" href="#">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div id="filter-tabs" class="hidden xl:block xl:flex xl:justify-left">
            {{-- <p class="font-bold py-4 mr-6">{{ $category->name }}</p> --}}
            <a class="{{ Request::is(['popularno', '/', 'kategorija/*/popularno']) || 
                ! Request::is(['novo', 'top', '*/novo', '*/top', 'kategorije', 'korisnici']) ? 'nav-active' : '' }} nav-item mr-3"
                @if(Request::is('*') && ! Request::is('kategorija/*'))
                href="{{ url('popularno') }}"
                @elseif(Request::is('kategorija/*'))
                href="{{ url('kategorija/' . $category->slug . '/popularno') }}"
                @endif>Popularno</a>
            <a class="{{ Request::is(['novo', '*/novo']) ? 'nav-active' : '' }} nav-item mr-3"
                @if(Request::is('*') && ! Request::is('kategorija/*'))
                href="{{ url('novo') }}"
                @elseif(Request::is('kategorija/*'))
                href="{{ url('kategorija/' . $category->slug . '/novo') }}"
                @endif>Novo</a>
            <a class="{{ Request::is(['top', '*/top']) ? 'nav-active' : '' }} nav-item mr-3" 
                @if(Request::is('*') && ! Request::is('kategorija/*'))
                href="{{ url('top') }}"
                @elseif(Request::is('kategorija/*'))
                href="{{ url('kategorija/' . $category->slug . '/top') }}"
                @endif>Top</a>
            @if(auth()->check() && !Request::is('kategorija/*'))
            <a class="{{ Request::is('kategorije') ? 'nav-active' : '' }} nav-item mr-3" 
                href="{{ url('kategorije') }}">Kategorije</a>
            <a class="{{ Request::is('korisnici') ? 'nav-active' : '' }} nav-item" 
                href="{{ url('korisnici') }}">Korisnici</a>
            @endif
        </div>
    </div>
</header>
<div class="mb-16 w-full h-4 xl:h-24"></div>
