{{-- Report modal --}}
<div class="fixed pin z-50 overflow-auto bg-smoke-darker flex">
    <div class="relative px-2 bg-white rounded shadow-lg w-full max-w-md m-auto flex-col flex border-t-4 border-blue">
        {{-- Close button --}}
        <span class="absolute pin-t pin-b pin-r p-4">
            <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                <i class="fas fa-times"></i>
            </a>
        </span>

        {{-- Content --}}
        <form class="px-1 md:px-4 py-4" action="{{ url('feedback') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <h3 class="text-grey-darkest uppercase mb-6 tracking-wide">Prijavite problem</h3>

            <div class="mb-4">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Tekst</label>
                <textarea class="w-full bg-grey-lightest p-4 text-grey-darkest rounded border border-grey-light" name="body" rows="6" autofocus></textarea>
            </div>
            <div class="mb-8">
                <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-1">Screenshot</label>
                <input type="file" name="image">
            </div>

            <button class="w-full bg-blue hover:bg-blue-dark rounded py-3 uppercase text-grey-light font-semibold text-sm tracking-wide" type="submit">Pošalji</button>
        </form>
    </div>
</div>