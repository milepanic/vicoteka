<div id="sidebar-responsive" class="h-screen overflow-hidden overflow-y-scroll fixed shadow z-40 w-80 bg-white border-l border-b px-4 pin-r" hidden>
    <div class="pl-4 py-4 my-1 flex justify-end">
        <button class="text-grey-darker p-2 -m-2" id="responsive-caret-close">
            <i class="fas fa-bars"></i>
        </button>
    </div>
    @guest
    <ul class="list-reset py-1 border-b mb-6 pb-6">
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-login-modal block py-2 px-4 text-sm text-grey-darkest" href="{{ url('login') }}"><i class="text-grey-dark fas fa-sign-in-alt"></i> Prijava</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-register-modal block py-2 px-4 text-sm text-grey-darkest" href="{{ url('register') }}"><i class="text-grey-dark fas fa-user-circle"></i> Registracija</a>
        </li>
    </ul>
    @endguest
    @auth
    <ul class="list-reset py-1 border-b mb-6 pb-6">
        <button class="bg-blue text-grey-lighter hover:bg-blue-dark text-sm font-bold tracking-wide w-full mb-4 py-2 px-4 rounded shadow" id="header-open-submit-modal"><i class="fas fa-sm fa-plus"></i> Postavi</button>
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('profil/' . auth()->user()->slug) }}"><i class="text-grey-dark fas fa-user"></i> Profil</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-create-modal block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-plus-circle"></i> Napravi kategoriju</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('moje-kategorije') }}"><i class="text-grey-dark fas fa-th-large"></i> Moje kategorije</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-ban"></i> Blokirane kategorije</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-report-modal block py-2 px-4 text-sm text-grey-darkest border-b border-grey-lighter" href="#"><i class="text-grey-dark fas fa-bug"></i> Prijavi problem</a>
        </li>
        @if(auth()->user()->admin)
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest border-b border-grey-lighter" href="{{ url('admin') }}"><i class="text-grey-dark fas fa-lock-open"></i> Admin Panel</a>
        </li>
        @endif
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('/logout') }}"><i class="text-grey-dark fas fa-sign-out-alt"></i> Odjavite se</a>
        </li>
    </ul>
    @endauth

    <div class="border-b mb-6 pb-6">
        <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
            Objave</h4>
        <ul class="list-reset px-4">
            <li class="py-2 border-b border-grey-lighter flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=postovi') }}">Postovi</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->posts_count }}</span>
            </li>
            <li class="py-2 flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=komentari') }}">Komentari</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->comments_count }}</span>
            </li>
        </ul>
    </div>

    <div class="border-b mb-6 pb-6">
        <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
            Korisnici</h4>
        <ul class="list-reset px-4">
            <li class="py-2 border-b border-grey-lighter flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=pratioci') }}">Pratioci</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->followers }}</span>
            </li>
            <li class="py-2 flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=prati') }}">Prati</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->following }}</span>
            </li>
        </ul>
    </div>
    
    <div class="border-b mb-6 pb-6">
        <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
            Kategorije</h4>
        <ul class="list-reset px-4">
            <li class="py-2 border-b border-grey-lighter flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=pretplacene-kategorije') }}">Pretplacene kategorije</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->subscribed }}</span>
            </li>
            <li class="py-2 flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=moderator-kategorija') }}">Vlasnik kategorija</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->moderator }}</span>
            </li>
        </ul>
    </div>

    <div class="border-b mb-6 pb-6">
        <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
            Aktivnost</h4>
        <ul class="list-reset px-4">
            <li class="py-2 border-b border-grey-lighter flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=omiljeni-postovi') }}">Omiljeni postovi</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->favorites }}</span>
            </li>
            <li class="py-2 border-b border-grey-lighter flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=upvoteovani-postovi') }}">Upvotovani postovi</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->upvoted }}</span>
            </li>
            <li class="py-2 flex justify-between">
                <a class="text-sm text-blue-dark hover:underline pr-2" href="{{ url('profil/' . $user->slug . '?prikaz=downvoteovani-postovi') }}">Downvoteovani postovi</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $user->downvoted }}</span>
            </li>
        </ul>
    </div>

    <div class="pb-6">
        @include('components/footer')
    </div>

</div>
