<div id="tabs-responsive" class="h-screen overflow-hidden overflow-y-scroll fixed shadow h-screen z-40 w-80 bg-white border-r border-b px-4 pin-l" hidden>
    <div class="pr-4 py-4 mt-1 mb-8">
        <button class="text-grey-darker p-2 -m-2" id="responsive-caret-left-close">
            <i class="fas fa-bars"></i>
        </button>
    </div>
    <img class="rounded-full border-4 border-white shadow" src="{{ asset($user->image_lg) }}" alt="{{ $user->username }}"> 

    <div class="py-4 mb-2">
        <div class="font-bold text-xl mb-3 text-center">{{ $user->username }}</div>
        <p class="text-grey-darkest text-sm">{{ $user->description }}</p>
    </div>

    @auth
        @if($user->id !== auth()->user()->id)
            @if(auth()->user()->following->contains($user->id))
                {{-- follow btn jquery selector --}}
                <button class="follow-btn following py-3 px-8 w-full uppercase border-red rounded shadow border font-medium tracking-wide">Pratite</button>
            @else
                <button class="follow-btn follow py-3 px-8 w-full uppercase border-red rounded shadow border font-medium tracking-wide">Zaprati</button>
            @endif
        @else
            <button class="open-edit-profile-modal py-3 px-8 w-full text-grey-darker border-grey-darker uppercase rounded shadow border font-medium tracking-wide">Izmeni profil</button>
        @endif
    @endauth
    
    <div class="mt-6 flex justify-around">
        @if($user->facebook)
        <a href="{{ $user->facebook }}" target="_blank">
            <i class="fab fa-facebook-f fa-2x"></i>
        </a>
        @endif
        @if($user->twitter)
        <a href="{{ $user->twitter }}">
            <i class="fab fa-twitter fa-2x"></i>
        </a>
        @endif
        @if($user->instagram)
        <a href="{{ $user->instagram }}">
            <i class="fab fa-instagram fa-2x"></i>
        </a>
        @endif
        @if($user->tumblr)
        <a href="{{ $user->tumblr }}">
            <i class="fab fa-tumblr fa-2x"></i>
        </a>
        @endif
    </div>
    @if(auth()->user() && $user->id === auth()->id())
    <div class="mt-2 text-center">
        <a class="text-xs text-blue" href="{{ url('profil/blokirane-kategorije') }}">Blokirane kategorije</a>
    </div>
    @endif
</div>