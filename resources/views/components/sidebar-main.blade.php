{{-- <button class="mb-6 w-full bg-blue hover:bg-blue-dark text-grey-lighter text-sm font-bold uppercase tracking-wide py-4 rounded shadow">Submit joke</button> --}}
<div class="mb-6 py-2 rounded shadow bg-white">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
        Izabrane objave</h4>
    <div class="flex px-2">
        <div class="open-bestOfToday-modal w-full flex flex-col items-center px-1 py-1 mx-1 hover:bg-grey-lightest cursor-pointer" 
            title="Najbolja objava dana">
            <div class="text-grey-darker mb-2">
                {{-- <i class="fa-3x fas fa-sun"></i> --}}
                <img src="{{ asset('images/medal4.png') }}" alt="Medalja">
                {{-- <img src="{{ asset('images/optimised.svg') }}" alt="Medalja"> --}}
            </div>
            <div class="text-sm">Dana</div>
        </div>        

        <div class="open-bestOfWeek-modal w-full flex flex-col items-center px-1 py-1 mx-1 hover:bg-grey-lightest cursor-pointer"
            title="Najbolja objava sedmice">
            <div class="text-grey-darker mb-2">
                <img src="{{ asset('images/star4.png') }}" alt="Medalja">
                {{-- <i class="fa-3x fas fa-star"></i>  --}}
            </div>
            <div class="text-sm">Sedmice</div>
        </div>

        <div class="open-bestOfMonth-modal w-full flex flex-col items-center px-1 py-1 mx-1 hover:bg-grey-lightest cursor-pointer"
            title="Najbolja objava meseca">
            <div class="text-grey-darker mb-2">
                {{-- <i class="fa-3x fas fa-trophy"></i> --}}
                <img src="{{ asset('images/trophy4.png') }}" alt="Medalja">
            </div>
            <div class="text-sm">Meseca</div>
        </div>

    </div>
</div>
<div class="my-6 py-2 rounded shadow bg-white">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-4 px-4">
        Popularne kategorije</h4>
    <div class="px-2">
        <a href="{{ url('kategorija/' . $bestCategory->slug) }}">
            <img class="rounded shadow" src="{{ asset($bestCategory->image) }}">
        </a>
        <div>
            <a class="text-lg mb-1 font-bold text-center block" href="{{ url('kategorija/' . $bestCategory->slug) }}">{{ $bestCategory->name }}</a>
            <p class="text-grey-darkest text-xs">{{ $bestCategory->description }}</p>
            <div class="flex justify-between mt-3">
                <span class="text-xs italic text-grey-darker">Pratioci: 
                    <span id="subscribers-count">{{ $bestCategory->followers }}</span>
                </span>
                @if(auth()->user() && $bestCategory->users->where('id', auth()->id())->first()
                    && $bestCategory->users->where('id', auth()->id())->first()->pivot->subscribed === 1)
                    <button class="subscribe-btn subscribed text-sm my-2 py-2 px-4 font-bold rounded shadow" data-id="{{ $bestCategory->id }}" data-action="0">Pratiš</button>
                @else
                    <button class="subscribe-btn subscribe text-sm my-2 py-2 px-4 font-bold rounded shadow" data-id="{{ $bestCategory->id }}" data-action="1">Zaprati</button>
                @endif
            </div>
        </div>
    </div>
    <div class="pt-2 pr-4 text-center">
        <a class="text-xs text-blue hover:underline" href="{{ url('otkrijte-kategorije') }}">Sve kategorije</a>
    </div>
</div>
@auth
<div class="my-6 py-2 rounded shadow bg-white">
    <h4 class="mb-6 px-4 uppercase text-center tracking-wide text-grey-darkest">Preporučeni korisnici</h4>
    <ul id="sidebar-users" class="list-reset px-4">

        @foreach($recommendedUsers as $user)

        <li class="mb-3 @if(!$loop->last) border-b border-grey-lighter pb-2 @endif">
            <div class="flex items-center relative">
                <a href="{{ url('profil/' . $user->slug) }}">
                    <img class="w-12 h-12 rounded-full" 
                        src="{{ asset($user->image_sm) }}">
                </a>
                <div class="ml-3 flex flex-col">
                    <a class="text-grey-darkest font-medium -mt-3 mb-1 truncate" 
                        href="{{ url('profil/' . $user->slug) }}">{{ $user->username }}</a>
                    <div>
                        <button class="follow-btn-sidebar py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Zaprati</button>
                    </div>
                </div>
                <span class="absolute pin-t pin-b pin-r p-4">
                    <a class="new-follow h-12 w-12 text-grey-light hover:text-grey-darkest" href="#" title="Nova preporuka">
                        <i class="fas fa-times"></i>
                    </a>
                </span>
            </div>
        </li>

        @endforeach

    </ul>
    <div class="pt-2 pr-4 text-center">
        <a class="more-users text-xs text-blue hover:underline" href="{{ url('otkrijte-korisnike') }}">Još korisnika</a>
    </div>
</div>
@endauth
{{-- <div class="my-6 py-2 rounded shadow bg-white">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
        Popularne kategorije</h4>
    <ul class="list-reset px-4">
        @foreach($bestCategories as $category)
        <li class="py-2 @if(!$loop->last) border-b border-grey-lighter @endif flex justify-between">
            <a class="text-sm text-blue-dark hover:underline truncate pr-2" href="{{ url('kategorija/' . $category->slug) }}">{{ $category->name }}</a>
            <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $category->followers }} pratilaca</span>
        </li>
        @endforeach
    </ul>
    <div class="pt-2 pr-4 text-center">
        <a class="text-xs text-blue hover:underline" href="{{ url('otkrijte-kategorije') }}">Još kategorija</a>
    </div>
</div> --}}
<div class="my-6">
    @include('components/footer')
</div>
