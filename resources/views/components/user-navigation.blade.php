<div class="flex justify-end -mb-2">

@auth

    @if(! Request::is(['kategorija/*', 'objava/*']))
    <div class="pt-1">
        <button class="bg-blue text-grey-lighter hover:bg-blue-dark mr-6 text-sm font-bold tracking-wide py-2 px-4 rounded shadow" id="main-header-open-submit-modal"><i class="fas fa-sm fa-plus"></i> Postavi</button>
    </div>
    @endif
    <div class="relative text-grey-darkest mr-4 p-2">
        <img id="notification-bell" class="cursor-pointer" src="{{ asset('images/notification.png') }}" alt="notifikaciono zvono">
        @if(auth()->user()->unreadNotifications->count() > 0)
            <div id="notification-count" class="absolute cursor-pointer pin-t pin-r -mr-1 bg-red rounded-full w-6 h-6 text-center">
                <span class="text-xs font-bold text-white">{{ auth()->user()->unreadNotifications->count() }}</span>
            </div>
        @endif
        <div id="notification-body" class="absolute hidden pin-r mt-4 border border-grey-light bg-white rounded shadow w-112">
            <div class="bg-blue text-white tracking-wide font-bold rounded-t text-sm py-3 pl-4 flex justify-between">
                Obaveštenja
                <div class="mr-4">
                    <span id="notification-mark-read" class="p-2 cursor-pointer">
                        <i class="fas fa-check"></i>
                    </span>
                    {{-- <span class="p-2 cursor-pointer">
                        <i class="fas fa-cog"></i>
                    </span> --}}
                </div>
            </div>
            <div class="text-sm text-grey-darkest h-128 overflow-hidden overflow-y-scroll">
                @foreach(auth()->user()->notifications as $notification)

                    <a class="notification-single block px-4 py-3 flex @if(is_null($notification->read_at)) bg-grey-lighter @endif hover:bg-grey-lightest cursor-pointer" href="{{ array_get($notification->data, 'url') }}">
                        <img class="w-10 h-10 rounded-full" src="{{ asset(array_get($notification->data, 'image')) }}">
                        <span class="ml-3">
                            {!! array_get($notification->data, 'data') !!}
                            <br>
                            <span class="text-grey-dark text-xs">{{ $notification->created_at->diffForHumans() }}</span>
                        </span>
                    </a>

                @endforeach
            </div>
            <div class="text-center py-2 border-t">
                {{-- <a class="text-blue text-xs hover:underline" href="#">Pogledaj sve</a> --}}
            </div>
        </div>
    </div>
    <a class="display-dropdown-nav mr-2 mt-2 text-base text-grey-darkest text-sm" 
        href="#"><i class="fas fa-caret-down fa-xs mr-1"></i>{{ auth()->user()->username }}</a>

    <a class="display-dropdown-nav" href="#">
        <img class="w-10 h-10 rounded-full" src="{{ asset(auth()->user()->image_sm) }}" alt="{{ auth()->user()->username }}">
    </a>
    <ul id="dropdown-nav" class="list-reset border border-grey-light bg-white py-1 rounded shadow-md w-48 mt-12 absolute hidden">
        <li class="hover:bg-grey-lightest">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('profil/' . auth()->user()->slug) }}"><i class="text-grey-dark fas fa-user"></i> Profil</a>
        </li>
        <li class="hover:bg-grey-lightest">
            <a class="open-create-modal block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-plus-circle"></i> Napravi kategoriju</a>
        </li>
        <li class="hover:bg-grey-lightest">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('moje-kategorije') }}"><i class="text-grey-dark fas fa-th-large"></i> Moje kategorije</a>
        </li>
        <li class="hover:bg-grey-lightest">
            <a class="open-feedback-modal block py-2 px-4 text-sm text-grey-darkest border-b" href="#"><i class="text-grey-dark fas fa-bug"></i> Prijavi problem</a>
        </li>
        @if(auth()->user()->admin)
        <li class="hover:bg-grey-lightest">
            <a class="block py-2 px-4 text-sm text-grey-darkest border-b" href="{{ url('admin') }}"><i class="text-grey-dark fas fa-lock-open"></i> Admin Panel</a>
        </li>
        @endif
        <li class="hover:bg-grey-lightest">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('/logout') }}"><i class="text-grey-dark fas fa-sign-out-alt"></i> Odjavite se</a>
        </li>
    </ul>
@endauth

@guest
    {{-- open-login-modal jquery selector --}}
    <a class="open-login-modal mr-6 text-grey-darker hover:text-grey-darkest mb-2 font-bold text-sm tracking-wide py-2" 
        href="#">Prijava</a>
    <a class="open-register-modal text-grey-darker hover:text-grey-darkest mb-2 font-bold text-sm tracking-wide py-2" 
        href="#">Registracija</a>
@endguest

</div>
