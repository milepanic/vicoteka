<div class="infinite-scroll">

    @forelse($comments as $comment)

    <div class="p-4 border-t flex bg-grey-lightest">
        <a class="mr-2 flex flex-no-shrink" href="{{ url('profil/' . $comment->user->slug) }}">
            <img class="rounded-full w-10 h-10" src="{{ asset($comment->user->image_sm) }}">
        </a>
        <div>
            <div class="flex justify-start">
                <a class="font-bold text-sm" href="{{ url('profil/' . $comment->user->slug) }}">{{ $comment->user->username }}</a>
                <span class="ml-2 text-grey-dark text-xs">{{ $comment->diffCreatedAt }}</span>
            </div>
            <p class="font-sans w-full text-grey-black text-xs md:text-sm my-1">{{ $comment->body }}</p>
            <div class="flex justify-start text-xs mt-2" data-id="{{ $comment->id }}">
                    <a class="comment-vote-btn mr-3 text-grey-dark" data-type="upvote" href="#">
                        <i class="fas fa-arrow-up"></i> <span> {{ $comment->upvotes_count }} </span>
                    </a>
                    <a class="comment-vote-btn mr-3 text-grey-dark" data-type="downvote" href="#">
                        <i class="fas fa-arrow-down"></i> <span> {{ $comment->downvotes_count }} </span>
                    </a>
                    <a class="open-report-modal text-grey-dark" data-type="comment" data-id="{{ $comment->id }}" data-post="{{ $comment->post->id }}" href="#">prijavi</a>
                </div>
        </div>
    </div>
    @empty
    <div class="flex items-center p-8">
        <img src="{{ asset('images/warning.png') }}" alt="Upozorenje">
        <p class="ml-8 text-lg text-grey-darkest">
            Trenutno nema komentara.
        </p>
    </div>
    @endforelse

    <div hidden>
        {{ $comments->appends(['prikaz' => 'komentari'])->links() }}
    </div>

</div>
