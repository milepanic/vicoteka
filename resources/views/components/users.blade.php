<div class="infinite-scroll">

    @forelse($users as $user)
    <div class="discover-user-box py-4 px-8 border-b flex flex-wrap">
        <div class="w-full sm:w-1/3">
            <a href="{{ url('profil/' . $user->slug) }}">
                <img class="rounded-full border-4 border-white" src="{{ asset($user->image_lg) }}" alt="{{ $user->username }}"> 
            </a>
        </div>
        <div class="w-full sm:w-2/3 sm:pl-4 relative">
            <a class="text-lg font-bold" href="{{ url('profil/' . $user->slug) }}">{{ $user->username }}</a>
            <p class="text-grey-darkest text-sm mb-16">{{ Illuminate\Support\Str::limit($user->description, 420) }}</p>
            <div class="alignt-bottom absolute pin-b">
                @if(Request::is('profil/*'))
                @auth
                    @if($user->id !== auth()->id())
                        @if(auth()->user()->following->contains($user->id))
                            {{-- follow btn jquery selector --}}
                            <button class="unfollow-btn py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Pratite</button> &nbsp;
                        @else
                            <button class="follow-btn py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Pratite</button> &nbsp;
                        @endif
                    @endif
                @endauth
                
                @else
                <button class="follow-btn-discover py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Zaprati</button> &nbsp;
                @endif
                <span class="text-xs italic text-grey-darkest">Pratioci: {{ $user->followers }}</span>
            </div>
        </div>
    </div>
    @empty
    <div class="flex items-center p-8">
        <img src="{{ asset('images/warning.png') }}" alt="Upozorenje">
        <p class="ml-8 text-lg text-grey-darkest">
            Trenutno nema korisnika. <br>Pronađite još korisnika <a class="text-blue" href="{{ url('otkrijte-korisnike') }}">ovde</a>.
        </p>
    </div>
    @endforelse

    <div hidden>
        @if(request()->input('prikaz') === 'prati')
            {{ $users->appends(['prikaz' => 'prati'])->links() }}

        @elseif(request()->input('prikaz') === 'pratioci')
            {{ $users->appends(['prikaz' => 'pratioci'])->links() }}

        @else
            {{ $users->links() }}

        @endif
    </div>

</div>
