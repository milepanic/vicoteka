<div id="sidebar-responsive" class="h-screen overflow-hidden overflow-y-scroll fixed shadow z-40 w-80 bg-white border-l border-b px-4 pin-r" hidden>
    <div class="pl-4 py-4 my-1 flex justify-end">
        <button class="text-grey-darker p-2 -m-2" id="responsive-caret-close">
            <i class="fas fa-bars"></i>
        </button>
    </div>
    @guest
    <ul class="list-reset py-1 border-b mb-6 pb-6">
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-login-modal block py-2 px-4 text-sm text-grey-darkest" href="{{ url('login') }}"><i class="text-grey-dark fas fa-sign-in-alt"></i> Prijava</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-register-modal block py-2 px-4 text-sm text-grey-darkest" href="{{ url('register') }}"><i class="text-grey-dark fas fa-user-circle"></i> Registracija</a>
        </li>
    </ul>
    @endguest
    @auth
    <ul class="list-reset py-1 border-b mb-6 pb-6">
        <button class="bg-blue text-grey-lighter hover:bg-blue-dark text-sm font-bold tracking-wide w-full mb-4 py-2 px-4 rounded shadow" id="header-open-submit-modal"><i class="fas fa-sm fa-plus"></i> Postavi</button>
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('profil/' . auth()->user()->slug) }}"><i class="text-grey-dark fas fa-user"></i> Profil</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-create-modal block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-plus-circle"></i> Napravi kategoriju</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('moje-kategorije') }}"><i class="text-grey-dark fas fa-th-large"></i> Moje kategorije</a>
        </li>
        <li class="hover:bg-grey-lightest text-center">
            <a class="open-feedback-modal block py-2 px-4 text-sm text-grey-darkest border-b border-grey-lighter" href="#"><i class="text-grey-dark fas fa-bug"></i> Prijavi problem</a>
        </li>
        @if(auth()->user()->admin)
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest border-b border-grey-lighter" href="{{ url('admin') }}"><i class="text-grey-dark fas fa-lock-open"></i> Admin Panel</a>
        </li>
        @endif
        <li class="hover:bg-grey-lightest text-center">
            <a class="block py-2 px-4 text-sm text-grey-darkest" href="{{ url('/logout') }}"><i class="text-grey-dark fas fa-sign-out-alt"></i> Odjavite se</a>
        </li>
    </ul>
    @endauth
    
    <div class="border-b mb-6 pb-6">
        <div id="search-box-responsive" class="relative">
            <input id="search-responsive" class="py-2 border border-1 border-grey-light pl-4 pr-8 rounded w-full text-black text-sm" 
                type="text" placeholder="Pretraga...">
            <div class="absolute pin-r pin-y flex items-center px-3 text-grey hover:text-grey-darker cursor-pointer text-sm">
                <i class="fas fa-search"></i>
            </div>
            <div hidden id="search-results-responsive" class="absolute border shadow w-full bg-white z-20">
                <div class="w-full bg-grey-lightest border-b">
                    <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Objave</span>
                </div>
                <div id="search-results-posts-responsive"></div>
                <div class="w-full bg-grey-lightest border-b">
                    <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Kategorije</span>
                </div>
                <div id="search-results-categories-responsive"></div>
                <div class="w-full bg-grey-lightest border-b">
                    <span class="text-xs tracking-wide text-grey-darker font-semibold uppercase px-2 py-1">Korisnici</span>
                </div>
                <div id="search-results-users-responsive"></div>
            </div>
        </div>    
    </div>

    <div class="border-b mb-6 pb-6">
        <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
            Izabrane objave
        </h4>
        <div class="flex px-2">
            <div class="open-bestOfToday-modal w-full flex flex-col items-center px-1 py-1 mx-1 hover:bg-grey-lightest cursor-pointer" 
                title="Najbolja objava dana">
                <div class="text-grey-darker mb-2">
                    {{-- <i class="fa-3x fas fa-sun"></i> --}}
                    <img src="{{ asset('images/medal4.png') }}" alt="Medalja">
                    {{-- <img src="{{ asset('images/optimised.svg') }}" alt="Medalja"> --}}
                </div>
                <div class="text-sm">Dana</div>
            </div>

            <div class="open-bestOfWeek-modal w-full flex flex-col items-center px-1 py-1 mx-1 hover:bg-grey-lightest cursor-pointer"
                title="Najbolja objava sedmice">
                <div class="text-grey-darker mb-2">
                    <img src="{{ asset('images/star4.png') }}" alt="Medalja">
                    {{-- <i class="fa-3x fas fa-star"></i>  --}}
                </div>
                <div class="text-sm">Sedmice</div>
            </div>

            <div class="open-bestOfMonth-modal w-full flex flex-col items-center px-1 py-1 mx-1 hover:bg-grey-lightest cursor-pointer"
                title="Najbolja objava meseca">
                <div class="text-grey-darker mb-2">
                    {{-- <i class="fa-3x fas fa-trophy"></i> --}}
                    <img src="{{ asset('images/trophy4.png') }}" alt="Medalja">
                </div>
                <div class="text-sm">Meseca</div>
            </div>

            <div class="modal" id="modal-bestOfMonth-post" hidden>
                {{-- @include('components/modal-bestOf-post', array('post' => $bestOfMonthPost)) --}}
                @include('components/modal-bestOf-post')
            </div>
        </div>
    </div>

    <div class="border-b mb-6 pb-6">
        @auth
        <h4 class="mb-6 px-4 uppercase text-center tracking-wide text-grey-darkest">Preporučeni korisnici</h4>
        <ul id="sidebar-users-responsive" class="list-reset px-4">

            @foreach($recommendedUsers as $user)

            <li class="mb-3 @if(!$loop->last) border-b border-grey-lighter pb-2 @endif">
                <div class="flex items-center relative">
                    <a href="{{ url('profil/' . $user->slug) }}">
                        <img class="w-12 h-12 rounded-full" 
                            src="{{ $user->image_sm }}">
                    </a>
                    <div class="ml-3 flex flex-col">
                        <a class="text-grey-darkest font-medium -mt-3 mb-1 truncate" 
                            href="{{ url('profil/' . $user->slug) }}">{{ $user->username }}</a>
                        <div>
                            <button class="follow-btn-sidebar py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Zaprati</button>
                        </div>
                    </div>
                    <span class="absolute pin-t pin-b pin-r p-4">
                        <a class="new-follow h-12 w-12 text-grey-light hover:text-grey-darkest" href="#" title="Nova preporuka">
                            <i class="fas fa-times"></i>
                        </a>
                    </span>
                </div>
            </li>

            @endforeach

        </ul>
        <div class="pt-2 pr-4 text-center">
            <a class="more-users text-xs text-blue hover:underline" href="{{ url('otkrijte-korisnike') }}">Još korisnika</a>
        </div>
        @endauth
    </div>

    <div class="border-b mb-6 pb-6">
        <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">
        Popularne kategorije</h4>
        <ul class="list-reset px-4">
            @foreach($bestCategories as $category)
            <li class="py-2 @if(!$loop->last) border-b border-grey-lighter @endif flex justify-between">
                <a class="text-sm text-blue-dark hover:underline truncate pr-2" href="{{ url('kategorija/' . $category->slug) }}">{{ $category->name }}</a>
                <span class="text-xs italic text-grey-darker whitespace-no-wrap">{{ $category->followers }} pratilaca</span>
            </li>
            @endforeach
        </ul>
        <div class="pt-2 pr-4 text-center">
            <a class="text-xs text-blue hover:underline" href="{{ url('otkrijte-kategorije') }}">Još kategorija</a>
        </div>
    </div>

    <div class="pb-6">
        @include('components/footer')
    </div>

</div>
