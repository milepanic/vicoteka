{{-- Post modal --}}
    <div class="fixed pin overflow-auto bg-smoke-darker flex ">
        <div class="relative px-4 bg-white rounded shadow-lg w-full max-w-md m-auto flex-col flex">
    
            {{-- Close button --}}
            <span class="absolute pin-t pin-b pin-r p-4">
                <a class="close-modal h-12 w-12 text-grey hover:text-grey-darkest" href="#" title="Close">
                    <i class="fas fa-times"></i>
                </a>
            </span>

            {{-- Content --}}
            <div class="p-4">
                <div class="flex mb-2 md:mb-3">
                    <a class="modal-bestof-user-slug" href="">
                        <img class="modal-bestof-user-image rounded-full w-10 h-10" src="">
                    </a>
                    <div class="ml-2 md:ml-4">
                        <a class="modal-bestof-user-slug text-grey-black" href="">
                            <h4 class="modal-bestof-user-username text-sm md:text-base font-bold -mb-1"></h4>
                        </a>
                        <span class="text-xs text-grey-dark">
                            <a class="open-post-modal modal-bestof-created text-grey-dark hover:text-grey-darker hover:underline" href="#"></a> &#8226; 
                            <a class="modal-bestof-category-slug text-grey-dark hover:text-grey-darker hover:underline" href=""></a>
                            <span class="modal-bestof-nsfw hidden">&#8226; <span class="text-red">18+</span></span>
                        </span>
                    </div>
                </div>
                
                <p class="modal-bestof-body font-sans text-grey-black text-xs md:text-sm mb-4"></p>
                <div class="modal-bestof-image text-center">
                    <img class="mb-4 w-full h-auto">
                </div>
                <div class="modal-bestof-video"></div>
                {{-- <iframe class="modal-bestof-video" width="560" height="316" frameborder="0" allowfullscreen></iframe> --}}
                <div class="flex justify-between">
                    <div class="calll flex items-center mr-4">
                        {{-- vote-btn jquery selector --}}
                        <a class="vote-btn mr-4 text-sm md:text-base text-grey hover:text-grey-darker" data-type="upvote" href="">
                            <i class="fas fa-arrow-up"></i>
                            <span class="modal-bestof-count-upvotes"></span>
                        </a>
                        {{-- vote-btn jquery selector --}}
                        <a class="vote-btn mr-4 text-grey text-sm md:text-base hover:text-grey-darker" data-type="downvote" href="">
                            <i class="fas fa-arrow-down"></i>
                            <span class="modal-bestof-count-downvotes"></span>
                        </a>
                        {{-- favorite-btn jquery selector --}}
                        <a class="favorite-btn mr-4 text-grey text-sm md:text-base hover:text-grey-darker " href="">
                            <i class="far fa-star"></i>
                            <span class="modal-bestof-count-favorites"></span>
                        </a>
                    </div>
                    <div class="float-right">
                        <a class="btn-comments mr-4 text-grey text-sm md:text-base hover:text-grey-darker" href="#">
                            <i class="fas fa-comments"></i>
                            <span class="modal-bestof-count-comments"></span>
                        </a>
                        <a class="display-dropdown-post text-grey text-sm md:text-base hover:text-grey-darker" href="#">
                            <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-post list-reset border border-grey-light bg-white py-1 rounded absolute pin-b pin-r w-48 mb-12 hidden">
                            <li class="hover:bg-grey-lightest">
                                <a class="block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-flag"></i> Prijavi vic</a>
                            </li>
                            <li class="hover:bg-grey-lightest">
                                <a class="block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-user-times"></i> Prijavi korisnika</a>
                            </li>
                            <li class="hover:bg-grey-lightest">
                                <a class="block py-2 px-4 text-sm text-grey-darkest" href="#"><i class="text-grey-dark fas fa-times"></i> Blokiraj kategoriju</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            {{-- Comments --}}
            {{-- <div class="comment-box bg-grey-lightest border-b px-6">
                @auth
                <div class="flex py-4">
                    <img class="rounded-full w-10 h-10 mr-3" 
                        src="{{ asset(auth()->user()->image_sm) }}" alt="">
                    write-comment jquery selector
                    <input class="write-comment block w-full rounded-full border border-grey-light px-4" type="text" name="comment" placeholder="Comment...">
                </div>
                @endauth

                <div class="comments-single py-4"></div>
            </div> --}}
        </div>
    </div>
