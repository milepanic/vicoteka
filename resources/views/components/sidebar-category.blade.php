@if(auth()->user() && $category->users->where('id', auth()->id())->first() && 
    $category->users->where('id', auth()->id())->first()->pivot->moderator === 1) 
<a class="mb-4 w-full block text-center bg-orange-dark hover:bg-orange text-grey-lighter text-sm font-bold uppercase tracking-wide py-2 rounded shadow" href="{{ url('kategorija/' . $category->slug . '/moderator?prikaz=sve-objave') }}">Moderator</a>
@endif
<div class="bg-white rounded shadow mb-4">
    <img class="rounded shadow" src="{{ asset($category->image) }}" alt="naslovna slika kategorije {{ $category->name }}">
    <div class="px-4 py-2">
        <div class="flex justify-between">
            <div>
                <a class="hover:underline" href="{{ url('kategorija/' . $category->slug) }}">
                    <h2 class="text-lg text-grey-black">{{ $category->name }}</h2>
                </a>
                <span class="text-sm text-grey-darker">Pratioci: 
                    <span id="subscribers-count">{{ $category->subscribers }}</span>
                </span>
            </div>
            {{-- subscribe-btn jquery selector --}}
            @if(auth()->user() && $category->users->where('id', auth()->id())->first()
                && $category->users->where('id', auth()->id())->first()->pivot->subscribed === 1)
            <button class="subscribe-btn subscribed text-sm my-2 py-2 px-4 font-bold rounded shadow" data-id="{{ $category->id }}" data-action="0">Pratiš</button>
            @else
            <button class="subscribe-btn subscribe text-sm my-2 py-2 px-4 font-bold rounded shadow" data-id="{{ $category->id }}" data-action="1">Zaprati</button>
            @endif
        </div>
    </div>
</div>
{{-- <div class="my-4">
    <div class="relative">
        <input class="w-full px-4 py-3 rounded shadow" type="text" name="category-search" placeholder="Uskoro...">
        <div class="absolute pin-r pin-y flex items-center px-3 text-grey hover:text-grey-darker cursor-pointer text-sm">
            <i class="fas fa-search"></i>
        </div>
    </div>
</div> --}}
<button class="w-full bg-blue hover:bg-blue-dark text-grey-lighter text-sm font-bold uppercase tracking-wide py-3 rounded shadow mb-4" id="open-submit-modal"><i class="fas fa-plus"></i> Postavi</button>

{{-- <div class="bg-white rounded shadow px-4 py-3 mb-4">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">Statistika</h4>
    <p class="text-grey-darker">
        ukupan broj postova <br> kad je napravljena
    </p>
</div> --}}
<div class="bg-white rounded shadow px-4 py-3 mb-4">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-3 px-4">Opis</h4>
    <p class="font-sans text-xs text-grey-darkest">{{ $category->description }}</p>
</div>
<div class="bg-white rounded shadow px-4 py-3">
    <h4 class="uppercase tracking-wide text-center text-grey-darkest mb-6 px-4">Moderatori</h4>
    <ul class="list-reset px-4">

        @foreach($category->users as $user)

        <li class="mb-3 @if(!$loop->last) border-b border-grey-lighter pb-2 @endif">
            <div class="flex items-center relative">
                <a href="{{ url('profil/' . $user->slug) }}">
                    <img class="w-12 h-12 rounded-full" src="{{ asset($user->image_sm) }}" alt="{{ $user->username }}">
                </a>
                <div class="ml-3 flex flex-col">
                    <a class="text-grey-darkest font-medium -mt-3 mb-1" 
                        href="{{ url('profil/' . $user->slug) }}">{{ $user->username }}</a>
                    <div>
                        {{-- <button class="follow-btn-sidebar py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Zaprati</button> --}}
                        @if(auth()->user() && $user->id !== auth()->id())
                            @if(auth()->user()->following->contains($user->id))
                                {{-- follow btn jquery selector --}}
                                <button class="follow-btn-category py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Pratite</button>
                            @else
                                <button class="follow-btn-category py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="{{ $user->id }}">Zaprati</button>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </li>

        @endforeach

    </ul>
</div>
<div class="my-4">
    @include('components/footer')
</div>
