@extends('admin.master')
@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@endsection
@section('content')

    <div class="row">
        <form class="col-md-4" action="{{ url('admin/submit-stack-posts') }}" method="POST">
            @csrf
            <input type="hidden" name="type" value="text">
            <div class="form-group">
                <select class="form-control" name="category">
                    @foreach($categories as $category)

                        <option value="{{ $category->id }}">{{ $category->name }}</option>

                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="user">
                    @foreach($users as $user)

                        <option value="{{ $user->id }}">{{ $user->username }}</option>

                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <textarea class="form-control" name="body" cols="30" rows="10"></textarea>
            </div>
            <div class="form-check">
                <label for="nsfw1" class="form-check-label">18+</label>
                <input id="nsfw1" class="form-check-input" type="checkbox" name="nsfw">
            </div>
            <button type="submit" class="btn btn-primary">Submit text</button>
        </form>
        <form class="col-md-4" action="{{ url('admin/submit-stack-posts') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="type" value="image">
            <div class="form-group">
                <select class="form-control" name="category">
                    @foreach($categories as $category)

                        <option value="{{ $category->id }}">{{ $category->name }}</option>

                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="user">
                    @foreach($users as $user)

                        <option value="{{ $user->id }}">{{ $user->username }}</option>

                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="file" name="image" class="form-control">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="body" cols="30" rows="10"></textarea>
            </div>
            <div class="form-check">
                <label for="nsfw1" class="form-check-label">18+</label>
                <input id="nsfw1" class="form-check-input" type="checkbox" name="nsfw">
            </div>
            <button type="submit" class="btn btn-primary">Submit image</button>
        </form>
        <form class="col-md-4" action="{{ url('admin/submit-stack-posts') }}" method="POST">
            @csrf
            <input type="hidden" name="type" value="video">
            <div class="form-group">
                <select class="form-control" name="category">
                    @foreach($categories as $category)

                        <option value="{{ $category->id }}">{{ $category->name }}</option>

                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="user">
                    @foreach($users as $user)

                        <option value="{{ $user->id }}">{{ $user->username }}</option>

                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="video" placeholder="Video embed URL">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="body" cols="30" rows="10"></textarea>
            </div>
            <div class="form-check">
                <label for="nsfw1" class="form-check-label">18+</label>
                <input id="nsfw1" class="form-check-input" type="checkbox" name="nsfw">
            </div>
            <button type="submit" class="btn btn-primary">Submit video</button>
        </form>
    </div>

@endsection
