@extends('admin.master')

@section('content')

	<div class="row">
	    <div class="col-lg-12">
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table id="dataTable" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Body</th>
                                <th>Reportable_type</th>
                                <th>Reportable_id</th>
                                <th>User ID</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($reports as $report)
                            <tr>				
                				<td>{{ $report->id }}</td>
                				<td>{{ $report->body }}</td>
                                <td>{{ $report->reportable_type }}</td>
                				<td>{{ $report->reportable_id }}</td>
                				<td>{{ $report->user_id }}</td>
                				<td>{{ $report->created_at }}</td>
                			</tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
	    </div>
	</div>

@endsection