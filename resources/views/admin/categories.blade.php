@extends('admin.master')

@section('content')

    <h3>Recommended categories</h3>
    <table class="table table-striped table-bordered table-hover">
        <tr>
            <th>Image</th>
            <th>Number</th>
            <th>Name</th>
            <th>Description</th>
            <th>NSFW</th>
            <th>Cover Box</th>
            <th>Pictures</th>
            <th>Videos</th>
            <th>Created at</th>
        </tr>
        @foreach($recommended as $category)
        <tr>
            <td><img width="80" height="80" src="{{ asset($category->image) }}"></td>
            <td> {{ $loop->iteration }} </td>
            <td> {{ $category->name }} </td>
            <td> {{ $category->description }}</td>
            <td> {{ $category->nsfw }} </td>
            <td> {{ $category->cover_box }} </td>
            <td> {{ $category->pictures }} </td>
            <td> {{ $category->videos }} </td>
            <td> 
                {{ \Carbon\Carbon::parse($category->updated_at)->diffForHumans() }}
            </td>
        </tr>
        @endforeach
    </table>

    <h3>All categories</h3>
	<div class="row">
	    <div class="col-lg-12">
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    {{-- <table class="table table-striped table-bordered table-hover" id="dataTable"> --}}
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>NSFW</th>
                                <th>Cover Box</th>
                                <th>Pictures</th>
                                <th>Videos</th>
                                <th>Created at</th>
                                <th>Approved</th>
                                <th>Izabrano</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($categories as $category)

                            <tr>
                                <td> {{ $category->name }} </td>
                                <td> {{ $category->nsfw }} </td>
                                <td> {{ $category->cover_box }} </td>
                                <td> {{ $category->pictures }} </td>
                                <td> {{ $category->videos }} </td>
                                <td> 
                                	{{ \Carbon\Carbon::parse($category->updated_at)->diffForHumans() }}
                                </td>
                                <td> 
                                	@if($category->approved === 0)
                                		Waiting
                                	@elseif($category->approved === 1)
                                		Rejected
                                	@else
                                		Approved
									@endif
                                </td>
                                <td>
                                    <input class="toggleRecommended" name="recommended" type="checkbox" data-id="{{ $category->id }}" @if($category->recommended === 1) checked="true" @endif>
                                </td>
                                <td>
                                	<a href="categories/approve/{{ $category->id }}">
                                		<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                	</a>&nbsp;
                                	<a class="text-danger" href="categories/reject/{{ $category->id }}">
                                		<i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                	</a>&nbsp;
                                	<a class="text-muted" href="categories/delete/{{ $category->id }}">
                                		<i class="fa fa-trash" aria-hidden="true"></i>
                                	</a>
                                </td>
                            </tr>

						@endforeach

                        </tbody>
                    </table>
                </div>
            </div>
	    </div>
	</div>

@endsection

@section('external-js')
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.toggleRecommended').click(function (e) {

            id = $(this).data('id');
            recommended = 0;

            if ($(this).is(":checked")) 
                recommended = 1;

            var data = {
                'id': id,
                'recommended': recommended
            };
            
            $.post('/admin/categories/recommended', data, function (data) {
                console.log('done. make append');
            });
        });
    });
</script>
@endsection
