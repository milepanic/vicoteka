@extends('layouts/master')
@section('info')
    @if($post->trashed())
        <title>[obrisano] | Distopija</title>
        <meta name="robots" content="noindex">
    @else
        <title>{{ Illuminate\Support\Str::limit($post->body, 35) ?? 'Objava' }} - {{ $post->category->name }} | Distopija</title>
        
        <meta name="description" content="{{ $post->created_at->format('d/m/Y H:i:s') }} - objavio {{ $post->user->username }}, u kategoriji {{ $post->category->name }}">
        <link rel="canonical" href="{{ url('objava/' . $post->id) }}">

        <meta property="og:type" content="article" />
        <meta property="og:url" content="{{ Request::url() }}" />
        @if($post->body)
            <meta property="og:title" content="{{ Illuminate\Support\Str::limit($post->body, 100) }}" />
        @else
            <meta property="og:title" content="Nova objava korisnika {{ $post->user->username }}" />
        @endif
        <meta property="og:description" content="Objavio {{ $post->user->username }}, u kategoriji {{ $post->category->name }} - {{ $post->created_at->format('d/m/Y H:i:s') }}" />
        {{-- @if(! is_null($post->image))
            <meta property="og:image" content="{{ asset($post->image) }}" />
        @elseif(! is_null($post->video))
            <meta property="og:image" content="https://img.youtube.com/vi/{{ substr($post->video, 30) }}/mqdefault.jpg" />
        @else --}}
        <meta property="og:image" content="{{ asset('storage/images/share-posts-fb/' . $post->id . '.jpg') }}" />
        @if(is_null($post->video) && is_null($post->image))
            <meta property="og:image:width" content="1200">
            <meta property="og:image:height" content="700">
        @endif
        <meta property="og:image:type" content="image/jpg" />
        <meta name="author" content="{{ $post->user->username }}" />
        <meta property="fb:app_id" content="1900990443537383">
        <meta property="og:locale" content="sr" />

        <meta name="twitter:description" content="Objavio {{ $post->user->username }}, u kategoriji {{ $post->category->name }} - {{ $post->created_at->format('d/m/Y H:i:s') }}" />
        <meta name="twitter:url" content="{{ Request::url() }}" />
        <meta name="twitter:image" content="{{ asset('storage/images/share-posts-fb/' . $post->id . '.jpg') }}" />
    @endif
@endsection
@section('content')
@include('components/sidebar-category-responsive')
@include('components/header')
<div class="modal absolute z-50" id="modal-submit" hidden>
    @include('components/modal-submit')
</div>
<div class="container mx-auto mb-8">
    <div class="flex xl:justify-center">
    	<div class="mx-auto xl:mx-0">
	        <div class="mx-auto xl:mx-0 xl:w-160 bg-white rounded shadow">
	            @include('components/post')
	        </div>
		</div>
        <div class="ml-8 w-full hidden xl:block xl:w-1/4">
            @include('components/sidebar-category')
        </div>

    </div>
</div>

@endsection
