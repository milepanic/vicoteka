@extends('email.template')

@section('content')
    <p>
        Zdravo <strong>{{ $user->username }}</strong>! 
        <br><br> 
        Dobrodošli na sajt <a href="{{ url('/') }}">{{ config('app.name') }}</a>,
        <br>hvala Vam što ste nam se pridružili. 
        <br><br>
        <a href="{{ url('otkrijte-kategorije') }}">Pronađite kategorije</a> i <a href="{{ url('otkrijte-korisnike') }}">zapratite korisnike</a>.
        <br><br>
        Svako dobro, <br>
        Vaša Distopija
    </p>
@endsection
