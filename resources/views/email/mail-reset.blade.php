@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/logo-full.png') }}" alt="Distopija logo">
        @endcomponent
    @endslot

    {{-- Body --}}
    <p>
        Da li ste zatražili resetovanje lozinke? Ako jeste, pritisnite dugme da resetujete. 
        <br><br>
        <div>
        <a href="{{ url('password/reset/' . $token) }}" target="_blank" class="button button-blue" style="margin: 0 auto; text-align: center;">Resetujte lozinku</a>
        </div>
        <br><br>
        Ako niste tražili, ignorišite ovu poruku. 
        <br><hr><br>
        <small>
            Ako imate problema sa kliktanjem dugmeta "Resetujte lozinku", kopirajte ovaj URL u vaš web browser: 
            <br>
            <a href="{{ url('password/reset/' . $token) }}">{{ url('password/reset/' . $token) }}</a> 
        </small>
        <br>
    </p>

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. Sva prava zadržana.
        @endcomponent
    @endslot
@endcomponent
