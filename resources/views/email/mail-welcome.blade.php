@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('images/logo-full.png') }}" alt="Distopija logo">
        @endcomponent
    @endslot

    {{-- Body --}}
    <p>
        Zdravo <strong>{{ $user->username }}</strong>! 
        <br><br> 
        Dobrodošli na sajt <a href="{{ url('/') }}">{{ config('app.name') }}</a>,
        <br>hvala Vam što ste nam se pridružili. 
        <br><br>
        <a href="{{ url('otkrijte-kategorije') }}">Pronađite kategorije</a> i <a href="{{ url('otkrijte-korisnike') }}">zapratite korisnike</a>.
        <br><br>
        Svako dobro, <br>
        Vaša Distopija
    </p>

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. Sva prava zadržana.
        @endcomponent
    @endslot
@endcomponent
