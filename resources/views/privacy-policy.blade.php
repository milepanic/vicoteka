@extends('layouts.master-clean')
@section('info')
    <title>Polisa privatnosti | Distopija</title>

    <meta name="robots" content="noindex">
@endsection
@section('content')
@include('components/header-clean')

<div class="container mx-auto bg-white rounded shadow py-4 px-8 font-sans">
    <strong class="text-grey-darkest uppercase tracking-wide">Privacy Policy</strong>
    <div>
        <p>This Privacy Policy governs the manner in which Distopija collects, uses, maintains and discloses information collected from users (each, a "User") of the http://www.distopija.com website ("Site").</p>

        <strong>Personal identification information</strong>
        <p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, fill out a form, respond to a survey, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, email address. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>

        <strong>Non-personal identification information</strong>
        <p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>

        <strong>Web browser cookies</strong>
        <p>Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>

        <strong>How we use collected information</strong>
        <p>Distopija may collect and use Users personal information for the following purposes:</p>
        <ul>
          <li>
            <i>To run and operate our Site</i><br/>
            We may need your information display content on the Site correctly.
          </li>
          <li>
            <i>To personalize user experience</i><br/>
            We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.
          </li>
          <li>
            <i>To improve our Site</i><br/>
            We may use feedback you provide to improve our products and services.
          </li>
          <li>
            <i>To send periodic emails</i><br/>
            We may use the email address to send them information and updates pertaining to their order. 
          </li>
        </ul>

        <strong>How we protect your information</strong>
        <p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>

        <strong>Sharing your personal information</strong>
        <p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above. We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>

        <strong>Electronic newsletters</strong>
        <p>If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>

        <strong>Third party websites</strong>
        <p>Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website's own terms and policies.</p>

        <strong>Advertising</strong>
        <p>Ads appearing on our site may be delivered to Users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile non personal identification information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This privacy policy does not cover the use of cookies by any advertisers. You may learn more about opting out of major ad networks at <a href="https://websitebuilders.com/tools/advertiser-opt-out/">https://websitebuilders.com/tools/advertiser-opt-out/</a>.</p>

        <strong>Google Adsense</strong>
        <p>Some of the ads may be served by Google. Google's use of the DART cookie enables it to serve ads to Users based on their visit to our Site and other sites on the Internet. DART uses "non personally identifiable information" and does NOT track personal information about you, such as your name, email address, physical address, etc. You may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at <a href="http://www.google.com/privacy_ads.html">http://www.google.com/privacy_ads.html</a></p>

        <strong>Compliance with children's online privacy protection act</strong>
        <p>Protecting the privacy of the very young is especially important. For that reason, we never collect or maintain information at our Site from those we actually know are under 13, and no part of our website is structured to attract anyone under 13.</p>

        <strong>Changes to this privacy policy</strong>
        <p>Distopija has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

        <strong>Your acceptance of these terms</strong>
        <p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes. This privacy policy was built <a href="https://privacypolicies.com/" target="_blank">using the generator at PrivacyPolicies.com</a>.</p>

        <strong>Contacting us</strong>
        <p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us.</p>

        <p>This document was last updated on May 18, 2018</p>
    </div>
</div>

@endsection
