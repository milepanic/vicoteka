<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#3490DC">
    
    @yield('info')
    
    <link rel="icon" href="{{ url('favicon.ico') }}" type="image/x-icon">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Cabin%7COpen+Sans%7COleo+Script+Swash+Caps" rel="stylesheet">
    <script defer src="{{ asset('js/fontawesome.js') }}"></script>
</head>
<body class="bg-grey-lightest leading-normal font-website">

    @yield('content')

    {{-- <script src="{{ asset('js/jquery.js') }}"></script> --}}
    {{-- <script async src="{{ asset('js/jquery-validation-1-17-0.js') }}"></script> --}}
{{-- 
    <script>
            var AuthUser = {{ auth()->check() ? 'true' : 'false' }};
    </script> --}}
    {{-- <script src="{{ asset('js/main.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/index.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/master.js') }}"></script> --}}
    {{-- <script async src="{{ asset('js/jquery-jscroll.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/validation.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/create-category.js') }}"></script> --}}
    {{-- <link href="{{ asset('assets/cropper/cropper.min.css') }}" rel="stylesheet"> --}}
    {{-- <script async src="{{ asset('assets/cropper/cropper.min.js') }}"></script> --}}
    
    {{-- @yield('external-js') --}}

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123209944-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-123209944-1');
    </script>

</body>
</html>
