<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#3490DC">

    @yield('info')

    <link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">

    {{-- <link rel="stylesheet" href="text/css" href="{{ asset('css/fonts.css') }}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Cabin%7COpen+Sans%7COleo+Script+Swash+Caps" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <script defer src="{{ asset('js/fontawesome.js') }}"></script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-7821993248906347",
        enable_page_level_ads: true
      });
    </script>
</head>
<body class="bg-grey-lightest leading-normal font-website">

    <div id="notification"></div>

    @guest
        <div class="modal absolute z-50" id="modal-login" hidden>
            @include('components/modal-login')
        </div>
        <div class="modal absolute z-50" id="modal-register" hidden>
            @include('components/modal-register')
        </div>
    @endguest

    @auth
        <div class="modal absolute z-50" id="modal-create" hidden>
            @include('components/modal-create-category')
        </div>
        <div class="modal absolute z-50" id="modal-submit-header" hidden>
            @include('components/modal-submit-header')
        </div>
    @endauth

    <div class="modal absolute z-50" id="modal-post" hidden>
        @include('components/modal-post')
    </div>
    <div class="modal absolute z-50" id="modal-feedback" hidden>
        @include('components/modal-feedback')
    </div>
    <div class="modal absolute z-50" id="modal-report" hidden>
        @include('components/modal-report')
    </div>
    <div class="modal absolute z-50" id="modal-contact" hidden>
        @include('components/footer/modal-contact')
    </div>
    <div class="modal absolute z-50" id="modal-autors" hidden>
        @include('components/footer/modal-autors')
    </div>

    @yield('content')

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script async src="{{ asset('js/jquery-validation-1-17-0.js') }}"></script>

    <script>
            var AuthUser = {{ auth()->check() ? 'true' : 'false' }};
            @auth
                var AuthIsAdmin = {{ auth()->user()->admin ? 'true' : 'false' }};
                var AuthLastPosted = new Date({{ strtotime(auth()->user()->last_posted_at) * 1000 }});
                var AuthLastCreated = new Date({{ strtotime(auth()->user()->last_created_category_at) * 1000 }});
                var AuthLastCommented = new Date({{ strtotime(auth()->user()->last_commented_at) * 1000 }});
            @endauth
    </script>
    <script src="{{ asset('js/main.min.js') }}"></script>
    <script src="{{ asset('js/jquery-jscroll.min.js') }}"></script>
    <link href="{{ asset('assets/cropper/cropper.min.css') }}" rel="stylesheet">
    <script async src="{{ asset('assets/cropper/cropper.min.js') }}"></script>
    <script src="{{ asset('js/lazysizes.min.js') }}" async=""></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>

    @yield('external-js')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123209944-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-123209944-1');
    </script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b9698ec404bea07"></script>
</body>
</html>
