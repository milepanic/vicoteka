<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::post('post', 'PostController@store')->middleware('can:store,App\Post');
Route::get('best-of/{param}', 'PostController@bestOf');
Route::get('najbolje/{param}', 'PostController@showBestOf');
Route::post('post/favorite/{id}', 'PostFavoritesController@store');
Route::post('post/vote', 'PostVotesController@store');
Route::get('objava/{id}', 'PostController@show');
Route::get('post-share', 'PostController@share');
Route::delete('post/{id}', 'PostController@destroy');
Route::patch('post/restore/{id}', 'PostController@restore');
Route::patch('post/{id}', 'PostController@update')->middleware('can:update,App\Post');
// Route::get('najbolje')

Route::get('profil/{slug}', 'UserController@show');
Route::post('profile/edit', 'UserController@update');
Route::post('profil/follow/{id}', 'UserController@follow');
Route::get('new-follow', 'UserController@getFollow');

Route::get('otkrijte-kategorije/{filter?}', 'CategoryController@index');
Route::get('otkrijte-korisnike', 'UserController@index')->middleware('auth');

Route::get('blokirane-kategorije', 'CategoryController@blockedCategories')->middleware('auth');
Route::get('moje-kategorije', 'CategoryController@ownedCategories')->middleware('auth');

Route::post('kategorija/subscribe', 'CategoryController@subscribe');
Route::get('kategorija/{slug}/moderator', 'CategoryController@edit')->middleware('moderator');
Route::get('kategorija/{slug}/{filter?}', 'CategoryController@show')->name('category.show');
Route::post('kategorija', 'CategoryController@store')->middleware('can:store,App\Category');
Route::post('kategorija/{id}', 'CategoryController@update')->middleware('can:update,App\Category');
Route::post('category/block/{id}', 'CategoryController@block');
Route::post('category/unblock/{id}', 'CategoryController@unblock');

Route::get('comments/{id}', 'CommentController@index');
Route::post('comment', 'CommentController@store')->middleware('can:store,App\Comment');
Route::post('comment/vote', 'CommentController@vote');

Route::post('search', 'SearchController@ajax');
Route::post('search/categories', 'SearchController@categories');

Route::post('validation/username', 'ValidationController@username');
Route::post('validation/email', 'ValidationController@email');
Route::post('validation/category-name', 'ValidationController@categoryName');
Route::post('validation/image', 'ValidationController@image');
Route::post('validation/post', 'ValidationController@post');
Route::post('validation/edit-username', 'ValidationController@editUsername');

Route::view('polisa-privatnosti', 'privacy-policy');

Route::post('feedback', 'FeedbackController@store');
Route::post('report', 'ReportController@store');

Route::post('notifications/all-read', 'NotificationsController@update');

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
	Route::get('/', 'AdminController@dashboard')->name('Dashboard');

	Route::get('users', 'AdminController@users')->name('Users');
	Route::post('users/ban/{id}', 'UserController@banUser');
	Route::get('users/unban/{id}', 'UserController@unbanUser');

	Route::get('posts', 'AdminController@posts')->name('Posts');
	Route::get('posts/delete/{id}', 'PostController@delete');

	Route::get('stack-posts', 'AdminController@stackPosts')->name('Stack Posts');
	Route::post('submit-stack-posts', 'AdminController@submitStackPosts');

	Route::get('categories', 'AdminController@categories')->name('Categories');
	Route::post('categories/recommended', 'CategoryController@toggleRecommended');
	Route::get('categories/approve/{id}', 'CategoryController@approve');
	Route::get('categories/reject/{id}', 'CategoryController@reject');
	Route::get('categories/delete/{id}', 'CategoryController@delete');

	Route::get('medals', 'AdminController@medals')->name('Medals');

	Route::get('reports', 'AdminController@reports')->name('Reports');
});

Route::get('//{filter?}', 'PostController@index')
		->where('filter', 'popularno|novo|top|kategorije|korisnici');
