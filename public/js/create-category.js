$(document).ready(function () {

	// Cropper
    var display_img = $("#display-cat-img");
    var upload_img = $("#upload-cat-img");

    // Display image before upload
    $("input:file").change(function () {
        if ($(this).val() !== '') 
        {
            display_img.cropper('destroy');

            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (e) {

                display_img.width("100%");
                display_img.attr('src', this.result);

                // Cropper options
                var options = {
                    aspectRatio: 1 / 1,
                    minContainerWidth: 350,
                    minContainerHeight: 350,
                    minCropBoxWidth: 100,
                    minCropBoxHeight: 100,
                    minCanvasWidth: 300,
                    minCanvasHeight: 300,
                    cropBoxResizable: true,
                    viewMode: 2,
                    responsive: true,
                    strict: false,
                    preview: '.ac-preview'
                };

                display_img.cropper(options);
            }
        }
    });

    var timer;
    
	$('#btn-create-category').on('click', function (e) {
		e.preventDefault();

        if(!$('#form-create').valid()) {
            $(this).removeClass('bg-blue hover:bg-blue-dark').addClass('bg-red hover:bg-red-dark');

            return;
        } else {
            if($(this).data('disabled') === true) return;

            $(this).addClass('opacity-50 cursor-not-allowed');
            $(this).data('disabled', true);
        }

		parent 			= $(this).parent();
		name 			= parent.find('#cat-name').val();
		description 	= parent.find('#cat-description').val();

        nsfw = 0;
        pictures = 0;
        videos = 0;
        mods = 0;

        if (parent.find('#cat-nsfw').is(":checked")) nsfw = 1;
        if (parent.find('#cat-pictures').is(":checked")) pictures = 1;
        if (parent.find('#cat-videos').is(":checked")) videos = 1;
        if (parent.find('#cat-mods').is(":checked")) mods = 1;

    	display_img.cropper('getCroppedCanvas').toBlob(function (blob) {
            var formData = new FormData();

            formData.append('name', name);
            formData.append('description', description);
            formData.append('croppedImage', blob);
            formData.append('nsfw', nsfw);
            formData.append('pictures', pictures);
            formData.append('videos', videos);
            formData.append('mods', mods);

			$.ajax({
				type: 'POST',
				url: '/kategorija',
				data: formData,
				processData: false,
                contentType: false,
				success: function (data) {
					window.location.replace(
                        window.location.protocol + 
                        '//' + 
                        window.location.host + 
                        '/kategorija/' + 
                        data
                    );
				},
				error: function (err) {

				}
			});
        });
	});

});