$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // infinite scroll for posts
    $('.infinite-scroll').jscroll({
        autoTrigger: true,
        loadingHtml: '<div class="mx-auto w-24"><img class="w-24" src="' + window.location.protocol + '//' + window.location.host
            + '/images/loading.gif" alt="Učitava se..." /></div>',
        padding: 200,
        nextSelector: '.pagination a[rel=next]',
        contentSelector: 'div.infinite-scroll',
        debug: false,
        callback: function () {
            initMagnificPopup();
        }
    });

    function reinitJscroll() {
        $('.infinite-scroll').removeData('jscroll').jscroll.destroy();

        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<div class="mx-auto w-24"><img class="w-24" src="' + window.location.protocol + '//' + window.location.host
                + '/images/loading.gif" alt="Učitava se..." /></div>',
            padding: 200,
            nextSelector: '.pagination a[rel=next]',
            contentSelector: 'div.infinite-scroll',
            debug: false,
            callback: function () {

            }
        });
    }

    function notify(color, title, message) {
        $('#notification').append(
            $(
                '<div class="fixed pin-r pin-t mr-8 mt-4 mb-2 rounded shadow-md flex z-50">' +
                    '<div class="bg-' + color + '-dark px-1 rounded-l">' +
                    '</div>' +
                    '<div class="relative bg-' + color + ' px-4 py-2 rounded-r shadow">' +
                        '<span class="absolute pin-t pin-b pin-r my-2">' +
                            '<a class="text-xs text-white p-4" href="#" title="Close">' +
                                '<i class="close-notification fas fa-times"></i>' +
                            '</a>' +
                        '</span>' +
                        '<div class="mr-16 max-w-xs">' +
                            '<p class="font-bold text-white">' + title + '</p>' +
                            '<p class="text-sm text-white">' + message + '</p>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            )
            .hide()
            .fadeIn(200)
        )
        .delay(4500)
        .fadeOut(200);
    }

    $('#notification').delegate('.close-notification', 'click', function (e) {
        $('#notification > div').fadeOut();
    });

    // submits login on enter
    $('#login-email, #login-password').keypress(function (e) {
        if(e.which == 13)
            $('.login-btn').click();
    });

    // send login user info
    $('.login-btn').on('click', function (e) {
        e.preventDefault();

        if ($(this).data('disabled') === true) return;

        $(this).addClass('opacity-50 cursor-not-allowed');
        $(this).data('disabled', true);

        parent = $(this).parent();
        email = parent.find('#login-email').val();
        password = parent.find('#login-password').val();
        remember = parent.find('#login-remember').val();

        var data = {
            email: email,
            password: password,
            remember: remember
        };

        var elem = $(this);

        $.ajax({
            type: 'POST',
            url: '/login',
            data: data,
            success: function (data) {
                window.location.replace('/');
            },
            error: function (err) {
                $('#login-has-errors').empty().append(
                    '<span class="text-xs text-red">' +
                        '<strong>Neispravan email ili lozinka</strong>' +
                    '</span>'
                );

                elem.data('disabled', false);
                elem.removeClass('opacity-50 cursor-not-allowed');
            }
        });
    });

    // send register user info
    $('.register-btn').on('click', function (e) {
        e.preventDefault();

        if(!$('#form-register').valid()) {
            $(this).removeClass('bg-teal hover:bg-teal-dark').addClass('bg-red hover:bg-red-dark');

            return;
        } else {
            if($(this).data('disabled') === true) return;

            $(this).addClass('opacity-50 cursor-not-allowed');
            $(this).data('disabled', true);
        }

        parent = $(this).parent();
        username = parent.find('#register-username').val();
        email = parent.find('#register-email').val();
        password = parent.find('#register-password').val();
        password_confirmation = parent.find('#register-password_confirmation').val();
        description = parent.find('#register-description').val();

        var data = {
            username: username,
            email: email,
            password: password,
            password_confirmation: password_confirmation,
            description: description
        };

        $.ajax({
            type: 'POST',
            url: '/register',
            data: data,
            success: function (data) {
                window.location.replace('/');
            },
            error: function(err) {
            console.log(err);

            }
        });
    });

    // change submit modal tab
    $('.submit-tab').on('click', function (e) {
        e.preventDefault();

        $(this).parent().find('.submit-tab-active').removeClass('submit-tab-active').addClass('submit-tab-inactive');
        $(this).removeClass('submit-tab-inactive').addClass('submit-tab-active');

        type = $(this).data('type');
        parent = $(this).parents(':eq(1)');

        parent.find('.submit-tab-div').hide();

        switch(type) {
            case 'text':
                parent.find('#form-submit-text').show();
                parent.find('#submit-hidden').val('text');
                break;
            case 'image':
                parent.find('#form-submit-image').show();
                parent.find('#submit-hidden').val('image');
                break;
            case 'video':
                parent.find('#form-submit-video').show();
                parent.find('#submit-hidden').val('video');
                break;
        }

    });

    // change header submit modal tab
    $('.h-submit-tab').on('click', function (e) {
        e.preventDefault();

        $(this).parent().find('.h-submit-tab-active').removeClass('h-submit-tab-active').addClass('h-submit-tab-inactive');
        $(this).removeClass('h-submit-tab-inactive').addClass('h-submit-tab-active');

        type = $(this).data('type');
        parent = $(this).parents(':eq(1)');

        parent.find('.h-submit-tab-div').hide();

        switch(type) {
            case 'text':
                parent.find('#h-form-submit-text').show();
                parent.find('#h-submit-hidden').val('text');
                break;
            case 'image':
                parent.find('#h-form-submit-image').show();
                parent.find('#h-submit-hidden').val('image');
                break;
            case 'video':
                parent.find('#h-form-submit-video').show();
                parent.find('#h-submit-hidden').val('video');
                break;
        }

    });

    // submit post from category page
    $('#modal-submit-content').delegate('.submit-btn', 'click', function (e) {
        e.preventDefault();

        parent = $(this).parents(':eq(1)');
        action = parent.find('#submit-hidden').val();
        body = parent.find('#submit-body').val();
        category = $(this).data('name');

        if(action === 'text' && !$('#form-submit-text').valid() || action === 'image' && !$('#form-submit-image').valid()
            || action === 'video' && !$('#form-submit-video').valid()) {
            $(this).removeClass('bg-blue hover:bg-blue-dark').addClass('bg-red hover:bg-red-dark');

            return;
        } else {
            if($(this).data('disabled') === true) return;

            $(this).addClass('opacity-50 cursor-not-allowed');
            $(this).data('disabled', true);
        }

        nsfw = 0;

        var elem = $(this);
        video = null;

        if(action === 'video') {
            video = parent.find('#submit-video-url').val();
            body = parent.find('#submit-body-video').val();

            if (parent.find('#submit-video-nsfw').is(":checked")) nsfw = 1;

            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = video.match(regExp);

            if (match && match[7].length == 11)
                video = 'https://www.youtube.com/embed/' + match[7];
            else {
                alert('Neispravan Youtube URL');
                return;
            }
        }

        if(action === 'image') {
            image = parent.find('input[type=file]')[0].files[0];
            body = parent.find('#submit-body-image').val();

            if (parent.find('#submit-image-nsfw').is(":checked")) nsfw = 1;

            var data = new FormData();

            data.append('action', action);
            data.append('body', body);
            data.append('image', image);
            data.append('category', category);
            data.append('nsfw', nsfw);

            $.ajax({
                type: 'POST',
                url: '/post',
                data: data,
                processData: false,
                contentType: false,
                success: function (data) {
                    elem.parents(':eq(3)').hide();
                    modal_active = false;
                    notify('green', 'Uspeh!', 'Uspešno ste objavili novi post');
                },
                error: function () {
                    notify('red', 'Dogodila se greška!', 'Pokušajte ponovo');
                }
            });

            return;
        }

        if (parent.find('#submit-text-nsfw').is(":checked")) nsfw = 1;

        var data = {
            action: action,
            body: body,
            video: video,
            category: category,
            nsfw: nsfw
        };

        $.ajax({
            type: 'POST',
            url: '/post',
            data: data,
            success: function () {
                elem.parents(':eq(3)').hide();
                modal_active = false;
                notify('green', 'Uspeh!', 'Uspešno ste objavili novi post');
            },
            error: function(err) {
                notify('red', 'Dogodila se greška!', 'Pokušajte ponovo');
            }
        });
    });

    // submit post from navigation
    $('#h-modal-submit-content').delegate('.submit-btn', 'click', function (e) {
        e.preventDefault();

        parent = $(this).parents(':eq(1)');
        action = parent.find('#h-submit-hidden').val();
        body = parent.find('#h-submit-body').val();
        category = $('#search-categories').val();

        // if(action === 'text' && !$('#h-form-submit-text').valid() || action === 'image' && !$('#h-form-submit-image').valid()
        //  || action === 'video' && !$('#h-form-submit-video').valid()) {
  //           $(this).removeClass('bg-blue hover:bg-blue-dark').addClass('bg-red hover:bg-red-dark');

  //           return;
  //       } else {
  //           if($(this).data('disabled') === true) return;

  //           $(this).addClass('opacity-50 cursor-not-allowed');
  //           $(this).data('disabled', true);
  //       }

        nsfw = 0;

        var elem = $(this);
        video = null;

        if(action === 'video') {
            video = parent.find('#h-submit-video-url').val();
            body = parent.find('#h-submit-body-video').val();

            if (parent.find('#h-submit-video-nsfw').is(":checked")) nsfw = 1;

            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = video.match(regExp);

            if (match && match[7].length == 11)
                video = 'https://www.youtube.com/embed/' + match[7];
            else {
                alert('Neispravan Youtube URL');
                return;
            }
        }

        if(action === 'image') {
            image = parent.find('input[type=file]')[0].files[0];
            body = parent.find('#h-submit-body-image').val();

            if (parent.find('#h-submit-image-nsfw').is(":checked")) nsfw = 1;

            var data = new FormData();

            data.append('action', action);
            data.append('body', body);
            data.append('image', image);
            data.append('category', category);
            data.append('nsfw', nsfw);

            $.ajax({
                type: 'POST',
                url: '/post',
                data: data,
                processData: false,
                contentType: false,
                success: function (data) {
                    elem.parents(':eq(3)').hide();
                    modal_active = false;
                    notify('green', 'Uspeh!', 'Uspešno ste objavili novi post');
                },
                error: function () {
                    notify('red', 'Dogodila se greška!', 'Pokušajte ponovo');
                }
            });

            return;
        }

        if (parent.find('#h-submit-text-nsfw').is(":checked")) nsfw = 1;

        var data = {
            action: action,
            body: body,
            video: video,
            category: category,
            nsfw: nsfw
        };

        $.ajax({
            type: 'POST',
            url: '/post',
            data: data,
            success: function () {
                elem.parents(':eq(3)').hide();
                modal_active = false;
                notify('green', 'Uspeh!', 'Uspešno ste objavili novi post');
            },
            error: function(err) {
                notify('red', 'Dogodila se greška!', 'Pokušajte ponovo');
            }
        });
    });

    // changing main tabs
    // $("#filter-tabs a").click(function (e) {
    //     e.preventDefault();

    //     var type = $(this).text().toLowerCase();
    //     $(this).parent().find('a.nav-active').removeClass("nav-active");
    //     $(this).addClass('nav-active');

    //     uri = '/';
    //     category = false;

    //     if(window.location.href.indexOf('kategorija') > -1) {
    //         category = true;
    //         pathArray = '';
    //         pathArray = window.location.pathname.split('/');
    //         uri = '/' + pathArray[1] + '/' + pathArray[2] + '/';
    //         category_name = pathArray[2].replace(/(?:^|\s)\S/g, function(l) {
    //             return l.toUpperCase();
    //         });
    //         category_name = category_name.split('-').join(' ');
    //     }

    //     $.get(uri + type, function (data) {
    //         $('#posts').html(data);
    //         window.history.pushState(type, 'Title', uri + type);

    //         if (!category) {
    //             if (window.location.href.indexOf('popularno') > -1) {
    //                 document.title = 'Popularno | Distopija';
    //             } else if (window.location.href.indexOf('novo') > -1) {
    //                 document.title = 'Novo | Distopija';
    //             } else if (window.location.href.indexOf('top') > -1) {
    //                 document.title = 'Top | Distopija';
    //             } else if (window.location.href.indexOf('kategorije') > -1) {
    //                 document.title = 'Kategorije | Distopija';
    //             } else if (window.location.href.indexOf('korisnici') > -1) {
    //                 document.title = 'Korisnici | Distopija';
    //             }
    //         } else {
    //             if (window.location.href.indexOf('popularno') > -1) {
    //                 document.title = 'Popularno - ' + category_name + ' | Distopija';
    //             } else if (window.location.href.indexOf('novo') > -1) {
    //                 document.title = 'Novo - ' + category_name + ' | Distopija';
    //             } else if (window.location.href.indexOf('top') > -1) {
    //                 document.title = 'Top - ' + category_name + ' | Distopija';
    //             }
    //         }
    //         // Reinitializes JScroll so it loads correct data
    //         reinitJscroll();
    //     });
    // });

    $(".homepage-sort-nav a, .category-sort-nav a, .discover-categories-sort-nav a").click(function (e) {
        e.preventDefault();

        var type = $.trim($(this).text().toLowerCase());
        var icon = $(this).find('.sort-icon').clone();
        icon.addClass('text-blue');

        $(this).parents(':eq(1)').find('.text-blue').removeClass("text-blue font-bold").addClass('text-grey-darker');
        $(this).addClass('text-blue font-bold');

        $('#sort-current-page').text(type);
        $('#sort-current-icon').html(icon);

        uri = '/';
        category = false;

        if(window.location.href.indexOf('kategorija') > -1) {
            category = true;
            pathArray = '';
            pathArray = window.location.pathname.split('/');
            uri = '/' + pathArray[1] + '/' + pathArray[2] + '/';
            category_name = pathArray[2].replace(/(?:^|\s)\S/g, function(l) {
                return l.toUpperCase();
            });
            category_name = category_name.split('-').join(' ');
        }

        if (window.location.href.indexOf('otkrijte-kategorije') > -1) {
           switch (type) {
                case 'nove kategorije':
                    type = 'nove';
                    break;

                case 'moje kategorije':
                    type = 'moje';
                    break;

                case 'nepraćene kategorije':
                    type = 'nepracene';
                    break;

                default:
                    type = 'popularne';
                    break;
           }

           $.ajax({
                url: '/otkrijte-kategorije/' + type,
                success: function (data) {
                    $('#all-categories').html(data);

                    window.history.pushState(type, 'Title', '/otkrijte-kategorije/' + type);
                }
           });

           reinitJscroll();
           initMagnificPopup();
           return;
        }

        $.get(uri + type, function (data) {
            $('#posts').html(data);
            window.history.pushState(type, 'Title', uri + type);

            if (!category) {
                if (window.location.href.indexOf('popularno') > -1) {
                    document.title = 'Popularno | Distopija';
                } else if (window.location.href.indexOf('novo') > -1) {
                    document.title = 'Novo | Distopija';
                } else if (window.location.href.indexOf('top') > -1) {
                    document.title = 'Top | Distopija';
                } else if (window.location.href.indexOf('kategorije') > -1) {
                    document.title = 'Kategorije | Distopija';
                } else if (window.location.href.indexOf('korisnici') > -1) {
                    document.title = 'Korisnici | Distopija';
                }
            } else {
                if (window.location.href.indexOf('popularno') > -1) {
                    document.title = 'Popularno - ' + category_name + ' | Distopija';
                } else if (window.location.href.indexOf('novo') > -1) {
                    document.title = 'Novo - ' + category_name + ' | Distopija';
                } else if (window.location.href.indexOf('top') > -1) {
                    document.title = 'Top - ' + category_name + ' | Distopija';
                }
            }
            // Reinitializes JScroll so it loads correct data
            reinitJscroll();
            initMagnificPopup();
        });
    });

    // $('.discover-categories-sort-nav a').click(function () {

    // });

    // subscribe to category from category page
    $('.subscribe-btn').on('click', function (e) {
        e.preventDefault();

        if(!AuthUser) {
            $(this).closest('body').find('#modal-login').show();
            return;
        }

        if ($(this).hasClass('subscribe')) {
            $(this).removeClass('subscribe').addClass('subscribed');
            $(this).text('Pratiš');
            count = $(this).parent().find('#subscribers-count');
            nmb = count.text();
            newNmb = parseInt(nmb) + 1;
            count.text(newNmb);
        } else {
            $(this).removeClass('subscribed').addClass('subscribe');
            $(this).text('Zaprati');
            count = $(this).parent().find('#subscribers-count');
            nmb = count.text();
            newNmb = parseInt(nmb) - 1;
            count.text(newNmb);
        }

        var data = {
            id: $(this).data('id'),
            column: 'subscribed',
            action: $(this).data('action'), // subscribe/unsubscribe
        };

        // change data action to 0 or 1
        x = $(this).data('action');
        x = 1 - x;
        $(this).data('action', x);

        $.ajax({
            url: '/kategorija/subscribe',
            type: 'POST',
            data: data,
            success: function () {
                if (data.action == 1)
                    notify('green', 'Kategorija zapraćena!', 'Uspešno ste se pretplatili na kategoriju');
                else
                    notify('orange', 'Kategorija otpraćena!', 'Uspešno ste otpratili kategoriju');
            },
        });
    });

    // Follow User from profile page
    $('.follow-btn').on('click', function (e) {
        e.preventDefault();

        if(!AuthUser) {
            $(this).closest('body').find('#modal-login').show();
            return;
        }

        action = 1;

        if ($(this).hasClass('follow')) {
            $(this).removeClass('follow').addClass('following');
            $(this).text('Pratite');
        } else {
            $(this).removeClass('following').addClass('follow');
            $(this).text('Zaprati');
            action = 0;
        }

        id = $(this).parents(':eq(1)').data('id');

        $.ajax({
            url: '/profil/follow/' + id,
            type: 'POST',
            success: function () {
                if (action == 1)
                    notify('green', 'Korisnik zapraćen!', 'Uspešno ste se zapratili korisnika');
                else
                    notify('orange', 'Korisnik otpraćen!', 'Uspešno ste otpratili korisnika');
            },
        });
    });

    // Removes current suggested user from sidebar and appends new
    function getNewFollow(elem) {
        $.get('new-follow', function (data) {
            elem.closest('li').empty().append(
                '<div class="flex items-center relative">' +
                    '<a href="">' +
                        '<img class="w-12 h-12 rounded-full" src="' + data[0].image_sm + '" alt="' + data[0].username + '">' +
                    '</a>' +
                    '<div class="ml-3 flex flex-col">' +
                        '<a class="text-grey-darkest font-medium -mt-3 mb-1" ' +
                            'href="">' + data[0].username + '</a>' +
                        '<div>' +
                            '<button class="follow-btn-sidebar py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="' + data[0].id + '">Zaprati</button>' +
                        '</div>' +
                    '</div>' +
                    '<span class="absolute pin-t pin-b pin-r p-4">' +
                        '<a class="new-follow h-12 w-12 text-grey-light hover:text-grey-darkest" href="#" title="Nova preporuka">' +
                            '<i class="fas fa-times"></i>' +
                        '</a>' +
                    '</span>' +
                '</div>'
            );
        });
    }

    // Follow user from category sidebar
    $('.follow-btn-category').on('click', function (e) {
        e.preventDefault();

        var elem = $(this);
        id = elem.data('id');

        if ($(this).text() === 'Zaprati')
            $(this).text('Pratite');
        else
            $(this).text('Zaprati');

        $.post('/profil/follow/' + id);
    });

    $('.unfollow-btn').on('click', function (e) {
        e.preventDefault();

        var elem = $(this);
        id = elem.data('id');

        if (confirm('Da li želite da otpratite korisnika?'))
            $.post('/profil/follow/' + id);

        $(this).text('Zaprati');
    });

    // Follow user from recommended users
    $('#sidebar-users, #sidebar-users-responsive').delegate('.follow-btn-sidebar', 'click', function (e) {
        e.preventDefault();

        var elem = $(this);
        id = elem.data('id');

        $.post('/profil/follow/' + id);

        getNewFollow(elem);
    });

    // Remove current and get new suggested user to follow
    $('#sidebar-users').delegate('.new-follow', 'click', function(e) {
        e.preventDefault();

        getNewFollow($(this));
    });

    $('.follow-btn-discover').on('click', function (e) {
        e.preventDefault();

        id = $(this).data('id');

        $.post('/profil/follow/' + id);

        $(this).closest('.discover-user-box').fadeOut('slow');
    });

    // --- Edit profile start ---
    // Greska pri prikazivanju kropera, prikaze na napravi kategoriju kroperu
    // var display_img = $("#edit-profile-display-img");
    // var upload_img = $("#edit-profile-upload-img");

    // // Display image before upload
    // $("#edit-profile-upload-img").change(function () {
    //     if ($(this).val() !== '')
    //     {
    //         display_img.cropper('destroy');

    //         var reader = new FileReader();
    //         reader.readAsDataURL(this.files[0]);
    //         reader.onload = function (e) {

    //             display_img.width("100%");
    //             display_img.attr('src', this.result);

    //             // Cropper options
    //             var options = {
    //                 aspectRatio: 1 / 1,
    //                 minContainerWidth: 350,
    //                 minContainerHeight: 350,
    //                 minCropBoxWidth: 100,
    //                 minCropBoxHeight: 100,
    //                 minCanvasWidth: 300,
    //                 minCanvasHeight: 300,
    //                 cropBoxResizable: true,
    //                 viewMode: 2,
    //                 responsive: true,
    //                 strict: false,
    //                 preview: '.ac-preview'
    //             };

    //             display_img.cropper(options);
    //         }
    //     }
    // });

    // // edit profile
    // $('#btn-edit-profile').on('click', function (e) {
    //     e.preventDefault();

    //     if(!$('#form-edit-profile').valid()) {
    //         $(this).removeClass('bg-teal hover:bg-teal-dark').addClass('bg-red hover:bg-red-dark');

    //         return;
    //     } else {
    //         if($(this).data('disabled') === true) return;

    //         $(this).addClass('opacity-50 cursor-not-allowed');
    //         $(this).data('disabled', true);
    //     }

    //     nsfw = 0;
    //     if ($('#user-adult').is(":checked")) nsfw = 1;

    //     parent          = $(this).parent();
    //     id              = parent.data('id');
    //     username        = $('#edit-username').val();
    //     description     = $('#edit-description').val();
    //     facebook        = $('#edit-facebook').val();
    //     twitter         = $('#edit-twitter').val();
    //     instagram       = $('#edit-instagram').val();
    //     tumblr          = $('#edit-tumblr').val();

    //     if ($('#edit-profile-upload-img').val() !== '') {
    //         display_img.cropper('getCroppedCanvas').toBlob(function (blob) {
    //             var formData = new FormData();

    //             formData.append('username', username);
    //             formData.append('description', description);
    //             formData.append('croppedImage', blob);
    //             formData.append('facebook', facebook);
    //             formData.append('twitter', twitter);
    //             formData.append('instagram', instagram);
    //             formData.append('tumblr', tumblr);
    //             formData.append('nsfw', nsfw);

    //             $.ajax('/profile/edit', {
    //                 method: "POST",
    //                 data: formData,
    //                 processData: false,
    //                 contentType: false,
    //                 success: function (data) {
    //                     window.location.replace(data);
    //                 },
    //                 error: function () {
    //                     alert('Dogodila se greška...');
    //                 }
    //             });
    //         });
    //     } else {
    //         var data = {
    //             username: username,
    //             description: description,
    //             facebook: facebook,
    //             twitter: twitter,
    //             instagram: instagram,
    //             tumblr: tumblr,
    //             nsfw: nsfw,
    //         };

    //         var elem = $(this);

    //         $.ajax({
    //             type: 'POST',
    //             url: '/profile/edit',
    //             data: data,
    //             success: function (data) {
    //                 window.location.replace(data);
    //             },
    //             error: function () {
    //                 alert('Dogodila se greška');
    //             }
    //         });
    //     }
    // });
    // --- Edit profile end ---

    // Modals
    $('#responsive-open-submit-modal, #open-submit-modal').on('click', function (e) {
        e.preventDefault();

        date = new Date();
        date.setMinutes(date.getMinutes() - 10);

        if (! AuthIsAdmin && AuthLastPosted > date) {
            notify('red', 'Pokušajte kasnije', 'Da bismo izbegli spam, za novu objavu mora proći 10 minuta');
            return;
        }

        $('#modal-submit').fadeIn(200);
    });

    $('#main-header-open-submit-modal, #header-open-submit-modal').on('click', function (e) {
        e.preventDefault();

        date = new Date();
        date.setMinutes(date.getMinutes() - 10);

        if (! AuthIsAdmin && AuthLastPosted > date) {
            notify('red', 'Pokušajte kasnije', 'Da bismo izbegli spam, za novu objavu mora proći 10 minuta');
            return;
        }

        $('#modal-submit-header').fadeIn(200);
    });

    $('.open-login-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        previous_url = window.location.href;
        window.history.pushState('', 'Title', '/login');

        $(this).closest('body').find('#modal-login').fadeIn(200);
        // modal = $(this).closest('body').find('#modal-login');
    });

    $('.open-register-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        previous_url = window.location.href;
        window.history.pushState('', 'Title', '/register');

        $(this).closest('body').find('#modal-register').fadeIn(200);
    });

    $('.open-create-modal').on('click', function (e) {
        e.preventDefault();

        date = new Date();
        date.setDate(date.getDate() - 1);

        if (! AuthIsAdmin && AuthLastCreated > date) {
            notify('red', 'Pokušajte kasnije', 'Da bismo izbegli spam, za kreiranje nove kategorije mora proći 1 dan');
            return;
        }

        $(this).closest('body').find('#modal-create').fadeIn(200);
    });

    // koristiti ovo umjesto appendovanja u html, testirati radi li dobro
    var previous_url = null;

    $('#posts').delegate('.open-post-modal', 'click', function (e) {
        e.preventDefault();

        modal = $(this).closest('body').find('#modal-post');
        // modal.find('.previous-url').empty().append(window.location.href);
        previous_url = window.location.href;

        parent = $(this).closest('.post-box');
        id = $(this).data('id');
        window.history.pushState(id, 'Title', '/objava/' + id);

        user = parent.find('.post-user-info').clone();
        content = parent.find('.post-content').clone();
        buttons = parent.find('.post-buttons').clone();

        modal.find('.post-box').data('id', id);
        modal.find('.modal-post-user-info').empty().append(user);
        modal.find('#modal-post-content').empty().append(content);
        modal.find('#modal-post-buttons').empty().append(buttons);

        $(this).closest('body').find('#modal-post').fadeIn(200);
    });

    $('.open-edit-profile-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).closest('body').find('#modal-edit-profile').fadeIn(200);
    });

    $('.open-feedback-modal').on('click', function (e) {
        e.preventDefault();

        $(this).closest('body').find('#modal-feedback').fadeIn(200);
    });

    reportable_type = null;
    reportable_id = null;
    report_category_id = null;
    report_post_id = null;

    $('#posts, #modal-post, #post-single').delegate('.open-report-modal', 'click', function (e) {
        e.preventDefault();

        reportable_type = $(this).data('type');
        reportable_id = $(this).data('id');
        report_category_id = $(this).data('category');

        if (reportable_type === 'post') {
            $('#report-1').text('Nelegalni ili uznemirujući sadržaj');
            $('#report-2').text('Kršenje pravila');
            $('#report-3').text('Vređanje');
        } else if (reportable_type === 'user') {
            $('#report-1').text('Neprikladno ime');
            $('#report-2').text('Spam');
            $('#report-3').text('Vređanje');
        } else if (reportable_type === 'comment') {
            $('#report-1').text('Kršenje pravila');
            $('#report-2').text('Spam');
            $('#report-3').text('Vređanje');

            $('#btn-report').data('post', $(this).data('post'));
        }

        $('#btn-report').data('type', reportable_type);
        $('#btn-report').data('id', reportable_id);
        $('#btn-report').data('category', report_category_id);

        $('#modal-report').fadeIn(200);
    });

    $('.open-nsfw-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $('#modal-nsfw').fadeIn(200);
    });

    $('.open-contact-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).closest('body').find('#modal-contact').fadeIn(200);
    });

    $('.open-autors-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).closest('body').find('#modal-autors').fadeIn(200);
    });

    $('.open-privacy-policy-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).closest('body').find('#modal-privacy-policy').fadeIn(200);
    });

    modal_bestof_today_active = false;

    $('.open-bestOfToday-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        modal = $('#modal-bestOf-post');

        previous_url = window.location.href;

        $.get('/best-of/day', function (data) {
            console.log(data);
            modal.find('.modal-bestof-user-slug').attr('href', data.user.slug);
            modal.find('.modal-bestof-user-image').attr('src', data.user.image_sm);
            modal.find('.modal-bestof-user-username').empty().append(data.user.username);
            modal.find('.modal-bestof-created').empty().append(data.diffCreatedAt);
            modal.find('.modal-bestof-category-slug').attr('href', data.user.slug).empty().append(data.category.name);
            if (data.nsfw)
                modal.find('.modal-bestof-nsfw').show();
            else
                modal.find('.modal-bestof-nsfw').hide();
            modal.find('.modal-bestof-body').empty().append(data.body);
            modal.find('.modal-bestof-image').empty();
            modal.find('.modal-bestof-video').empty();
            if (data.image)
                modal.find('.modal-bestof-image').append('<img class="mb-4 w-full h-auto" src="' + data.image + '">');
            if (data.video)
                modal.find('.modal-bestof-video').append(
                    '<iframe class="modal-bestof-video" width="560" height="316" frameborder="0" src="' + data.video + '?autoplay=1&rel=0" allowfullscreen></iframe>'
                );
            modal.find('.modal-bestof-count-upvotes').empty().append(data.upvotes_count);
            modal.find('.modal-bestof-count-downvotes').empty().append(data.downvotes_count);
            modal.find('.modal-bestof-count-favorites').empty().append(data.favorites_count);
            modal.find('.modal-bestof-count-comments').empty().append(data.comments_count);

            modal.fadeIn(200);

            window.history.pushState(null, 'Najbolje dana', '/najbolje/dana');
        });

    });

    modal_bestof_week_active = false;

    $('.open-bestOfWeek-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        previous_url = window.location.href;

        modal = $('#modal-bestOf-post');

        $.get('/best-of/week', function (data) {
            console.log(data);
            modal.find('.modal-bestof-user-slug').attr('href', data.user.slug);
            modal.find('.modal-bestof-user-image').attr('src', data.user.image_sm);
            modal.find('.modal-bestof-user-username').empty().append(data.user.username);
            modal.find('.modal-bestof-created').empty().append(data.diffCreatedAt);
            modal.find('.modal-bestof-category-slug').attr('href', data.user.slug).empty().append(data.category.name);
            if (data.nsfw)
                modal.find('.modal-bestof-nsfw').show();
            else
                modal.find('.modal-bestof-nsfw').hide();
            modal.find('.modal-bestof-body').empty().append(data.body);
            modal.find('.modal-bestof-image').empty();
            modal.find('.modal-bestof-video').empty();
            if (data.image)
                modal.find('.modal-bestof-image').append('<img class="mb-4 w-full h-auto" src="' + data.image + '">');
            if (data.video)
                modal.find('.modal-bestof-video').append(
                    '<iframe class="modal-bestof-video" width="560" height="316" frameborder="0" src="' + data.video + '?autoplay=1&rel=0" allowfullscreen></iframe>'
                );
            modal.find('.modal-bestof-count-upvotes').empty().append(data.upvotes_count);
            modal.find('.modal-bestof-count-downvotes').empty().append(data.downvotes_count);
            modal.find('.modal-bestof-count-favorites').empty().append(data.favorites_count);
            modal.find('.modal-bestof-count-comments').empty().append(data.comments_count);

            modal.fadeIn(200);

            window.history.pushState(null, 'Najbolje sedmice', '/najbolje/sedmice');
        });
    });

    modal_bestof_month_active = false;

    $('.open-bestOfMonth-modal').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        previous_url = window.location.href;

        modal = $('#modal-bestOf-post');

        $.get('/best-of/month', function (data) {
            modal.find('.modal-bestof-user-slug').attr('href', data.user.slug);
            modal.find('.modal-bestof-user-image').attr('src', data.user.image_sm);
            modal.find('.modal-bestof-user-username').empty().append(data.user.username);
            modal.find('.modal-bestof-created').empty().append(data.diffCreatedAt);
            modal.find('.modal-bestof-category-slug').attr('href', data.user.slug).empty().append(data.category.name);
            if (data.nsfw)
                modal.find('.modal-bestof-nsfw').show();
            else
                modal.find('.modal-bestof-nsfw').hide();
            modal.find('.modal-bestof-body').empty().append(data.body);
            modal.find('.modal-bestof-image').empty();
            modal.find('.modal-bestof-video').empty();
            if (data.image)
                modal.find('.modal-bestof-image').append('<img class="mb-4 w-full h-auto" src="' + data.image + '">');
            else
            if (data.video)
                modal.find('.modal-bestof-video').append(
                    '<iframe class="modal-bestof-video" width="560" height="316" frameborder="0" src="' + data.video + '?autoplay=1&rel=0" allowfullscreen></iframe>'
                );
            modal.find('.modal-bestof-count-upvotes').empty().append(data.upvotes_count);
            modal.find('.modal-bestof-count-downvotes').empty().append(data.downvotes_count);
            modal.find('.modal-bestof-count-favorites').empty().append(data.favorites_count);
            modal.find('.modal-bestof-count-comments').empty().append(data.comments_count);

            modal.fadeIn(200);

            window.history.pushState(null, 'Najbolje meseca', '/najbolje/meseca');
        });
    });

    $('.close-modal').on('click', function (e) {
        e.preventDefault();

        var url = $(this).parent().find('.previous-url').text();
        window.history.pushState(previous_url, 'Title', previous_url);

        $(this).parents(':eq(3)').fadeOut(150);
    });

    // display sort dropdown nav box
    $('.display-sort-nav').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        // $('#sort-nav').fadeToggle(150);
        // $('#sort-nav').animate({height:'toggle'}, 150);
        $('#sort-nav').toggle();
    });

    // display dropdown nav box
    $('.display-dropdown-nav').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        // $('#dropdown-nav').animate({height:'toggle'}, 150);
        $('#dropdown-nav').toggle();
    });

    $(document).click(function () {
        // $('#dropdown-nav').animate({height:'hide'}, 150);
        $('#dropdown-nav').hide();
        $('#search-results').fadeOut(150);
        $('#search-results-responsive').fadeOut(150);
        // $('#sort-nav').animate({height:'hide'}, 150);
        $('#sort-nav').hide();
        $('#notification-body').hide();
    });

    $('#search-box').click(function (e) {
        e.stopPropagation();

        results = $(this).find('#search-results');

        if(results.is(':hidden') && $(this).find('#search').val() !== '')
            results.fadeIn(150);
    });

    $('#search-box-responsive').click(function (e) {
        e.stopPropagation();

        results = $(this).find('#search-results-responsive');

        if(results.is(':hidden') && $(this).find('#search-responsive').val() !== '')
            results.fadeIn(150);
    });

    var timer;

    $('#search').keyup(function (e) {

        if($(this).val() === '')
            $(this).parent().find('#search-results').fadeOut('fast');

        clearTimeout(timer);

        timer = setTimeout(search, 500);
    });

    $('#search-responsive').keyup(function (e) {

        if($(this).val() === '')
            $(this).parent().find('#search-results-responsive').fadeOut('fast');

        clearTimeout(timer);

        timer = setTimeout(searchResponsive, 500);
    });

    function search() {
        data = $('#search').val();
        results = $('#search-results');

        if (data !== '')
            results.fadeIn('fast')
        else
            return;

        $.post('/search', {'data': data}, function (data) {

            results.find('#search-results-posts, #search-results-categories, #search-results-users').empty();

            $.each(data.posts, function (i) {

                image = '';

                if(data.posts[i].image !== null)
                    image = '<div class="block mt-2 text-center"><img class="h-48" src="' + window.location.protocol + '//' + window.location.host + '/' + data.posts[i].image + '" alt></div>'

                results.find('#search-results-posts').append(
                    '<a class="p-2 block border-b hover:bg-grey-lightest" href="' + window.location.protocol + '//' + window.location.host + '/objava/' + data.posts[i].id + '">' +
                        '<span class="text-xs">' + data.posts[i].body + '</span>' +
                        image +
                    '</a>'
                );
            });

            $.each(data.categories, function (i) {
                results.find('#search-results-categories').append(
                    '<a href="' + window.location.protocol + '//' + window.location.host + '/kategorija/' + data.categories[i].slug + '">' +
                        '<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
                            '<img class="w-12 h-12" src="' + window.location.protocol + '//' + window.location.host + '/' + data.categories[i].image + '" alt="' + data.categories[i].name + '">' +
                            '<div class="ml-2">' +
                                '<div class="tracking-tight font-bold text-black">' + data.categories[i].name + '</div>' +
                                // '<div class="text-grey-darker text-xs italic">37 pratilaca</div>' +
                            '</div>' +
                        '</div>' +
                    '</a>'
                );
            });

            $.each(data.users, function (i) {
                results.find('#search-results-users').append(
                    '<a href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data.users[i].slug + '">' +
                        '<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
                            '<img class="w-12 h-12 rounded-full" src="' + window.location.protocol + '//' + window.location.host + '/' + data.users[i].image_sm + '" alt="' + data.users[i].username + '">' +
                            '<div class="ml-2">' +
                                '<div class="text-black">' + data.users[i].username + '</div>' +
                                // '<div class="text-grey-darker text-xs italic"></div>' +
                            '</div>' +
                        '</div>' +
                    '</a>'
                );
            });

        });
    }

    function searchResponsive() {
        data = $('#search-responsive').val();
        results = $('#search-results-responsive');

        if (data !== '')
            results.fadeIn('fast')
        else
            return;

        $.post('/search', {'data': data}, function (data) {

            results.find('#search-results-posts-responsive, #search-results-categories-responsive, #search-results-users-responsive').empty();

            $.each(data.posts, function (i) {

                image = null;

                if(data.posts[i].image !== null)
                    image = '<div class="block mt-2 text-center"><img class="h-48" src="' + window.location.protocol + '//' + window.location.host + '/' + data.posts[i].image + '" alt></div>'

                results.find('#search-results-posts-responsive').append(
                    '<a class="p-2 block border-b hover:bg-grey-lightest" href="' + window.location.protocol + '//' + window.location.host + '/objava/' + data.posts[i].id + '">' +
                        '<span class="text-xs">' + data.posts[i].body + '</span>' +
                        image +
                    '</a>'
                );
            });

            $.each(data.categories, function (i) {
                results.find('#search-results-categories-responsive').append(
                    '<a href="' + window.location.protocol + '//' + window.location.host + '/kategorija/' + data.categories[i].slug + '">' +
                        '<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
                            '<img class="w-12 h-12" src="' + window.location.protocol + '//' + window.location.host + '/' + data.categories[i].image + '" alt="' + data.categories[i].name + '">' +
                            '<div class="ml-2">' +
                                '<div class="tracking-tight font-bold text-black">' + data.categories[i].name + '</div>' +
                                // '<div class="text-grey-darker text-xs italic">37 pratilaca</div>' +
                            '</div>' +
                        '</div>' +
                    '</a>'
                );
            });

            $.each(data.users, function (i) {
                results.find('#search-results-users-responsive').append(
                    '<a href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data.users[i].slug + '">' +
                        '<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
                            '<img class="w-12 h-12 rounded-full" src="' + window.location.protocol + '//' + window.location.host + '/' + data.users[i].image_sm + '" alt="' + data.users[i].username + '">' +
                            '<div class="ml-2">' +
                                '<div class="text-black">' + data.users[i].username + '</div>' +
                                // '<div class="text-grey-darker text-xs italic"></div>' +
                            '</div>' +
                        '</div>' +
                    '</a>'
                );
            });

        });
    }

    $('#search-categories').keyup(function (e) {

        if($(this).val() === '')
            $(this).parent().find('#search-results').fadeOut('fast');

        clearTimeout(timer);

        timer = setTimeout(searchCategories, 500);
    });

    function searchCategories() {
        data = $('#search-categories').val();
        results = $('#search-categories-results');

        if (data !== '')
            results.fadeIn('fast')
        else
            return;

        $.post('/search/categories', {'data': data}, function (data) {

            results.find('#search-categories-results').empty();

            $.each(data.categories, function (i) {
                results.find('#search-categories-results').append(
                    '<a class="search-categories-single" data-id="' + data.categories[i].id + '" href="' + window.location.protocol + '//' + window.location.host + '/kategorija/' + data.categories[i].slug + '">' +
                        '<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
                            '<img class="w-12 h-12" src="' + window.location.protocol + '//' + window.location.host + '/' + data.categories[i].image + '" alt="' + data.categories[i].name + '">' +
                            '<div class="ml-2">' +
                                '<div class="search-categories-single-text tracking-tight font-bold text-black">' + data.categories[i].name + '</div>' +
                            '</div>' +
                        '</div>' +
                    '</a>'
                );
            });

        });
    }

    $('#modal-submit-header').delegate('.search-categories-single', 'click', function (e) {
        e.preventDefault();

        id = $(this).data('id');
        text = $(this).find('.search-categories-single-text').text();

        $('#search-categories').val(text);
        $('#search-categories').data('id', id);

        $(this).parent().hide();
    });

    $('.more-users').on('click', function (e) {
        if(!AuthUser) {
            e.preventDefault();
            $(this).closest('body').find('#modal-login').fadeIn(200);
            modal_active = true;
            return;
        }
    });

    $('.unblock-btn').on('click', function (e) {
        e.preventDefault();

        id = $(this).data('id');

        if (confirm('Da li želite da odblokirate ovu kategoriju?'))
            $.post('/category/unblock/' + id);

        $(this).closest('.blocked-category-box').fadeOut('slow');
    });

    sidebar_active = false;
    tabs_active = false;

    $('#responsive-caret, #responsive-caret-close').on('click', function (e) {
        e.preventDefault();

        // $('#tabs-responsive').animate({width:'hide'}, 200);
        $('#tabs-responsive').hide();
        tabs_active = false;

        // $('#sidebar-responsive').animate({width:'toggle'}, 200);
        $('#sidebar-responsive').toggle();
        sidebar_active = true;
    });

    $('#responsive-caret-left, #responsive-caret-left-close').on('click', function (e) {
        e.preventDefault();

        // $('#sidebar-responsive').animate({width:'hide'}, 200);
        $('#sidebar-responsive').hide();
        sidebar_active = false;

        // $('#tabs-responsive').animate({width:'toggle'}, 200);
        $('#tabs-responsive').toggle();
        tabs_active = true;
    });

    $('#report-other').on('click', function (e) {
        $('#report-text').fadeIn(200);
    });

    $('input[name=report-radio]').on('click', function (e) {
        if ($(this).val() !== '4')
            $('#report-text').fadeOut(100);
    });

    $('#btn-report').on('click', function (e) {
        e.preventDefault();

        id = $(this).data('id');
        type = $(this).data('type');
        body = $(this).parent().find('input[name=report-radio]:checked').val();

        if (body === "4")
            body = $('#report-body').val();

        post_id = null;
        if (type === 'comment')
            post_id = $(this).data('post');

        var data = {
            body: body,
            reportable_type: type,
            reportable_id: id,
            post_id: post_id,
            category_id: report_category_id,
        };

        $.ajax({
            url: '/report',
            type: 'POST',
            data: data,
            success: function () {
                notify('green', 'Uspeh!', 'Prijava je uspešno poslata moderatorima kategorije na razmatranje. Hvala vam što pomažete održavanju sajta');
            },
            error: function () {
                notify('red', 'Greška!', 'Dogodila se greška i vaša prijava nije poslata. Pokušajte ponovo');
            },
        });
    });

    $.validator.setDefaults({
        onkeyup: false,
        onclick: false,
        errorClass: 'text-red text-xs italic',
        highlight: function(element) {
            $(element).closest('.validate-input').addClass('border-red');
        },
        unhighlight: function(element) {
            $(element).closest('.validate-input').removeClass('border-red');
        }
    });

    $.validator.addMethod('nameMaxLength', function(value, element) {
        return value.length <= 30;
    }, 'Ime kategorije ne može biti duže od 30 karaktera');

    $.validator.addMethod('nameMinLength', function(value, element) {
        return value.length >= 3;
    }, 'Ime kategorije ne može biti kraće od 3 karaktera');

    $.validator.addMethod('usernameMaxLength', function(value, element) {
        return value.length <= 30;
    }, 'Korisničko ime ne može biti duže od 30 karaktera');

    $.validator.addMethod('usernameMinLength', function(value, element) {
        return value.length >= 3;
    }, 'Korisničko ime ne može biti kraće od 3 karaktera');

    $.validator.addMethod('passwordMinLength', function(value, element) {
        return value.length >= 8;
    }, 'Vaša lozinka je prekratka. Morate staviti najmanje 8 karaktera');

    $.validator.addMethod('postMinLength', function(value, element) {
        return value.length >= 10;
    }, 'Objava ne može imati manje od 10 karaktera');


    $('#form-register').validate({
        rules: {
            username: {
                required: true,
                usernameMaxLength: true,
                usernameMinLength: true,
                remote: {
                    url: window.location.protocol + '//' + window.location.host + '/validation/username',
                    type: 'POST'
                }
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: window.location.protocol + '//' + window.location.host + '/validation/email',
                    type: 'POST'
                }
            },
            password: {
                required: true,
                passwordMinLength: true
            },
            password_confirmation: {
                required: true,
                equalTo: '#register-password'
            }
        },
        messages: {
            username: {
                required: 'Ovo polje je obavezno',
                remote: $.validator.format("<strong>{0}</strong> već postoji")
            },
            email: {
                required: 'Ovo polje je obavezno',
                email: 'Unesite validnu email adresu',
                remote: $.validator.format("Vi ste već registrovani. Ulogujte se <a class='text-blue' href='#'>ovde</a>")
            },
            password: {
                required: 'Ovo polje je obavezno'
            },
            password_confirmation: {
                equalTo: 'Lozinke se ne podudaraju',
            }
        }
    });

    $('#form-create').validate({
        rules: {
            name: {
                required: true,
                nameMaxLength: true,
                nameMinLength: true,
                remote: {
                    url: window.location.protocol + '//' + window.location.host + '/validation/category-name',
                    type: 'POST'
                }
            },
            image: {
                required: true,
            }
        },
        messages: {
            name: {
                required: 'Vaša kategorija mora imati ime',
                remote: $.validator.format("{0} već postoji")
            },
            image: {
                required: 'Postavite naslovnu sliku kategorije',
            }
        }
    });

    $('#form-submit-text').validate({
        rules: {
            body: {
                required: true,
                postMinLength: true,
            }
        },
        messages: {
            body: {
                required: 'Objava ne može biti prazna'
            }
        }
    });

    $('#form-submit-image').validate({
        rules: {
            image: {
                required: true
            }
        },
        messages: {
            image: {
                required: 'Dodajte sliku'
            }
        }
    });

    $('#form-submit-video').validate({
        rules: {
            video: {
                required: true
            }
        },
        messages: {
            video: {
                required: 'Dodajte URL videa'
            }
        }
    });

    $('#form-edit-profile').validate({
        rules: {
            username: {
                required: true,
                usernameMaxLength: true,
                usernameMinLength: true,
                remote: {
                    url: window.location.protocol + '//' + window.location.host + '/validation/edit-username',
                    type: 'POST'
                }
            },
        },
        messages: {
            username: {
                required: 'Ovo polje je obavezno',
                remote: $.validator.format("<strong>{0}</strong> već postoji")
            }
        }
    });

    // Play video
    $('#posts, #modal-post, #post-single').delegate('.video-player', 'click', function (e) {
        e.preventDefault();

        id = $(this).data('id');
        $(this).find('.video-thumbnail').hide();
        $(this).find('.video-play-btn').hide();

        $(this).append(
            '<iframe class="absolute pin-t pin-l w-full h-full" width="560" height="316" src="https://www.youtube.com/embed/' + id + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>'
        );
    });

    // Post Upvote / Downvote
    $('#posts, #modal-post, #post-single').delegate('.vote-btn', 'click', function (e) {
        e.preventDefault();

        if(!AuthUser) {
            $(this).closest('body').find('#modal-login').fadeIn(200);
            modal_active = true;
            return;
        }

        span = $(this).find('span');
        type = $(this).data('type');
        id   = $(this).closest('.post-box')
            .data('id');

        if(type === 'upvote')
            otherBtn = $(this).parent().find('[data-type="downvote"]');
        else
            otherBtn = $(this).parent().find('[data-type="upvote"]');

        var data = {
            id:   id,
            type: type
        };

        if(type === 'upvote') {
            if($(this).hasClass('text-blue')) {
                $(this).removeClass('text-blue');

                num = span.text();
                span.text(parseInt(num) - 1);
            } else {
                $(this).addClass('text-blue');

                num = span.text();
                span.text(parseInt(num) + 1);
            }

            if(otherBtn.hasClass('text-purple')) {
                otherBtn.removeClass('text-purple');

                num2 = otherBtn.find('span').text();
                otherBtn.find('span').text(parseInt(num2) - 1);
            }
        } else {
            if($(this).hasClass('text-purple')) {
                $(this).removeClass('text-purple');

                num = span.text();
                span.text(parseInt(num) - 1);
            } else {
                $(this).addClass('text-purple');

                num = span.text();
                span.text(parseInt(num) + 1);
            }

            if(otherBtn.hasClass('text-blue')) {
                otherBtn.removeClass('text-blue');

                num2 = otherBtn.find('span').text();
                otherBtn.find('span').text(parseInt(num2) - 1);
            }
        }

        $.post('/post/vote', data, function (data) {
            console.log(data);
        });
    });

    // Post Favorite
    $('#posts, #modal-post, #post-single').delegate('.favorite-btn', 'click', function (e) {
        e.preventDefault();

        if(!AuthUser) {
            $(this).closest('body').find('#modal-login').fadeIn(200);
            modal_active = true;
            return;
        }

        id = $(this).closest('.post-box')
            .data('id');
        span = $(this).find('span');

        if($(this).hasClass('text-grey')) {
            $(this).removeClass('text-grey hover:text-grey-darker')
                .addClass('text-orange hover:text-orange-dark');

            num = span.text();
            span.text(parseInt(num) + 1);

        } else {
            $(this).removeClass('text-orange hover:text-orange-darker')
                .addClass('text-grey hover:text-grey-dark');

            num = span.text();
            span.text(parseInt(num) - 1);
        }

        $.post('/post/favorite/' + id);
    });

    // Show comments
    $('#posts, #modal-post, #post-single').delegate('.btn-comments', 'click', function (e) {
        e.preventDefault();

        parent = $(this).closest('.post-box');
        id = parent.data('id');
        parent.find('.comments-single').empty();

        $(this).toggleClass('text-grey text-grey-darker');

        $.get('/comments/' + id, function (data) {

            $.each(data, function (i) {

                parent.find('.comments-single').append(
                    '<div class="mb-4 flex">' +
                        '<a class="mr-2 flex flex-no-shrink" href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data[i].user.slug + '">' +
                            '<img class="rounded-full w-10 h-10" src="' + window.location.protocol + '//' + window.location.host + '/' + data[i].user.image_sm + '">' +
                        '</a>' +
                        '<div>' +
                            '<div class="flex justify-start">' +
                                '<a class="font-bold text-sm" href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data[i].user.slug + '">' + data[i].user.username + '</a>' +
                                '<span class="ml-2 text-grey-dark text-xs">' + data[i].diffCreatedAt + '</span>' +
                            '</div>' +
                            '<p class="font-sans w-full text-grey-black text-xs md:text-sm my-1">' + data[i].body + '</p>' +
                            '<div class="flex justify-start text-xs mt-2" data-id="' + data[i].id + '">' +
                                 '<a class="comment-vote-btn mr-3 text-grey-dark" data-type="upvote" href="#">' +
                                     '<i class="fas fa-arrow-up"></i> <span>' + data[i].upvotes_count + '</span>' +
                                 '</a>' +
                                 '<a class="comment-vote-btn mr-3 text-grey-dark" data-type="downvote" href="#">' +
                                     '<i class="fas fa-arrow-down"></i> <span>' + data[i].downvotes_count + '</span>' +
                                 '</a>' +
                                 '<a class="open-report-modal text-grey-dark" data-type="comment" data-id="' + data[i].id + '" data-post="' + data[i].post_id + '" href="#">prijavi</a>' +
                             '</div>' +
                        '</div>' +
                    '</div>'
                );
            });

        });

        parent.find('.comment-box').toggle();

    });

    // Submit comment
    $('.write-comment').keypress(function (e) {
        if (e.which === 13) {
            e.preventDefault();

            if(!AuthUser) {
                $(this).closest('body').find('#modal-login').fadeIn(200);
                modal_active = true;
                return;
            }

            date = new Date();
            date.setSeconds(date.getSeconds() - 30);

            if (! AuthIsAdmin && AuthLastCommented > date) {
                notify('red', 'Pokušajte kasnije', 'Da bismo izbegli spam, za novi komentar mora proći 30 sekundi');
                return;
            }

            parent  = $(this).closest('.post-box');
            id      = parent.data('id');
            body    = $(this).val();

            var data = {
                id: id,
                body: body
            };

            $.post('/comment', data, function (data) {

                parent.find('.comments-single').prepend(
                    '<div class="mb-4 flex">' +
                        '<a class="mr-2 flex flex-no-shrink" href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data.user.slug + '">' +
                            '<img class="rounded-full w-10 h-10" src="' + window.location.protocol + '//' + window.location.host + '/' + data.user.image_sm + '">' +
                        '</a>' +
                        '<div>' +
                            '<div class="flex justify-start">' +
                                '<a class="font-bold text-sm" href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data.user.slug + '">' + data.user.username + '</a>' +
                                '<span class="ml-2 text-grey-dark text-xs">pre 1 sekunde</span>' +
                            '</div>' +
                            '<p class="font-sans w-full text-grey-black text-xs md:text-sm my-1">' + data.comment.body + '</p>' +
                            '<div class="flex justify-start text-xs mt-2" data-id="' + data.comment.id + '">' +
                                 '<a class="comment-vote-btn mr-3 text-grey-dark" data-type="upvote" href="#">' +
                                     '<i class="fas fa-arrow-up"></i> <span>0</span>' +
                                 '</a>' +
                                 '<a class="comment-vote-btn mr-3 text-grey-dark" data-type="downvote" href="#">' +
                                     '<i class="fas fa-arrow-down"></i> <span>0</span>' +
                                 '</a>' +
                             '</div>' +
                        '</div>' +
                    '</div>'
                );

            });

            $(this).val('');

        }
    });

    // Vote on comment
    $('#posts, #modal-post, #post-single, .single-comment').delegate('.comment-vote-btn', 'click', function (e) {
        e.preventDefault();

        if(!AuthUser) {
            $(this).closest('body').find('#modal-login').fadeIn(200);
            modal_active = true;
            return;
        }

        span = $(this).find('span');
        type = $(this).data('type');
        id   = $(this).parent()
            .data('id');

        if(type === 'upvote')
            otherBtn = $(this).parent().find('[data-type="downvote"]');
        else
            otherBtn = $(this).parent().find('[data-type="upvote"]');

        var data = {
            id:   id,
            type: type
        };

        if(type === 'upvote') {
            if($(this).hasClass('text-blue')) {
                $(this).removeClass('text-blue');

                num = span.text();
                span.text(parseInt(num) - 1);
            } else {
                $(this).addClass('text-blue');

                num = span.text();
                span.text(parseInt(num) + 1);
            }

            if(otherBtn.hasClass('text-purple')) {
                otherBtn.removeClass('text-purple');

                num2 = otherBtn.find('span').text();
                otherBtn.find('span').text(parseInt(num2) - 1);
            }
        } else {
            if($(this).hasClass('text-purple')) {
                $(this).removeClass('text-purple');

                num = span.text();
                span.text(parseInt(num) - 1);
            } else {
                $(this).addClass('text-purple');

                num = span.text();
                span.text(parseInt(num) + 1);
            }

            if(otherBtn.hasClass('text-blue')) {
                otherBtn.removeClass('text-blue');

                num2 = otherBtn.find('span').text();
                otherBtn.find('span').text(parseInt(num2) - 1);
            }
        }

        $.post('/comment/vote', data);
    });

    // display dropdown menu for posts
    $('#posts, #modal-post, #post-single').delegate('.display-dropdown-post', 'click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).parent().find('.dropdown-post').fadeToggle('fast');
    });

    $(document).click(function() {
        $(".dropdown-post").fadeOut('fast');
    });

    // Blokiranje kategorije
    $('.post-box').delegate('.block-category-btn', 'click', function (e) {
        e.preventDefault();

        if(!AuthUser) {
            $(this).closest('body').find('#modal-login').show();
            modal_active = true;
            return;
        }

        id = $(this).closest('.post-box').data('category-id');

        if (confirm('Da li želite da blokirate ovu kategoriju? Ukoliko je blokirana, kategorija se neće pojavljivati na početnoj strani, ali ćete joj i dalje moći pristupiti.'))
            $.ajax({
                url: '/category/block/' + id,
                type: 'POST',
                success: function () {
                    notify('orange', 'Kategorija blokirana!', 'Uspešno ste blokirali kategoriju');
                }
            });
    });

    // Cropper
    var display_img = $("#display-cat-img");
    var upload_img = $("#upload-cat-img");

    // Display image before upload
    $("input:file").change(function () {
        if ($(this).val() !== '')
        {
            display_img.cropper('destroy');

            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (e) {

                display_img.width("100%");
                display_img.attr('src', this.result);

                // Cropper options
                var options = {
                    aspectRatio: 1 / 1,
                    minContainerWidth: 350,
                    minContainerHeight: 350,
                    minCropBoxWidth: 100,
                    minCropBoxHeight: 100,
                    minCanvasWidth: 300,
                    minCanvasHeight: 300,
                    cropBoxResizable: true,
                    viewMode: 2,
                    responsive: true,
                    strict: false,
                    preview: '.ac-preview'
                };

                display_img.cropper(options);
            }
        }
    });

    var timer;

    $('#btn-create-category').on('click', function (e) {
        e.preventDefault();

        if(!$('#form-create').valid()) {
            $(this).removeClass('bg-blue hover:bg-blue-dark').addClass('bg-red hover:bg-red-dark');

            return;
        } else {
            if($(this).data('disabled') === true) return;

            $(this).addClass('opacity-50 cursor-not-allowed');
            $(this).data('disabled', true);
        }

        parent          = $(this).parent();
        name            = parent.find('#cat-name').val();
        description     = parent.find('#cat-description').val();

        nsfw = 0;
        pictures = 0;
        videos = 0;
        mods = 0;

        if (parent.find('#cat-nsfw').is(":checked")) nsfw = 1;
        if (parent.find('#cat-pictures').is(":checked")) pictures = 1;
        if (parent.find('#cat-videos').is(":checked")) videos = 1;
        if (parent.find('#cat-mods').is(":checked")) mods = 1;

        display_img.cropper('getCroppedCanvas').toBlob(function (blob) {
            var formData = new FormData();

            formData.append('name', name);
            formData.append('description', description);
            formData.append('croppedImage', blob);
            formData.append('nsfw', nsfw);
            formData.append('pictures', pictures);
            formData.append('videos', videos);
            formData.append('mods', mods);

            $.ajax({
                type: 'POST',
                url: '/kategorija',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    window.location.replace(
                        window.location.protocol +
                        '//' +
                        window.location.host +
                        '/kategorija/' +
                        data
                    );
                },
                error: function (err) {
                    notify('red', 'Greška!', 'Dogodila se greška');
                }
            });
        });
    });

    $('#posts').delegate('.post-delete', 'click', function (e) {
        id = $(this).parents(':eq(2)').data('id');

        if (confirm('Da li ste sigurni da želite da obrišete objavu?')) {
            $(this).parents(':eq(2)')
                .find('.post-content')
                .empty()
                .append('<p class="font-sans text-grey-black text-xs md:text-sm mb-4">[obrisano]</p>');

            $.ajax({
                url: '/post/' + id,
                method: 'DELETE',
                success: function () {
                    notify('green', 'Objava obrisana',
                        'Uspešno ste obrisali objavu. Objava će i dalje postojati ali se neće videti njen sadržaj'
                    );
                }
            });
        }
    });

    $('#posts').delegate('.post-edit', 'click', function (e) {
        id = $(this).parents(':eq(2)').data('id');
        content = $(this).parents(':eq(2)').find('.post-content');
        body = content.find('p').text();

        content.find('p').empty().append(
            '<textarea class="edit-post-active w-full bg-grey-lightest p-4 mb-6 text-grey-darkest rounded border border-grey-light" data-id="' + id + '" autofocus>' + $.trim(body) + '</textarea>'
        );
    });

    $('#posts').delegate('.edit-post-active', 'keypress', function (e) {
        if (e.which == 13) {
            id = $(this).data('id');
            body = $(this).val();

            $(this).parent().empty().append('<p class="font-sans text-grey-black text-xs md:text-sm mb-4">' + body + '</p>');

            $.ajax({
                url: '/post/' + id,
                method: 'PATCH',
                data: { body: body },
                success: function () {
                    notify('green', 'Objava izmenjena',
                        'Uspešno ste izmenili objavu.'
                    );
                }
            });
        }

    });

    /*
     * show modal when clicked on image
     * function is called from jscroll callback
     */
    function initMagnificPopup() {
        $('.magnific-popup').magnificPopup({
            type: 'image',
            cursor: 'mfp-zoom-cur',
            tError: '<a href="%url%">Slika</a> ne može da se učita.',
            callbacks: {
                elementParse: function (item) {
                    item.src = item.el.attr('src');
                }
            }
        });
    }

    // instantiate on startup
    initMagnificPopup();

    // Open/Close notifications
    $('#notification-bell, #notification-count').on('click', function (e) {
        e.stopPropagation();

        $('#notification-body').toggle();
    });

    // Mark all notifications as read
    $('#notification-mark-read').on('click', function (e) {
        e.stopPropagation()

        $('.notification-single').removeClass('bg-grey-lighter');

        $.post('/notifications/all-read');

        $('#notification-count').hide();
    });

});
