$.validator.setDefaults({
	onkeyup: false,
	onclick: false,
	errorClass: 'text-red text-xs italic',
	highlight: function(element) {
		$(element).closest('.validate-input').addClass('border-red');
	},
	unhighlight: function(element) {
		$(element).closest('.validate-input').removeClass('border-red');
	}
});
	
$.validator.addMethod('nameMaxLength', function(value, element) {
	return value.length <= 30;
}, 'Ime kategorije ne može biti duže od 30 karaktera');

$.validator.addMethod('nameMinLength', function(value, element) {
	return value.length >= 3;
}, 'Ime kategorije ne može biti kraće od 3 karaktera');

$.validator.addMethod('usernameMaxLength', function(value, element) {
	return value.length <= 30;
}, 'Korisničko ime ne može biti duže od 30 karaktera');

$.validator.addMethod('usernameMinLength', function(value, element) {
	return value.length >= 3;
}, 'Korisničko ime ne može biti kraće od 3 karaktera');

$.validator.addMethod('passwordMinLength', function(value, element) {
	return value.length >= 8;
}, 'Vaša lozinka je prekratka. Morate staviti najmanje 8 karaktera');

$.validator.addMethod('postMinLength', function(value, element) {
	return value.length >= 10;
}, 'Objava ne može imati manje od 10 karaktera');


$('#form-register').validate({
	rules: {
		username: {
			required: true,
			usernameMaxLength: true,
			usernameMinLength: true,
			remote: {
				url: window.location.protocol + '//' + window.location.host + '/validation/username',
				type: 'POST'
			}
		},
		email: {
			required: true,
			email: true,
			remote: {
				url: window.location.protocol + '//' + window.location.host + '/validation/email',
				type: 'POST'
			}
		},
		password: {
			required: true,
			passwordMinLength: true
		},
		password_confirmation: {
			required: true,
			equalTo: '#register-password'
		}
	},
	messages: {
		username: {
			required: 'Ovo polje je obavezno',
			remote: $.validator.format("<strong>{0}</strong> već postoji")
		},
		email: {
			required: 'Ovo polje je obavezno',
			email: 'Unesite validnu email adresu',
			remote: $.validator.format("Vi ste već registrovani. Ulogujte se <a class='text-blue' href='#'>ovde</a>")
		},
		password: {
			required: 'Ovo polje je obavezno'
		},
		password_confirmation: {
			equalTo: 'Lozinke se ne podudaraju',
		}
	}
});

$('#form-create').validate({
	rules: {
		name: {
			required: true,
			nameMaxLength: true,
			nameMinLength: true,
			remote: {
				url: window.location.protocol + '//' + window.location.host + '/validation/category-name',
				type: 'POST'
			}
		},
		image: {
			required: true,
		}
	},
	messages: {
		name: {
			required: 'Vaša kategorija mora imati ime',
			remote: $.validator.format("{0} već postoji")
		},
		image: {
			required: 'Postavite naslovnu sliku kategorije',
		}
	}
});

$('#form-submit-text').validate({
	rules: {
		body: {
			required: true,
			postMinLength: true,
		}
	},
	messages: {
		body: {
			required: 'Objava ne može biti prazna'
		}
	}
});

$('#form-submit-image').validate({
	rules: {
		image: {
			required: true
		}
	},
	messages: {
		image: {
			required: 'Dodajte sliku'
		}
	}
});

$('#form-submit-video').validate({
	rules: {
		video: {
			required: true
		}
	},
	messages: {
		video: {
			required: 'Dodajte URL videa'
		}
	}
});

$('#form-edit-profile').validate({
	rules: {
		username: {
			required: true,
			usernameMaxLength: true,
			usernameMinLength: true,
			remote: {
				url: window.location.protocol + '//' + window.location.host + '/validation/edit-username',
				type: 'POST'
			}
		},
	},
	messages: {
		username: {
			required: 'Ovo polje je obavezno',
			remote: $.validator.format("<strong>{0}</strong> već postoji")
		}
	}
});
