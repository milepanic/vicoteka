$(document).ready(function (e) {

    $('.delete-post-btn').on('click', function (e) {
        e.preventDefault();

        id = $(this).closest('.post-row').data('id');

        if (!confirm('Da li želite da obrišete ovu objavu?'))
            return;
        
        $.ajax({
            url: '/post/' + id,
            type: 'DELETE',
            success: function () {
                window.location.reload();
            }
        });
        
    });

    $('.restore-post-btn').on('click', function (e) {
        e.preventDefault();

        id = $(this).closest('.post-row').data('id');

        if (!confirm('Da li želite da vratite ovu objavu?'))
            return;
        
        $.ajax({
            url: '/post/restore/' + id,
            type: 'PATCH',
            success: function () {
                window.location.reload();
            }
        });
        
    });

    $('.edit-post-btn').on('click', function (e) {
        e.preventDefault();

        id = $(this).closest('.post-row').data('id');
        text = $(this).closest('.post-row').find('.post-body').text();

        
        
        $.ajax({
            url: '/post/' + id,
            type: 'PATCH',
            success: function () {
                window.location.reload();
            }
        });
        
    });

});