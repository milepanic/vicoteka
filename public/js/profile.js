$(document).ready(function () {

	$('#btn-edit-profile').on('click', function (e) {
		e.preventDefault();

		parent 			= $(this).parent();
		id				= parent.data('id');
		username 		= parent.find('#edit-username').val();
		description 	= parent.find('#edit-description').val();
		facebook 		= parent.find('#edit-facebook').val();
		twitter 		= parent.find('#edit-twitter').val();
		instagram 		= parent.find('#edit-instagram').val();
		tumblr 			= parent.find('#edit-tumblr').val();

		var data = {
			username: username,
			description: description,
			facebook: facebook,
			twitter: twitter,
			instagram: instagram,
			tumblr: tumblr,
		};

		$.ajax({
			type: 'PUT',
			url: '/profile/edit/' + id,
			data: data,
			success: function (data) {
				console.log(data);
			},
			error: function(err) {
				console.log(err);
			}
		});
	});
});