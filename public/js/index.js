/**
 *
 *	JQuery file for posts
 *	
 *	1. Upvote/Downvote posts
 *	2. Favorite posts
 *	3. Show comments, submit comment
 *	4. Report comment
 *	5. Upvote/Downvote comment
 *	6. Dropdown menu for posts
 *	7. Block category
 *
 */


$(document).ready(function () {

	// Play video
	$('#posts, #modal-post, #post-single').delegate('.video-player', 'click', function (e) {
		e.preventDefault();

		id = $(this).data('id');
		$(this).find('.video-thumbnail').hide();
		$(this).find('.video-play-btn').hide();

		$(this).append(
			'<iframe class="absolute pin-t pin-l w-full h-full" width="560" height="316" src="https://www.youtube.com/embed/' + id + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>'
		);
	});

	// Post Upvote / Downvote
	$('#posts, #modal-post, #post-single').delegate('.vote-btn', 'click', function (e) {
		e.preventDefault();

		if(!AuthUser) {
			$(this).closest('body').find('#modal-login').fadeIn(200);
			modal_active = true;
			return;
		}

		span = $(this).find('span');
		type = $(this).data('type');
		id 	 = $(this).closest('.post-box')
			.data('id');

		if(type === 'upvote')
			otherBtn = $(this).parent().find('[data-type="downvote"]');
		else
			otherBtn = $(this).parent().find('[data-type="upvote"]');

		var data = {
			id:   id,
			type: type
		};

		if(type === 'upvote') {
			if($(this).hasClass('text-blue')) {
				$(this).removeClass('text-blue');

				num = span.text();
				span.text(parseInt(num) - 1);
			} else {
				$(this).addClass('text-blue');

				num = span.text();
				span.text(parseInt(num) + 1);
			}

			if(otherBtn.hasClass('text-purple')) {
				otherBtn.removeClass('text-purple');

				num2 = otherBtn.find('span').text();
				otherBtn.find('span').text(parseInt(num2) - 1);
			}
		} else {
			if($(this).hasClass('text-purple')) {
				$(this).removeClass('text-purple');

				num = span.text();
				span.text(parseInt(num) - 1);
			} else {
				$(this).addClass('text-purple');

				num = span.text();
				span.text(parseInt(num) + 1);
			}

			if(otherBtn.hasClass('text-blue')) {
				otherBtn.removeClass('text-blue');

				num2 = otherBtn.find('span').text();
				otherBtn.find('span').text(parseInt(num2) - 1);
			}
		}

		$.post('/post/vote', data, function (data) {
			console.log(data);
		});
	});

	// Post Favorite
	$('#posts, #modal-post, #post-single').delegate('.favorite-btn', 'click', function (e) {
		e.preventDefault();

		if(!AuthUser) {
			$(this).closest('body').find('#modal-login').fadeIn(200);
			modal_active = true;
			return;
		}

		id = $(this).closest('.post-box')
			.data('id');
		span = $(this).find('span');

		if($(this).hasClass('text-grey')) {
			$(this).removeClass('text-grey hover:text-grey-darker')
				.addClass('text-orange hover:text-orange-dark');

			num = span.text();
			span.text(parseInt(num) + 1);

		} else {
			$(this).removeClass('text-orange hover:text-orange-darker')
				.addClass('text-grey hover:text-grey-dark');

			num = span.text();
			span.text(parseInt(num) - 1);
		}

		$.post('/post/favorite/' + id);
	});

	// Show comments
	$('#posts, #modal-post, #post-single').delegate('.btn-comments', 'click', function (e) {
		e.preventDefault();

		parent = $(this).closest('.post-box');
		id = parent.data('id');
		parent.find('.comments-single').empty();

		$(this).toggleClass('text-grey text-grey-darker');

		$.get('/comments/' + id, function (data) {

			$.each(data, function (i) {

				parent.find('.comments-single').append(
					'<div class="mb-4">' +
					    '<a class="flex" href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data[i].user.slug + '">' + 
		                	'<img class="rounded-full w-10 h-10 mr-1"' +
		                		'src="' + window.location.protocol + '//' + window.location.host + '/' + data[i].user.image_sm + '" alt="' + data[i].user.username + '">' + 
			                '<p class="px-2 py-2 -mt-2">' +
			                    '<span class="text-base font-semibold mr-1">' + data[i].user.username + '</span> ' +
			                    '<span class="font-sans text-grey-black text-xs md:text-sm">' + data[i].body + '</span>' +
			                '</p>' +
			            '</a>' +
			            '<div class="flext items-center pl-12 text-xs" data-id="' + data[i].id + '">' +
			                '<a class="comment-vote-btn mr-3 text-grey-dark" data-type="upvote" href="#">' +
			                    '<i class="fas fa-arrow-up"></i> <span>' + data[i].upvotes_count + '</span>' +
			                '</a>' +
			                '<a class="comment-vote-btn mr-3 text-grey-dark" data-type="downvote" href="#">' +
			                    '<i class="fas fa-arrow-down"></i> <span>' + data[i].downvotes_count + '</span>' +
			                '</a>' +
			                '<a class="mr-3 text-grey-dark" href="#">' + data[i].diffCreatedAt + '</a>' +
			                '<a class="open-report-modal text-grey-dark" data-type="comment" data-id="' + data[i].id + '" data-post="' + data[i].post_id + '" href="#">prijavi</a>' +
			            '</div>' +
		            '</div>'
	            );
			});

		});

		parent.find('.comment-box').toggle();

	});

	// Submit comment
	$('.write-comment').keypress(function (e) {
	    if (e.which === 13) {
	        e.preventDefault();

	        if(!AuthUser) {
				$(this).closest('body').find('#modal-login').fadeIn(200);
				modal_active = true;
				return;
			}

	        parent 	= $(this).closest('.post-box');
	        id 		= parent.data('id');
			body 	= $(this).val();
	        
	        var data = {
	        	id: id,
	        	body: body
	        };

			$.post('/comment', data, function (data) {

				parent.find('.comments-single').prepend(
					'<div class="mb-4">' + 
						'<a class="flex" href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data.user.slug + '">' +
		                	'<img class="rounded-full w-10 h-10 mr-1"' +
		                		'src="' + window.location.protocol + '//' + window.location.host + '/'
						            + data.user.image_sm + '" alt="' + data.user.username + '">' + 
			                '<p class="px-2 py-2 -mt-2">' +
			                    '<span class="text-base font-semibold mr-1">' + data.user.username + '</span> ' +
			                    '<span class="font-sans text-grey-black text-xs md:text-sm">' + data.comment.body + '</span>' +
			                '</p>' +
		                '</a>' +
			            '<div class="flext items-center pl-12 text-xs" data-id="' + data.comment.id + '">' +
			                '<a class="comment-vote-btn mr-3 text-grey-dark" data-type="upvote" href="#">' +
			                    '<i class="fas fa-arrow-up"></i> <span>0</span>' +
			                '</a>' +
			                '<a class="comment-vote-btn mr-3 text-grey-dark" data-type="downvote" href="#">' +
			                    '<i class="fas fa-arrow-down"></i> <span>0</span>' +
			                '</a>' +
			                '<a class="mr-3 text-grey-dark" href="#">pre 3 sekunde</a>' +
			            '</div>' +
			        '</div>'
				);

			});

			$(this).val('');
			
	    }
	});

	// Vote on comment
	$('#posts, #modal-post, #post-single, .single-comment').delegate('.comment-vote-btn', 'click', function (e) {
		e.preventDefault();

		if(!AuthUser) {
			$(this).closest('body').find('#modal-login').fadeIn(200);
			modal_active = true;
			return;
		}

		span = $(this).find('span');
		type = $(this).data('type');
		id 	 = $(this).parent()
			.data('id');

		if(type === 'upvote')
			otherBtn = $(this).parent().find('[data-type="downvote"]');
		else
			otherBtn = $(this).parent().find('[data-type="upvote"]');

		var data = {
			id:   id,
			type: type
		};

		if(type === 'upvote') {
			if($(this).hasClass('text-blue')) {
				$(this).removeClass('text-blue');

				num = span.text();
				span.text(parseInt(num) - 1);
			} else {
				$(this).addClass('text-blue');

				num = span.text();
				span.text(parseInt(num) + 1);
			}

			if(otherBtn.hasClass('text-purple')) {
				otherBtn.removeClass('text-purple');

				num2 = otherBtn.find('span').text();
				otherBtn.find('span').text(parseInt(num2) - 1);
			}
		} else {
			if($(this).hasClass('text-purple')) {
				$(this).removeClass('text-purple');

				num = span.text();
				span.text(parseInt(num) - 1);
			} else {
				$(this).addClass('text-purple');

				num = span.text();
				span.text(parseInt(num) + 1);
			}

			if(otherBtn.hasClass('text-blue')) {
				otherBtn.removeClass('text-blue');

				num2 = otherBtn.find('span').text();
				otherBtn.find('span').text(parseInt(num2) - 1);
			}
		}

		$.post('/comment/vote', data);
	});

	// display dropdown menu for posts
	$('#posts, #modal-post, #post-single').delegate('.display-dropdown-post', 'click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).parent().find('.dropdown-post').fadeToggle('fast');
	});

	$(document).click(function() {
		$(".dropdown-post").fadeOut('fast');
	});

	// Blokiranje kategorije
	$('.post-box').delegate('.block-category-btn', 'click', function (e) {
		e.preventDefault();

		if(!AuthUser) {
			$(this).closest('body').find('#modal-login').show();
			modal_active = true;
			return;
		}

		id = $(this).closest('.post-box').data('category-id');
		
		if (confirm('Da li želite da blokirate ovu kategoriju? Ukoliko je blokirana, kategorija se neće pojavljivati na početnoj strani, ali ćete joj i dalje moći pristupiti.'))
			$.post('/category/block/' + id);
	});

});
