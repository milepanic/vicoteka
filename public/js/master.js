/**
 *
 *	Main JQuery file for the whole site
 * 
 *	1. JScroll
 *	2. Login AJAX request and submit login on enter keypress
 *	3. Register AJAX request
 *	4. Change tabs in submit modal and send AJAX request
 *	5. Change tabs on index page
 *	6. Subscribe to category
 *	7. Follow user
 *	8. Follow user from sidebar, or get new suggestion and from discover page
 *	9. Open and close modals
 *	10. Dropdown menu for user navigation
 *	11. Search
 *	12. Changing page title dynamically
 *	13. Open Best Of post modal
 *	14. Restrict guests from discover-... pages
 *	15. Unblock category
 *	16. Footer functionality and modals
 *	17. Responsive html css
 *	18. Send report data
 *
 */


$(document).ready(function () {

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	// infinite scroll for posts
    $('.infinite-scroll').jscroll({
        autoTrigger: true,
        loadingHtml: '<div class="mx-auto w-24"><img class="w-24" src="' + window.location.protocol + '//' + window.location.host 
            + '/images/loading.gif" alt="Učitava se..." /></div>',
        padding: 0,
        nextSelector: '.pagination li.active + li a',
        contentSelector: 'div.infinite-scroll',
        debug: true,
        callback: function () {
        	
        }
    });

    function reinitJscroll() {
    	$('.infinite-scroll').removeData('jscroll').jscroll.destroy();

    	$('.infinite-scroll').jscroll({
	        autoTrigger: true,
	        loadingHtml: '<div class="mx-auto w-24"><img class="w-24" src="' + window.location.protocol + '//' + window.location.host 
	            + '/images/loading.gif" alt="Učitava se..." /></div>',
	        padding: 0,
	        nextSelector: '.pagination li.active + li a',
	        contentSelector: 'div.infinite-scroll',
	        debug: true,
	        callback: function () {
	        	
	        }
	    });
    } 

	// submits login on enter
	$('#login-email, #login-password').keypress(function (e) {
		if(e.which == 13)
			$('.login-btn').click();
	});

	// send login user info
	$('.login-btn').on('click', function (e) {
		e.preventDefault();

		if ($(this).data('disabled') === true) return;

		$(this).addClass('opacity-50 cursor-not-allowed');
		$(this).data('disabled', true);

		parent = $(this).parent();
		email = parent.find('#login-email').val();
		password = parent.find('#login-password').val();
		remember = parent.find('#login-remember').val();

		var data = {
			email: email,
			password: password,
			remember: remember
		};

		var elem = $(this);

		$.ajax({
			type: 'POST',
			url: '/login',
			data: data,
			success: function (data) {
				window.location.replace('/');
			},
			error: function (err) {
				$('#login-has-errors').append(
					'<span class="text-xs text-red">' +
		                '<strong>Neispravan email ili lozinka</strong>' +
		            '</span>'
				);

				elem.data('disabled', false);
				elem.removeClass('opacity-50 cursor-not-allowed');
			}
		});
	});

	// send register user info
	$('.register-btn').on('click', function (e) {
		e.preventDefault();

		if(!$('#form-register').valid()) {
            $(this).removeClass('bg-teal hover:bg-teal-dark').addClass('bg-red hover:bg-red-dark');

            return;
        } else {
            if($(this).data('disabled') === true) return;

            $(this).addClass('opacity-50 cursor-not-allowed');
            $(this).data('disabled', true);
        }

		parent = $(this).parent();
		username = parent.find('#register-username').val();
		email = parent.find('#register-email').val();
		password = parent.find('#register-password').val();
		password_confirmation = parent.find('#register-password_confirmation').val();
		description = parent.find('#register-description').val();

		var data = {
			username: username,
			email: email,
			password: password,
			password_confirmation: password_confirmation,
			description: description
		};

		$.ajax({
			type: 'POST',
			url: '/register',
			data: data,
			success: function (data) {
				window.location.replace('/');
			},
			error: function(err) {
			console.log(err);

			}
		});
	});

	// change submit modal tab
	$('.submit-tab').on('click', function (e) {
		e.preventDefault();

		$(this).parent().find('.submit-tab-active').removeClass('submit-tab-active').addClass('submit-tab-inactive');
		$(this).removeClass('submit-tab-inactive').addClass('submit-tab-active');

		type = $(this).data('type');
		parent = $(this).parents(':eq(1)');

		parent.find('.submit-tab-div').hide();

		switch(type) {
			case 'text':
				parent.find('#form-submit-text').show();
				parent.find('#submit-hidden').val('text');
				break;
			case 'image':
				parent.find('#form-submit-image').show();
				parent.find('#submit-hidden').val('image');
				break;
			case 'video':
				parent.find('#form-submit-video').show();
				parent.find('#submit-hidden').val('video');
				break;
		}

	});

	// change header submit modal tab
	$('.h-submit-tab').on('click', function (e) {
		e.preventDefault();

		$(this).parent().find('.h-submit-tab-active').removeClass('h-submit-tab-active').addClass('h-submit-tab-inactive');
		$(this).removeClass('h-submit-tab-inactive').addClass('h-submit-tab-active');

		type = $(this).data('type');
		parent = $(this).parents(':eq(1)');

		parent.find('.h-submit-tab-div').hide();

		switch(type) {
			case 'text':
				parent.find('#h-form-submit-text').show();
				parent.find('#h-submit-hidden').val('text');
				break;
			case 'image':
				parent.find('#h-form-submit-image').show();
				parent.find('#h-submit-hidden').val('image');
				break;
			case 'video':
				parent.find('#h-form-submit-video').show();
				parent.find('#h-submit-hidden').val('video');
				break;
		}

	});

	// submit post from category page
	$('#modal-submit-content').delegate('.submit-btn', 'click', function (e) {
		e.preventDefault();

		parent = $(this).parents(':eq(1)');
		action = parent.find('#submit-hidden').val();
		body = parent.find('#submit-body').val();
		category = $(this).data('name');
		
		if(action === 'text' && !$('#form-submit-text').valid() || action === 'image' && !$('#form-submit-image').valid() 
			|| action === 'video' && !$('#form-submit-video').valid()) {
            $(this).removeClass('bg-blue hover:bg-blue-dark').addClass('bg-red hover:bg-red-dark');

            return;
        } else {
            if($(this).data('disabled') === true) return;

            $(this).addClass('opacity-50 cursor-not-allowed');
            $(this).data('disabled', true);
        }

		nsfw = 0;

		var elem = $(this);
		video = null;

		if(action === 'video') {
			video = parent.find('#submit-video-url').val();
			body = parent.find('#submit-body-video').val();

			if (parent.find('#submit-video-nsfw').is(":checked")) nsfw = 1;

			var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		    var match = video.match(regExp);

		    if (match && match[7].length == 11)
		    	video = 'https://www.youtube.com/embed/' + match[7];
		    else {
		    	alert('Neispravan Youtube URL');
		    	return;
		    }
		}

		if(action === 'image') {
			image = parent.find('input[type=file]')[0].files[0];
			body = parent.find('#submit-body-image').val();

			if (parent.find('#submit-image-nsfw').is(":checked")) nsfw = 1;

			var data = new FormData();

			data.append('action', action);
			data.append('body', body);
			data.append('image', image);
			data.append('category', category);
			data.append('nsfw', nsfw);

			$.ajax({
			    type: 'POST',
			    url: '/post',
			    data: data,
			    processData: false,
			    contentType: false,
			    success: function (data) {
			       elem.parents(':eq(3)').hide();
				modal_active = false;
			    },
			    error: function () {
			        console.log('Upload error');
			    }
			});

			return;
		}

		if (parent.find('#submit-text-nsfw').is(":checked")) nsfw = 1;

    	var data = {
			action: action,
			body: body,
			video: video,
			category: category,
			nsfw: nsfw
		};

		$.ajax({
			type: 'POST',
			url: '/post',
			data: data,
			success: function () {
				elem.parents(':eq(3)').hide();
				modal_active = false;
			},
			error: function(err) {
				console.log(err);
			}
		});
	});

	// submit post from navigation
	$('#h-modal-submit-content').delegate('.submit-btn', 'click', function (e) {
		e.preventDefault();

		parent = $(this).parents(':eq(1)');
		action = parent.find('#h-submit-hidden').val();
		body = parent.find('#h-submit-body').val();
		category = $('#search-categories').val();

		// if(action === 'text' && !$('#h-form-submit-text').valid() || action === 'image' && !$('#h-form-submit-image').valid() 
		// 	|| action === 'video' && !$('#h-form-submit-video').valid()) {
  //           $(this).removeClass('bg-blue hover:bg-blue-dark').addClass('bg-red hover:bg-red-dark');

  //           return;
  //       } else {
  //           if($(this).data('disabled') === true) return;

  //           $(this).addClass('opacity-50 cursor-not-allowed');
  //           $(this).data('disabled', true);
  //       }

		nsfw = 0;

		var elem = $(this);
		video = null;

		if(action === 'video') {
			video = parent.find('#h-submit-video-url').val();
			body = parent.find('#h-submit-body-video').val();

			if (parent.find('#h-submit-video-nsfw').is(":checked")) nsfw = 1;

			var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		    var match = video.match(regExp);

		    if (match && match[7].length == 11)
		    	video = 'https://www.youtube.com/embed/' + match[7];
		    else {
		    	alert('Neispravan Youtube URL');
		    	return;
		    }
		}

		if(action === 'image') {
			image = parent.find('input[type=file]')[0].files[0];
			body = parent.find('#h-submit-body-image').val();

			if (parent.find('#h-submit-image-nsfw').is(":checked")) nsfw = 1;

			var data = new FormData();

			data.append('action', action);
			data.append('body', body);
			data.append('image', image);
			data.append('category', category);
			data.append('nsfw', nsfw);

			$.ajax({
			    type: 'POST',
			    url: '/post',
			    data: data,
			    processData: false,
			    contentType: false,
			    success: function (data) {
					elem.parents(':eq(3)').hide();
					modal_active = false;
			    },
			    error: function () {
			        alert('Dogodila se greška');
			    }
			});

			return;
		}

		if (parent.find('#h-submit-text-nsfw').is(":checked")) nsfw = 1;

    	var data = {
			action: action,
			body: body,
			video: video,
			category: category,
			nsfw: nsfw
		};

		$.ajax({
			type: 'POST',
			url: '/post',
			data: data,
			success: function () {
				elem.parents(':eq(3)').hide();
				modal_active = false;
			},
			error: function(err) {
				console.log(err);
			}
		});
	});

	// changing main tabs
	$("#filter-tabs a").click(function (e) {
		e.preventDefault();

		var type = $(this).text().toLowerCase();
		$(this).parent().find('a.nav-active').removeClass("nav-active");
		$(this).addClass('nav-active');

		uri = '/';
		category = false;

		if(window.location.href.indexOf('kategorija') > -1) {
			category = true;
			pathArray = '';
			pathArray = window.location.pathname.split('/');
			uri = '/' + pathArray[1] + '/' + pathArray[2] + '/';
			category_name = pathArray[2].replace(/(?:^|\s)\S/g, function(l) {
				return l.toUpperCase();
			});
			category_name = category_name.split('-').join(' ');
		}

		$.get(uri + type, function (data) {
			$('#posts').html(data);
			window.history.pushState(type, 'Title', uri + type);

			if (!category) {
				if (window.location.href.indexOf('popularno') > -1) {
					document.title = 'Popularno | Distopija';
				} else if (window.location.href.indexOf('novo') > -1) {
					document.title = 'Novo | Distopija';
				} else if (window.location.href.indexOf('top') > -1) {
					document.title = 'Top | Distopija';
				} else if (window.location.href.indexOf('kategorije') > -1) {
					document.title = 'Kategorije | Distopija';
				} else if (window.location.href.indexOf('korisnici') > -1) {
					document.title = 'Korisnici | Distopija';
				}
			} else {
				if (window.location.href.indexOf('popularno') > -1) {
					document.title = 'Popularno - ' + category_name + ' | Distopija';
				} else if (window.location.href.indexOf('novo') > -1) {
					document.title = 'Novo - ' + category_name + ' | Distopija';
				} else if (window.location.href.indexOf('top') > -1) {
					document.title = 'Top - ' + category_name + ' | Distopija';
				}
			}
			// Reinitializes JScroll so it loads correct data
			reinitJscroll();
		});
	});

	// subscribe to category from category page
	$('.subscribe-btn').on('click', function (e) {
		e.preventDefault();

		if(!AuthUser) {
			$(this).closest('body').find('#modal-login').show();
			return;
		}

		if ($(this).hasClass('subscribe')) {
			$(this).removeClass('subscribe').addClass('subscribed');
			$(this).text('Pratiš');
			count = $(this).parent().find('#subscribers-count');
			nmb = count.text();
			newNmb = parseInt(nmb) + 1;
			count.text(newNmb);
		} else {
			$(this).removeClass('subscribed').addClass('subscribe');
			$(this).text('Zaprati');
			count = $(this).parent().find('#subscribers-count');
			nmb = count.text();
			newNmb = parseInt(nmb) - 1;
			count.text(newNmb);
		}

		var data = {
			id: $(this).data('id'),
			column: 'subscribed',
			action: $(this).data('action'), // subscribe/unsubscribe
		};

		// change data action to 0 or 1
		x = $(this).data('action');
		x = 1 - x;
		$(this).data('action', x);

		$.post('/kategorija/subscribe', data);
	});

	$('.subscribe-btn-discover').on('click', function (e) {
		e.preventDefault();

		id = $(this).data('id');

		var data = {
			id: $(this).data('id'),
			column: 'subscribed',
			action: 1,
		};
		
		$.post('/kategorija/subscribe', data);

		$(this).closest('.discover-category-box').fadeOut('slow');
	});

	// Follow User from profile page
	$('.follow-btn').on('click', function (e) {
		e.preventDefault();

		if(!AuthUser) {
			$(this).closest('body').find('#modal-login').show();
			return;
		}

		if ($(this).hasClass('follow')) {
			$(this).removeClass('follow').addClass('following');
			$(this).text('Pratite');
		} else {
			$(this).removeClass('following').addClass('follow');
			$(this).text('Zaprati');
		}

		id = $(this).parents(':eq(1)').data('id');

		$.post('/profil/follow/' + id);
	});

	// Removes current suggested user from sidebar and appends new
	function getNewFollow(elem) {
		$.get('new-follow', function (data) {
			elem.closest('li').empty().append(
	            '<div class="flex items-center relative">' +
	                '<a href="">' +
	                    '<img class="w-12 h-12 rounded-full" src="' + data[0].image_sm + '" alt="' + data[0].username + '">' +
	                '</a>' +
	                '<div class="ml-3 flex flex-col">' +
	                    '<a class="text-grey-darkest font-medium -mt-3 mb-1" ' +
	                        'href="">' + data[0].username + '</a>' +
	                    '<div>' +
	                        '<button class="follow-btn-sidebar py-1 px-4 bg-white hover:bg-red-lightest rounded-full shadow text-red text-sm border border-red" data-id="' + data[0].id + '">Zaprati</button>' +
	                    '</div>' +
	                '</div>' +
	                '<span class="absolute pin-t pin-b pin-r p-4">' +
	                    '<a class="new-follow h-12 w-12 text-grey-light hover:text-grey-darkest" href="#" title="Nova preporuka">' +
	                        '<i class="fas fa-times"></i>' +
	                    '</a>' +
	                '</span>' +
	            '</div>'
			);
		});
	}

	// Follow user from category sidebar
	$('.follow-btn-category').on('click', function (e) {
		e.preventDefault();
		
		var elem = $(this);
		id = elem.data('id');

		if ($(this).text() === 'Zaprati')
			$(this).text('Pratite');
		else
			$(this).text('Zaprati');

		$.post('/profil/follow/' + id);
	});

	$('.unfollow-btn').on('click', function (e) {
		e.preventDefault();

		var elem = $(this);
		id = elem.data('id');

		if (confirm('Da li želite da otpratite korisnika?'))
			$.post('/profil/follow/' + id);

		$(this).text('Zaprati');
	});

	// Follow user from recommended users
	$('#sidebar-users, #sidebar-users-responsive').delegate('.follow-btn-sidebar', 'click', function (e) {
		e.preventDefault();

		var elem = $(this);
		id = elem.data('id');

		$.post('/profil/follow/' + id);

		getNewFollow(elem);
	});

	// Remove current and get new suggested user to follow
	$('#sidebar-users').delegate('.new-follow', 'click', function(e) {
		e.preventDefault();

		getNewFollow($(this));
	});

	$('.follow-btn-discover').on('click', function (e) {
		e.preventDefault();

		id = $(this).data('id');

		$.post('/profil/follow/' + id);

		$(this).closest('.discover-user-box').fadeOut('slow');
	});

	// ovo je premjesteno u modal-edit-profile, ako radi obrisati
	// // edit profile
	// $('#btn-edit-profile').on('click', function (e) {
	// 	e.preventDefault();

	// 	parent 			= $(this).parent();
	// 	id				= parent.data('id');
	// 	username 		= parent.find('#edit-username').val();
	// 	description 		= parent.find('#edit-description').val();
	// 	facebook 		= parent.find('#edit-facebook').val();
	// 	twitter 			= parent.find('#edit-twitter').val();
	// 	instagram 		= parent.find('#edit-instagram').val();
	// 	tumblr 			= parent.find('#edit-tumblr').val();

	// 	var data = {
	// 		username: username,
	// 		description: description,
	// 		facebook: facebook,
	// 		twitter: twitter,
	// 		instagram: instagram,
	// 		tumblr: tumblr,
	// 	};

	// 	var elem = $(this);

	// 	$.ajax({
	// 		type: 'PUT',
	// 		url: '/profile/edit/' + id,
	// 		data: data,
	// 		success: function (data) {
	// 			elem.parents(':eq(3)').hide();
	// 			modal_active = false;
	// 			window.location.reload();
	// 		},
	// 		error: function(err) {
	// 			console.log(err);
	// 		}
	// 	});
	// });

	// Modals
	$('#responsive-open-submit-modal, #open-submit-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('#modal-submit').fadeIn(200);
	});

	$('#main-header-open-submit-modal, #header-open-submit-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('#modal-submit-header').fadeIn(200);
	});

	$('.open-login-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		previous_url = window.location.href;
		window.history.pushState('', 'Title', '/login');

		$(this).closest('body').find('#modal-login').fadeIn(200);
		// modal = $(this).closest('body').find('#modal-login');
	});

	$('.open-register-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		previous_url = window.location.href;
		window.history.pushState('', 'Title', '/register');

		$(this).closest('body').find('#modal-register').fadeIn(200);
	});

	$('.open-create-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).closest('body').find('#modal-create').fadeIn(200);
	});

	// koristiti ovo umjesto appendovanja u html, testirati radi li dobro
	var previous_url = null;

	$('#posts').delegate('.open-post-modal', 'click', function (e) {
		e.preventDefault();

		modal = $(this).closest('body').find('#modal-post');
		// modal.find('.previous-url').empty().append(window.location.href);
		previous_url = window.location.href;

		parent = $(this).closest('.post-box');
		id = $(this).data('id');
		window.history.pushState(id, 'Title', '/objava/' + id);

		user = parent.find('.post-user-info').clone();
		content = parent.find('.post-content').clone();
		buttons = parent.find('.post-buttons').clone();

		modal.find('.post-box').data('id', id);
		modal.find('.modal-post-user-info').empty().append(user);
		modal.find('#modal-post-content').empty().append(content);
		modal.find('#modal-post-buttons').empty().append(buttons);

		$(this).closest('body').find('#modal-post').fadeIn(200);
	});

	$('.open-edit-profile-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).closest('body').find('#modal-edit-profile').fadeIn(200);
	});

	$('.open-feedback-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).closest('body').find('#modal-feedback').fadeIn(200);
	});

	reportable_type = null;
	reportable_id = null;
	report_category_id = null;
	report_post_id = null;

	$('#posts, #modal-post, #post-single').delegate('.open-report-modal', 'click', function (e) {
		e.preventDefault();

		reportable_type = $(this).data('type');
		reportable_id = $(this).data('id');
		report_category_id = $(this).data('category');

		if (reportable_type === 'post') {
			$('#report-1').text('Nelegalni ili uznemirujući sadržaj');
			$('#report-2').text('Kršenje pravila');
			$('#report-3').text('Vređanje');
		} else if (reportable_type === 'user') {
			$('#report-1').text('Neprikladno ime');
			$('#report-2').text('Spam');
			$('#report-3').text('Vređanje');
		} else if (reportable_type === 'comment') {
			$('#report-1').text('Kršenje pravila');
			$('#report-2').text('Spam');
			$('#report-3').text('Vređanje');
			
			$('#btn-report').data('post', $(this).data('post'));
		}

		$('#btn-report').data('type', reportable_type);
		$('#btn-report').data('id', reportable_id);
		$('#btn-report').data('category', report_category_id);

		$('#modal-report').fadeIn(200);
	});

	$('.open-nsfw-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('#modal-nsfw').fadeIn(200);
	});

	$('.open-contact-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).closest('body').find('#modal-contact').fadeIn(200);
	});

	$('.open-autors-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).closest('body').find('#modal-autors').fadeIn(200);
	});

	$('.open-privacy-policy-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).closest('body').find('#modal-privacy-policy').fadeIn(200);
	});

	modal_bestof_today_active = false;

	$('.open-bestOfToday-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		modal = $('#modal-bestOf-post');

		previous_url = window.location.href;

		$.get('/best-of/day', function (data) {
			modal.find('.modal-bestof-user-slug').attr('href', data.user.slug);
			modal.find('.modal-bestof-user-image').attr('src', data.user.image_sm);
			modal.find('.modal-bestof-user-username').empty().append(data.user.username);
			modal.find('.modal-bestof-created').empty().append(data.diffCreatedAt);
			modal.find('.modal-bestof-category-slug').attr('href', data.user.slug).empty().append(data.category.name);
			modal.find('.modal-bestof-body').empty().append(data.body);
			modal.find('.modal-bestof-count-upvotes').empty().append(data.upvotes_count);
			modal.find('.modal-bestof-count-downvotes').empty().append(data.downvotes_count);
			modal.find('.modal-bestof-count-favorites').empty().append(data.favorites_count);
			modal.find('.modal-bestof-count-comments').empty().append(data.comments_count);

			modal.fadeIn(200);

			window.history.pushState(null, 'Najbolje dana', '/najbolje/dana');
		});

	});

	modal_bestof_week_active = false;

	$('.open-bestOfWeek-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		previous_url = window.location.href;

		modal = $('#modal-bestOf-post');

		$.get('/best-of/week', function (data) {
			modal.find('.modal-bestof-user-slug').attr('href', data.user.slug);
			modal.find('.modal-bestof-user-image').attr('src', data.user.image_sm);
			modal.find('.modal-bestof-user-username').empty().append(data.user.username);
			modal.find('.modal-bestof-created').empty().append(data.diffCreatedAt);
			modal.find('.modal-bestof-category-slug').attr('href', data.user.slug).empty().append(data.category.name);
			modal.find('.modal-bestof-body').empty().append(data.body);
			modal.find('.modal-bestof-count-upvotes').empty().append(data.upvotes_count);
			modal.find('.modal-bestof-count-downvotes').empty().append(data.downvotes_count);
			modal.find('.modal-bestof-count-favorites').empty().append(data.favorites_count);
			modal.find('.modal-bestof-count-comments').empty().append(data.comments_count);

			modal.fadeIn(200);

			window.history.pushState(null, 'Najbolje sedmice', '/najbolje/sedmice');
		});
	});

	modal_bestof_month_active = false;

	$('.open-bestOfMonth-modal').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		previous_url = window.location.href;

		modal = $('#modal-bestOf-post');

		$.get('/best-of/month', function (data) {
			modal.find('.modal-bestof-user-slug').attr('href', data.user.slug);
			modal.find('.modal-bestof-user-image').attr('src', data.user.image_sm);
			modal.find('.modal-bestof-user-username').empty().append(data.user.username);
			modal.find('.modal-bestof-created').empty().append(data.diffCreatedAt);
			modal.find('.modal-bestof-category-slug').attr('href', data.user.slug).empty().append(data.category.name);
			modal.find('.modal-bestof-body').empty().append(data.body);
			modal.find('.modal-bestof-count-upvotes').empty().append(data.upvotes_count);
			modal.find('.modal-bestof-count-downvotes').empty().append(data.downvotes_count);
			modal.find('.modal-bestof-count-favorites').empty().append(data.favorites_count);
			modal.find('.modal-bestof-count-comments').empty().append(data.comments_count);

			modal.fadeIn(200);
		
			window.history.pushState(null, 'Najbolje meseca', '/najbolje/meseca');
		});
	});

	$('.close-modal').on('click', function (e) {
		e.preventDefault();

		var url = $(this).parent().find('.previous-url').text();
		window.history.pushState(previous_url, 'Title', previous_url);

		$(this).parents(':eq(3)').fadeOut(150);
	});

	// display dropdown nav box
	$('.display-dropdown-nav').on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).parent().find('#dropdown-nav').fadeToggle(150);
	});

	$(document).click(function () {
		$('#dropdown-nav').fadeOut(150);
		$('#search-results').fadeOut(150);
		$('#search-results-responsive').fadeOut(150);
	});

	$('#search-box').click(function (e) {
		e.stopPropagation();

		results = $(this).find('#search-results');

		if(results.is(':hidden') && $(this).find('#search').val() !== '') 
			results.fadeIn(150);
	});

	$('#search-box-responsive').click(function (e) {
		e.stopPropagation();

		results = $(this).find('#search-results-responsive');

		if(results.is(':hidden') && $(this).find('#search-responsive').val() !== '') 
			results.fadeIn(150);
	});

	var timer;

	$('#search').keyup(function (e) {

		if($(this).val() === '')
			$(this).parent().find('#search-results').fadeOut('fast');

		clearTimeout(timer);

		timer = setTimeout(search, 500);
	});

	$('#search-responsive').keyup(function (e) {

		if($(this).val() === '')
			$(this).parent().find('#search-results-responsive').fadeOut('fast');

		clearTimeout(timer);

		timer = setTimeout(searchResponsive, 500);
	});

	function search() {
		data = $('#search').val();
		results = $('#search-results');

		if (data !== '')
			results.fadeIn('fast')
		else
			return;

		$.post('/search', {'data': data}, function (data) {

			results.find('#search-results-posts, #search-results-categories, #search-results-users').empty();

			$.each(data.posts, function (i) {

				image = '';

				if(data.posts[i].image !== null)
					image = '<div class="block mt-2 text-center"><img class="h-48" src="' + window.location.protocol + '//' + window.location.host + '/' + data.posts[i].image + '" alt></div>'
				
				results.find('#search-results-posts').append(
					'<a class="p-2 block border-b hover:bg-grey-lightest" href="' + window.location.protocol + '//' + window.location.host + '/objava/' + data.posts[i].id + '">' +
						'<span class="text-xs">' + data.posts[i].body + '</span>' +
						image +
					'</a>'
				);
			});

			$.each(data.categories, function (i) {
				results.find('#search-results-categories').append(
					'<a href="' + window.location.protocol + '//' + window.location.host + '/kategorija/' + data.categories[i].slug + '">' +
						'<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
	                        '<img class="w-12 h-12" src="' + window.location.protocol + '//' + window.location.host + '/' + data.categories[i].image + '" alt="' + data.categories[i].name + '">' +
	                        '<div class="ml-2">' +
	                            '<div class="tracking-tight font-bold text-black">' + data.categories[i].name + '</div>' +
	                            // '<div class="text-grey-darker text-xs italic">37 pratilaca</div>' +
	                        '</div>' +
	                    '</div>' +
	                '</a>'
				);
			});

			$.each(data.users, function (i) {
				results.find('#search-results-users').append(
					'<a href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data.users[i].slug + '">' +
						'<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
	                        '<img class="w-12 h-12 rounded-full" src="' + window.location.protocol + '//' + window.location.host + '/' + data.users[i].image_sm + '" alt="' + data.users[i].username + '">' +
	                        '<div class="ml-2">' +
	                            '<div class="text-black">' + data.users[i].username + '</div>' +
	                            // '<div class="text-grey-darker text-xs italic"></div>' +
	                        '</div>' +
	                    '</div>' +
                    '</a>'
				);
			});

		});
	}

	function searchResponsive() {
		data = $('#search-responsive').val();
		results = $('#search-results-responsive');

		if (data !== '')
			results.fadeIn('fast')
		else
			return;

		$.post('/search', {'data': data}, function (data) {

			results.find('#search-results-posts-responsive, #search-results-categories-responsive, #search-results-users-responsive').empty();

			$.each(data.posts, function (i) {

				image = null;

				if(data.posts[i].image !== null)
					image = '<div class="block mt-2 text-center"><img class="h-48" src="' + window.location.protocol + '//' + window.location.host + '/' + data.posts[i].image + '" alt></div>'
				
				results.find('#search-results-posts-responsive').append(
					'<a class="p-2 block border-b hover:bg-grey-lightest" href="' + window.location.protocol + '//' + window.location.host + '/objava/' + data.posts[i].id + '">' +
						'<span class="text-xs">' + data.posts[i].body + '</span>' +
						image +
					'</a>'
				);
			});

			$.each(data.categories, function (i) {
				results.find('#search-results-categories-responsive').append(
					'<a href="' + window.location.protocol + '//' + window.location.host + '/kategorija/' + data.categories[i].slug + '">' +
						'<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
	                        '<img class="w-12 h-12" src="' + window.location.protocol + '//' + window.location.host + '/' + data.categories[i].image + '" alt="' + data.categories[i].name + '">' +
	                        '<div class="ml-2">' +
	                            '<div class="tracking-tight font-bold text-black">' + data.categories[i].name + '</div>' +
	                            // '<div class="text-grey-darker text-xs italic">37 pratilaca</div>' +
	                        '</div>' +
	                    '</div>' +
	                '</a>'
				);
			});

			$.each(data.users, function (i) {
				results.find('#search-results-users-responsive').append(
					'<a href="' + window.location.protocol + '//' + window.location.host + '/profil/' + data.users[i].slug + '">' +
						'<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
	                        '<img class="w-12 h-12 rounded-full" src="' + window.location.protocol + '//' + window.location.host + '/' + data.users[i].image_sm + '" alt="' + data.users[i].username + '">' +
	                        '<div class="ml-2">' +
	                            '<div class="text-black">' + data.users[i].username + '</div>' +
	                            // '<div class="text-grey-darker text-xs italic"></div>' +
	                        '</div>' +
	                    '</div>' +
                    '</a>'
				);
			});

		});
	}

	$('#search-categories').keyup(function (e) {

		if($(this).val() === '')
			$(this).parent().find('#search-results').fadeOut('fast');

		clearTimeout(timer);

		timer = setTimeout(searchCategories, 500);
	});

	function searchCategories() {
		data = $('#search-categories').val();
		results = $('#search-categories-results');

		if (data !== '')
			results.fadeIn('fast')
		else
			return;

		$.post('/search/categories', {'data': data}, function (data) {

			results.find('#search-categories-results').empty();

			$.each(data.categories, function (i) {
				results.find('#search-categories-results').append(
					'<a class="search-categories-single" data-id="' + data.categories[i].id + '" href="' + window.location.protocol + '//' + window.location.host + '/kategorija/' + data.categories[i].slug + '">' +
						'<div class="p-2 flex justify-left border-b hover:bg-grey-lightest">' +
	                        '<img class="w-12 h-12" src="' + window.location.protocol + '//' + window.location.host + '/' + data.categories[i].image + '" alt="' + data.categories[i].name + '">' +
	                        '<div class="ml-2">' +
	                            '<div class="search-categories-single-text tracking-tight font-bold text-black">' + data.categories[i].name + '</div>' +
	                        '</div>' +
	                    '</div>' +
	                '</a>'
				);
			});

		});
	}

	$('#modal-submit-header').delegate('.search-categories-single', 'click', function (e) {
		e.preventDefault();

		id = $(this).data('id');
		text = $(this).find('.search-categories-single-text').text();

		$('#search-categories').val(text);
		$('#search-categories').data('id', id);

		$(this).parent().hide();
	});

	$('.more-categories, .more-users').on('click', function (e) {
		if(!AuthUser) {
			e.preventDefault();
			$(this).closest('body').find('#modal-login').fadeIn(200);
			modal_active = true;
			return;
		}
	});

	$('.unblock-btn').on('click', function (e) {
		e.preventDefault();

		id = $(this).data('id');
		
		if (confirm('Da li želite da odblokirate ovu kategoriju?'))
			$.post('/category/unblock/' + id);

		$(this).closest('.blocked-category-box').fadeOut('slow');
	});

	sidebar_active = false;
	tabs_active = false;

	$('#responsive-caret, #responsive-caret-close').on('click', function (e) {
		e.preventDefault();

		$('#tabs-responsive').animate({width:'hide'}, 200);
		tabs_active = false;
		// $('#sidebar-responsive').animate({scrollTop: document.body.scrollHeight}, 'slow');

		$('#sidebar-responsive').animate({width:'toggle'}, 200);
		sidebar_active = true;
	});

	$('#responsive-caret-left, #responsive-caret-left-close').on('click', function (e) {
		e.preventDefault();

		$('#sidebar-responsive').animate({width:'hide'}, 200);
		sidebar_active = false;

		$('#tabs-responsive').animate({width:'toggle'}, 200);
		tabs_active = true;
	});

	$('#report-other').on('click', function (e) {
		$('#report-text').fadeIn(200);
	});

	$('input[name=report-radio]').on('click', function (e) {
		if ($(this).val() !== '4')
			$('#report-text').fadeOut(100);
	});

	$('#btn-report').on('click', function (e) {
		e.preventDefault();

		id = $(this).data('id');
		type = $(this).data('type');
		body = $(this).parent().find('input[name=report-radio]:checked').val();

		if (body === "4") 
			body = $('#report-body').val();

		post_id = null;
		if (type === 'comment')
			post_id = $(this).data('post');

		var data = {
			body: body,
			reportable_type: type,
			reportable_id: id,
			post_id: post_id,
			category_id: report_category_id,
		};

		$.post('/report', data);
	});
	

});