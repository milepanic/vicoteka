<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('slug')->unique();
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable()->unique();
            $table->text('description', 300)->nullable();
            $table->boolean('admin')->default(false);
            $table->string('image_sm')->nullable();
            $table->string('image_lg')->nullable();
            $table->boolean('nsfw')->default(false);
            // $table->integer('points')->default(0);
            // $table->date('banned_until')->nullable();
            // $table->string('ban_message')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('tumblr')->nullable();
            $table->timestamp('last_posted_at')->nullable();
            $table->timestamp('last_commented_at')->nullable();
            $table->timestamp('last_created_category_at')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('follow', function (Blueprint $table) {
            $table->integer('follower_id')->unsigned();
            $table->integer('followed_id')->unsigned();

            $table->foreign('follower_id')->references('id')->on('users');
            $table->foreign('followed_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
