<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20)->unique();
            $table->string('slug')->unique();
            $table->text('description');
            $table->string('image');
            $table->boolean('nsfw')->nullable();
            $table->boolean('cover_box')->nullable();
            $table->boolean('pictures')->nullable();
            $table->boolean('videos')->nullable();
            $table->boolean('mods_only')->nullable();
            $table->boolean('approved')->nullable();
            $table->boolean('recommended')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('category_user', function (Blueprint $table){
            $table->integer('category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('subscribed')->nullable();
            $table->boolean('blocked')->nullable();
            $table->boolean('moderator')->nullable();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
