<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'body' 	  => $faker->text,
        'post_id' => $faker->numberBetween($min = 1, $max = 15),
        'user_id' => $faker->numberBetween($min = 1, $max = 15),
    ];
});
