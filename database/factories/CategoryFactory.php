<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name'          => $faker->unique()->lastName,
        'slug'          => function (array $category) { return str_slug($category['name']); },
        'description'   => $faker->text,
        'image'         => 'images/categories/obrisati.png',
        'nsfw'          => $faker->boolean,
        'cover_box'     => $faker->boolean,
        'pictures'      => $faker->boolean,
        'videos'        => $faker->boolean,
        'mods_only'     => $faker->boolean,
        'approved'      => $faker->boolean,
    ];
});
