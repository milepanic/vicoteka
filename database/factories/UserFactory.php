<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $rand = rand(1, 8);
    return [
        'username' 			=> $faker->name,
        'slug' 				=> function (array $user) { return str_slug($user['username']); },
        'email' 			       => $faker->unique()->safeEmail,
        'password' 			=> '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
        'description' 		=> $faker->paragraph,
        'admin'				=> $faker->boolean,
        'image_sm'			=> 'images/avatar/blank-' . $rand . '.png',
        'image_lg'			=> 'images/avatar/blank-' . $rand . '.png',
        'remember_token' 	=> str_random(10),
    ];
});
