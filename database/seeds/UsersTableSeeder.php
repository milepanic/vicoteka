<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
            'username'  => 'admin',
            'slug'      => str_slug('admin'),
            'email'     => 'admin@admin.com',
            'password'  => bcrypt('admin123'),
            'description' => 'Nikada se ne bih kandidovao za predsjednika Amerike jer ne bih podnio da živim u manjoj kući. A i nisam Amer',
            'admin'     => true,
            'image_sm'  => 'images/avatar/blank-1.png',
            'image_lg'  => 'images/avatar/blank-1.png',
        ]);
        
        factory(App\User::class, 30)->create();
    }
}
